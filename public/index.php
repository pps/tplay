<?php
// [ 应用入口文件 ]
//ini_set("display_errors",'on');
//error_reporting(E_ALL);

// 版本信息
define('PRODUCT_NAME', 'Tplay');
define('PRODUCT_VERSION', 'v2.1.9');
define('PRODUCT_URL', 'https://gitee.com/pps/tplay');
// 定义应用目录
define('APP_PATH', __DIR__ . '/../app/');
// 重定义扩展类库目录
define('EXTEND_PATH', __DIR__ . '/../extend/');
// 重定义第三方类库目录
define('VENDOR_PATH', __DIR__ . '/../vendor/');
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';