/**
 * Created by Administrator on 2020/12/11.
 */

//是否存在指定函数
function isExitsFunction(funcName) {
    try {
        if (typeof(eval(funcName)) == "function") {
            return true;
        }
    } catch (e) {
    }
    return false;
}

//获取url参数
function getQueryValue(key) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == key) {
            return pair[1];
        }
    }
    return (false);
}

//获取cookie参数
function getCookieValue(key) {
    const regex = new RegExp(key + '=([^;]+)');
    const match = document.cookie.match(regex);
    return match && match[1];
}

//获取环境
function getEnv() {
    var ua = navigator.userAgent.toLowerCase();
    const isWeixin = ua.indexOf('micromessenger') !== -1;
    if (isWeixin) {
        return 'wx';
    } else {
        const isInApp = /(^|;\s)app\//.test(ua);
        if (!isInApp) {
            return 'browser';
        } else {
            var isAndroid = ua.match(/android|adr/i);
            if (isAndroid) {
                return 'android';
            }
            var isIos = ua.match(/(iphone|ipod|ipad);?/i);
            if (isIos) {
                return 'ios';
            }
            return 'app';
        }
    }
}

//限制微信才能打开
function onlyWeixin() {
    var ua = navigator.userAgent.toLowerCase();
    var isWeixin = ua.indexOf('micromessenger') !== -1;
    if (!isWeixin) {
        document.head.innerHTML = '<title>抱歉，出错了</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0"><link rel="stylesheet" type="text/css" href="https://res.wx.qq.com/open/libs/weui/0.4.1/weui.css">';
        document.body.innerHTML = '<div class="weui_msg"><div class="weui_icon_area"><i class="weui_icon_info weui_icon_msg"></i></div><div class="weui_text_area"><h4 class="weui_msg_title">请在微信客户端打开链接</h4></div></div>';
    }
}

//给网页注入jquery
function addJquery() {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js';
    head.appendChild(script);
}


//依赖jQuery
if (window.jQuery) {
    (function ($) {
        /**
         * 表单转json 函数
         * var post_data = $('#myForm').serializeJson();
         * @param allow
         * @returns {{}}
         */
        $.fn.serializeJson = function (allow) {
            var serializeObj = {};
            var array = this.serializeArray();
            var str = this.serialize();
            $(array).each(function () {
                if (serializeObj[this.name]) {
                    if ($.isArray(serializeObj[this.name]) && this.value != "") {
                        serializeObj[this.name].push(this.value);
                    } else {
                        if (allow || this.value != "") {
                            serializeObj[this.name] = [serializeObj[this.name], this.value];
                        }
                    }
                } else {
                    if (allow || this.value != "") {
                        serializeObj[this.name] = this.value;
                    }
                }
            });
            return serializeObj;
        };

        /**
         * outerHtml 函数
         * @param allow
         */
        $.fn.outerHtml = function (allow) {
            return this.clone().prop('outerHTML');
        }

    })(jQuery);


    /**
     * cookie 函数
     * 依赖jquery.cookie.js
     * @param key
     * @param val
     */
    function setCookie(key, val) {
        // localStorage.setItem('jwt', val);
        $.cookie(key, val, { path: '/', expires: 1 });//默认有效时间1天
    }
    function setCookie2(key, val, expires) {
        if (expires == undefined) {
            expires = 1;//默认有效时间1天
        }
        $.cookie(key, val, { path: '/', expires: expires });
    }
    function getCookie(key) {
        // localStorage.getItem(key);
        return $.cookie(key);
    }
    function removeCookie(key) {
        // localStorage.removeItem(key);
        $.cookie(key, null, { path: '/' }); //将cookie名为‘openid’的值设置为空，实际已删除
        $.removeCookie(key, { path: '/' });
    }
}

//生成10位时间戳
function getTimestamp() {
    var tmp = Date.parse(new Date()).toString();
    tmp = tmp.substr(0, 10);
    return tmp;
}

//字节大小转换单位
//format_bytes(filesize, '');
function format_bytes(size, delimiter) {
    var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    for (var i = 0; size >= 1024 && i < 5; i++) size /= 1024;
    return Math.round(size) + delimiter + units[i];
}

/**
 * 格式化金额
 * number：要格式化的数字
 * decimals：保留几位小数
 * dec_point：小数点符号
 * thousands_sep：千分位符号
 * */
function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k; // 使用Math.round进行四舍五入
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.floor(n)).split('.');
    var re = /(\d{1,3})(?=(\d{3})+(?!\d))/g; // 修正正则表达式以匹配所有数字
    while (re.test(s[0])) {
        s[0] = s[0].replace(re, "$1" + sep + "$2");
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

//判断是否数字
function isNumber(str) {
    if (isNaN(Number(str, 10))) {
        return false;
    } else {
        return true;
    }
}

//随机生成密码
function randomPassword(size) {
    var seed = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'p', 'Q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '2', '3', '4', '5', '6', '7', '8', '9'
    );//数组
    seedlength = seed.length;//数组长度
    var createPassword = '';
    for (i = 0; i < size; i++) {
        j = Math.floor(Math.random() * seedlength);
        createPassword += seed[j];
    }
    return createPassword;
}

//深拷贝
function deepClone(data) {
    var _data = JSON.stringify(data),
        dataClone = JSON.parse(_data);
    return dataClone;
}