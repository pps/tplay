<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/7/24
 * Time: 10:35
 */
session_start();
if (isset($_SESSION['admin']['admin_name']) && !empty($_SESSION['admin']['admin_name'])) {
    //login
} else {
    //logout
    header(sprintf('Location:%s', 'http://127.0.0.1'));
    exit ('logout');
}