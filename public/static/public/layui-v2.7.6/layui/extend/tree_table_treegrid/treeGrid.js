/**

 @Name：treeGrid树状表格
 @Author：lrd
 */
layui.extend({
    dltable: 'tree_table_treegrid/dltable' //拓展一个模块别名
}).define(['laytpl', 'laypage', 'dltable', 'layer', 'form'], function (exports) {
    "use strict";
    var $ = layui.jquery;
    var layer = layui.layer;
    var dltable = layui.dltable;
    var MOD_NAME = 'treeGrid';
    var treeGrid = $.extend({}, dltable);
    treeGrid._render = treeGrid.render;
    treeGrid.render = function (param) {//重写渲染方法
        param.isTree = true;//是树表格
        treeGrid._render(param);
    };

    console.log(layui.cache.base)
    layui.link(layui.cache.base+ 'tree_table_treegrid/treeGrid.css?v=2');//引入css

    exports(MOD_NAME, treeGrid);
});