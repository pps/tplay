layui.define(['jquery'], function (exports) {

    //先将其定义为一个函数,构造函数
    MyTest = function() {};

    //然后给它的prototype添加方法
    MyTest.prototype.hello = function (str) {
        alert('hello ' + str)
    }

    // 导出构造函数，而不是实例
    exports('test', MyTest)
});