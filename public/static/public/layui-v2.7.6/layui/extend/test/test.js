layui.define(['jquery'], function (exports) {

    //先将其定义为一个函数,构造函数
    MyTest = function() {};

    //然后给它的prototype添加方法
    MyTest.prototype.hello = function () {
        alert('hello')
    }

    //在这里，你试图导出一个mytest的实例，而不是导出mytest函数或者它的原型方法。
    var mytest = new MyTest();
    exports('test', mytest)
});