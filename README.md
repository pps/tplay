
![预览](https://images.gitee.com/uploads/images/2020/1209/110523_b9207c4c_396298.jpeg "tplaylogin.jpg")
![预览](https://images.gitee.com/uploads/images/2020/1209/110503_e5481600_396298.jpeg "tplayindex.jpg")
![预览](https://images.gitee.com/uploads/images/2020/1209/110535_c00e2385_396298.jpeg "tplaymenu.jpg")

## 简介
 
Tplay是一款基于TP5 + layui + Mysql开发的后台管理框架，致力于快速建站。

集成了一般应用所必须的基础性功能，为开发者减少重复性的工作，提升开发速度，规范团队开发模式。

本人基于Tplay 1.3.4二次开发，修复了许多bug，添加了许多功能，让开发更有效率。



## 安装使用

### [在线文档](https://gitee.com/pps/tplay/wikis/pages?sort_id=4562118&doc_id=480370)

> 离线文档已加入到源码中，文档目录 /app/install/doc/

> QQ交流群：609048497

## 版本信息

php适用版本:7 ~ 7.2

mysql适用版本:5.5 ~ 5.7

基于layui-2.7.6

基于thinkphp-5.0.24

## 版权信息

遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017 by Tplay

All rights reserved


