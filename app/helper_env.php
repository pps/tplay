<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

// +----------------------------------------------------------------------
// 系统方法
// +----------------------------------------------------------------------


/**
 * 限制只允许本地访问
 */
function stop_online()
{
    if (!isLocal()) {
        exit("非法请求");
    }
}


//判断本地环境
function isLocal()
{
    $db_password = \think\Env::get('db_password', '');
    return in_array($db_password, ['', '123456']);
}


/**
 * 清理temp目录
 * @return bool
 */
function tempClear()
{
    if (array_map('unlink', glob(TEMP_PATH . '*.php'))) {
        return true;
    } else {
        return false;
    }
}


/**
 * 获取后台名称
 * @return string
 */
function systemName()
{
    if (isInstalled()) {
        $name = \app\common\model\Webconfig::getValue('name', 3600);
        if ($name) {
            return $name;
        }
    }
    return PRODUCT_NAME;
}

/**
 * 检查php版本
 */
function checkPhpVersion()
{
    if (PHP_VERSION < 7 || PHP_VERSION >= 7.3) {
        header("Content-type: text/html; charset=utf-8");
        exit("运行环境要求PHP 7.0 ~ 7.2，请修改PHP版本");
    }
}

/**
 * 检查安装文件
 */
function checkInstalled()
{
    if (!isInstalled()) {
        exit("未找到安裝文件，请重新安装!");
    }
}

/**
 * 判断网站维护中
 * 要先执行 register_route()才行
 */
function checkCloseSite()
{
    if (isInstalled()) {
        $is_close = \think\Env::get('is_close_site', false);
        if ($is_close) {
            $request = \think\Request::instance();
            if ($request->has('sitekey')) {
                cookie('sitekey', $request->param('sitekey'));
            }
            if (cookie('sitekey') != (new \app\admin\model\Urlconfig())->getCloseSiteKey()) {
                exit("网站维护中 ...");
            }
        }
    }
}

/**
 * 注册自定义路由
 */
function registerRoute()
{
    if (isInstalled()) {
        //url美化 例：Route::rule('blog/:id','index/blog/read');
        if (think\Cache::get('vae_route')) {
            $runtimeRoute = think\Cache::get('vae_route');
        } else {
            $runtimeRoute = \think\Db::name("urlconfig")->where(['status' => 1])->order('id desc')->column('aliases,url');
            think\Cache::set('vae_route', $runtimeRoute);
        }
        if ($runtimeRoute) {
            \think\Route::rule($runtimeRoute);
        }
    }
}

/**
 * 判断是否完成安装
 * @return bool
 */
function isInstalled()
{
    static $isInstalled;
    if (empty($isInstalled)) {
        $isInstalled = file_exists(ROOT_PATH . 'install.lock');
    }
    return $isInstalled;
}

/**
 * 系统的加密解密的salt
 * 使用安装文件计算得到salt，这样每个安装系统salt唯一
 * 安装文件要注意保密,否则会导致密码被破解
 * @return string
 */
function getSalt()
{
    checkInstalled();
    $origin = file_get_contents(ROOT_PATH . "install.lock");
    return password($origin, 'Y8xz1WH7KiQyE4GXgpJIVcbvLCeUdoOr');
}

/**
 * 不可逆加密,用于管理员密码加密
 * @param $password [密码]
 * @param $password_salt [可以自定义salt]
 * @return string
 */
function password($password, $password_salt = '')
{
    if ($password_salt == '') {
        $password_salt = getSalt();
    }
    return md5(md5($password) . md5($password_salt));
}

/**
 * 从附件表获取图片文件的url
 * @param $id int 附件表的id
 * @param $default string 没数据时返回的默认值
 * @param $host bool/string 可指定域名,默认当前域名
 * @return bool|mixed|string
 */
function getAttachmentUrl($id, $default = '', $host = true)
{
    if ($id) {
        if (is_string($id)) {
            $thumb_url = $id;
            if (ifStartWith($thumb_url, 'http')) {
                return $thumb_url;
            } else {
                return url($thumb_url, '', false, $host);
            }
        }
        $attachment = \think\Db::name("attachment")->where(['id' => $id])->find();
        if (empty($attachment)) {
            return $default;
        }
        if ($attachment['status'] == 1) {
            //审核通过
            $thumb_url = replaceUrlDS($attachment['filepath']);
        } elseif ($attachment['status'] == 0) {
            //待审核
            $thumb_url = '/static/public/images/shenhe.jpg';
        } else {
            //不通过
            $thumb_url = '/static/public/images/shenhe.jpg';
        }
        if (ifStartWith($thumb_url, 'http')) {
            return $thumb_url;
        } else {
            return url($thumb_url, '', false, $host);
        }
    }
    return $default;
}


/**
 * 邮件发送
 * @param $to_email string 收件邮箱地址
 * @param $title string 邮件标题
 * @param $content string 邮件内容
 * @param $from_email string 发件邮箱地址
 * @param $from_name string 发件人名字
 * @param $smtp string [smtp服务器]
 * @param $username string 发件箱的账号
 * @param $password string 发件箱的密码（或授权码）
 * @return array [bool,string]
 */
function sendMail($to_email, $title, $content, $from_email, $from_name, $smtp, $username, $password)
{
    vendor('phpmailer.PHPMailerAutoload');
    //vendor('PHPMailer.class#PHPMailer');
    $mail = new \PHPMailer();
    // 设置PHPMailer使用SMTP服务器发送Email
    $mail->IsSMTP();
    // 设置邮件的字符编码，若不指定，则为'UTF-8'
    $mail->CharSet = 'UTF-8';
    // 添加收件人地址，可以多次使用来添加多个收件人
    $mail->AddAddress($to_email);
    // 设置邮件正文
    $mail->Body = $content;
    // 设置邮件头的From字段
    $mail->From = $from_email;
    // 设置发件人名字
    $mail->FromName = $from_name;
    // 设置邮件标题
    $mail->Subject = $title;
    // 设置SMTP服务器。
    $mail->Host = $smtp;
    // 检查SSL/TLS设置是否正确
    $mail->SMTPSecure = 'ssl';
    //确保服务器上的防火墙没有阻止SMTP端口（通常是25、465或587）
    $mail->Port = 465;
    // 设置为"需要验证"
    $mail->SMTPAuth = true;
    //设置html发送格式
    $mail->isHTML(true);
    // 设置用户名和密码。
    $mail->Username = $username;
    $mail->Password = $password;
    //开启调试模式，这将显示客户端和服务器之间的所有通信信息
    //$mail->SMTPDebug = 2;
    // 发送邮件。
    $res = $mail->Send();
    return [$res, $mail->ErrorInfo];
}



/**
 * 防止快速刷新，如3秒内请求5次，则提示异常
 * 此方法存在的问题，并发的时候，实际并不能通过前$refresh-1次的请求，所以用来防止重复注册，是不行的
 * @param int $seconds 几秒内
 * @param int $refresh 刷新次数
 * @param $callback null|mixed 回调函数，默认跳转至攻击者服务器地址
 */
function stopCC($callback = null, $seconds = 3, $refresh = 5)
{
    //代理IP直接退出
    empty($_SERVER['HTTP_VIA']) or exit('Ip Agent Denied');

    // 设置监控变量
    $cur_time = time();
    if (session("?last_time")) {
        session("refresh_times", session("refresh_times") + 1);
    } else {
        session("last_time", $cur_time);
        session("refresh_times", 1);
    }


    $interval = $cur_time - session("last_time");
    //打印
//    var_dump($interval);//几秒内
//    var_dump(session("refresh_times"));//刷新次数


    // 处理监控结果
    if ($interval <= $seconds && session("refresh_times") >= $refresh) {
        if (empty($callback)) {
            // 跳转至攻击者服务器地址
            header(sprintf('Location:%s', 'http://127.0.0.1'));
        } else {
            $callback();
        }
        exit ('Access Denied');
    } else {
        //超过3秒后重置变量
        if ($interval > $seconds) {
            session("last_time", $cur_time);
            session("refresh_times", 0);
        }
    }
}


/**
 * 获取控制器的所有方法名
 * @param $className [类全名]
 * @return array 这将返回一个数组，仅包含在该类中声明的方法，不包含从父类继承的方法
 */
function getActions($className)
{
    return $declared_methods = array_diff(get_class_methods($className), get_class_methods(get_parent_class($className)));
}

/**
 * 限制ID字段的长度，防止非法格式导致入库失败
 * 超出長度的值，日志处理
 * @param $name [字段名]
 * @param $value [值]
 * @param int $len [限制长度]
 * @return int|string
 */
function limitNumLen($name, $value, $len = 10)
{
    if (!isset($value) || empty($value)) {
        return 0;
    }
    $value = trim($value);
    //判断是否为数字，或字符串数字
    if (is_numeric($value) && strlen($value) <= $len) {
        return $value;
    }
    \think\Log::record("limitNumLen : $name = $value", 'alert');
    return 0;
}

/**
 * 限制字段的长度，防止非法格式导致入库失败
 * 超出長度的值，日志处理
 * @param $name [字段名]
 * @param $value [值]
 * @param int $len [限制长度]
 * @return int|string
 */
function limitStrLen($name, $value, $len = 10)
{
    if (!isset($value) || empty($value)) {
        return '';
    }
    if (is_string($value) && mb_strlen($value, "utf-8") > $len) {
        \think\Log::record("limitStrLen : $name = $value", 'alert');
        return subStrCN($value, $len);
    }
    return $value;
}
