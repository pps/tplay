<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/7/6
 * Time: 13:46
 */

namespace app\common\service;


use app\api\controller\base\Api;
use app\common\model\User;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use string\Encryption;

class LoginService extends Api
{

    const ACCESS_TOKEN = 'access_token';
    const ACCESS_TOKEN_EXPIRE_TIME = 'access_token_expire';
    const REFRESH_TOKEN = 'refresh_token';
    const REFRESH_TOKEN_EXPIRE_TIME = 'refresh_token_expire';
    const LOGIN_INFO = 'login_info';


    /**
     * 刷新访问令牌
     * 网页端会自动刷新cookie中的令牌
     * 其他端如果不支持cookie才需要此接口
     * @param $refresh_token
     * @return array
     */
    public function refreshTokens($refresh_token)
    {
        try {
            //如果要更严格校验刷新令牌，还应该验证access_token, 或者增加client_id,client_key验证是哪个端的jwt

            JWT::$leeway = 60;
            $jwt_key = getSalt();
            $decoded = JWT::decode($refresh_token, new Key($jwt_key, 'HS256'));

            $arr = (array)$decoded;
            if (!isset($arr['exp']) || $arr['exp'] < time()) {
                throw new \think\Exception('Expired token');
            } else {

                if (LoginService::REFRESH_TOKEN != $arr['data']->type) {
                    throw new \think\Exception('刷新令牌不正确');
                }

                //解析成功
                $userId = $arr['data']->userId;
                $loginTime = $arr['data']->loginTime;
                $salt = $arr['data']->salt;

                //防止重复刷新
                $user = User::get($userId);
                if (!$user) {
                    throw new \think\Exception('找不到用户信息,请先注册');
                }
                if ($user->salt != $salt) {
                    throw new \think\Exception('刷新令牌不可重复使用');
                }

                //刷新令牌只能使用一次，用一次就重置
                self::resetUserSalt($userId);

                //返回新的 访问令牌 和 刷新令牌
                return LoginService::getAccessToken($userId, $loginTime);
            }

        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case "Expired token":
                    $this->json_error('Token过期,请重新登录', null, self::ERR_CODE_401);
                    break;
                case "Wrong number of segments":
                    $this->json_error('Token验证失败,请重新登录', null, self::ERR_CODE_401);
                    break;
                default:
                    $this->json_error($e->getMessage(), null, self::ERR_CODE_401);
            }
        }
    }


    /**
     * 登入时调用，获取登入令牌、刷新令牌,并设置登入状态
     * @param $userId int 用户id
     * @param $loginTime int 用户登入时间
     * @param int $expire 设置jwt过期时长,默认2小时
     * @return array
     */
    public static function getAccessToken($userId, $loginTime, $expire = 3600 * 2)
    {
        $exptime = time() + $expire;
        // data 添加 exptime，后端可以判断session过期并退出，有部分页面是没有经过jwt鉴权的
        $data = ['userId' => $userId, 'loginTime' => $loginTime, 'exptime' => $exptime, 'expire' => $expire, 'type' => self::ACCESS_TOKEN];
        session(self::LOGIN_INFO, $data);//后端可验证登入状态

        //生成访问令牌
        $access_token = LoginService::createJwt($data, $exptime);
        cookie(self::ACCESS_TOKEN, $access_token, $expire);//前端可验证登入状态
        cookie(self::ACCESS_TOKEN_EXPIRE_TIME, $exptime);


        //生成刷新令牌，用于访问令牌过期时，刷新访问令牌，无需重新登入
        $exptime2 = $exptime + 3600;//过期时间要比访问令牌长
        $salt = self::getUserSalt($userId);
        $data2 = ['userId' => $userId, 'loginTime' => $loginTime, 'exptime' => $exptime2, 'salt' => $salt, 'type' => self::REFRESH_TOKEN];

        $refresh_token = LoginService::createJwt($data2, $exptime2);
        cookie(self::REFRESH_TOKEN, $refresh_token, $expire + 3600);//过期时长要比访问令牌长
        cookie(self::REFRESH_TOKEN_EXPIRE_TIME, $exptime2);


        return [
            self::ACCESS_TOKEN => $access_token,
            self::ACCESS_TOKEN_EXPIRE_TIME => $exptime,
            self::REFRESH_TOKEN => $refresh_token,
            self::REFRESH_TOKEN_EXPIRE_TIME => $exptime2,
        ];
    }


    /**
     * 重置用户salt,用于重置刷新令牌
     * @param $userId
     * @return string
     * @throws \think\Exception
     */
    public static function resetUserSalt($userId)
    {
        $user = User::get($userId);
        if (!$user) {
            throw new \think\Exception('找不到用户信息,请先注册', Api::ERR_CODE_401);
        }
        $salt = Encryption::uniqidStr();
        $user->save(['salt' => $salt]);
    }


    /**
     * @param $userId
     * @return mixed
     * @throws \think\Exception
     */
    public static function getUserSalt($userId)
    {
        $user = User::get($userId);
        if (!$user) {
            throw new \think\Exception('找不到用户信息,请先注册', Api::ERR_CODE_401);
        }
        if (empty($user->salt)) {
            $salt = Encryption::uniqidStr();
            $user->save(['salt' => $salt]);
        }
        return $user->salt;
    }

    /**
     * 生成jwt
     * @param $data array  携带信息，不要存放隐私信息，jwt可以保证内容不被修改，但可以被解码查看内容
     * @param $exptime int 过期时间
     * @return string
     */
    public static function createJwt($data, $exptime)
    {
        $nowtime = time();
        $token_data = [
            'iss' => PRODUCT_URL, //签发者
            'aud' => PRODUCT_URL, //jwt所面向的用户,这里应该填userID
            'iat' => $nowtime, //签发时间
            'nbf' => $nowtime + 10, //在什么时间之后该jwt才可用
            'exp' => $exptime, //过期时间，这个过期时间必须要大于签发时间
            'data' => $data //描述说明, 不要存放隐私信息，jwt可以保证内容不被修改，但可以被解码查看内容
        ];
        $jwt_key = getSalt();
        $jwt = JWT::encode($token_data, $jwt_key, 'HS256');
        return $jwt;
    }


    /**
     * 退出登入状态
     */
    public static function clear_session()
    {
        session(self::LOGIN_INFO, null);
        cookie(self::ACCESS_TOKEN, null);
        cookie(self::ACCESS_TOKEN_EXPIRE_TIME, null);
        cookie(self::REFRESH_TOKEN, null);
        cookie(self::REFRESH_TOKEN_EXPIRE_TIME, null);
    }
}