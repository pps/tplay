<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2024/8/22
 * Time: 15:54
 * 1464674022@qq.com
 */

namespace app\common\service;

/**
 * 常量配置
 * Class Constant
 * @package app\common\service
 */
class Constant
{
    const STATUS_OPEN = 1;//开启
    const STATUS_CLOSE = 0;//关闭

    const STATUS_WAIT = 0;
    const STATUS_PASS = 1;
    const STATUS_UNPASS = -1;

    const STATUS = [
        self::STATUS_WAIT => '待审',
        self::STATUS_PASS => '通过',
        self::STATUS_UNPASS => '拒绝'
    ];


}