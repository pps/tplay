<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/9/21
 * Time: 10:49
 */

namespace app\common\service;


use think\Db;
use think\Log;

class GenerationService
{

    /**
     * 表名 转 驼峰命名(类名)
     * @param $tableName
     * @return string
     */
    public static function tableName2Hump($tableName)
    {
        //去除表前缀
        $underLine = self::tableName2UnderLine($tableName);
        return self::underLine2Hump($underLine);
    }


    /**
     * 下划线命名 转 驼峰命名
     * @param $underLine
     * @return string
     */
    public static function underLine2Hump($underLine)
    {
        //拆分，首字母转大写
        $trems = explode('_', $underLine);
        $humpName = "";
        foreach ($trems as $trem) {
            $humpName .= ucfirst($trem);
        }
        return $humpName ?: 'DefaultName';
    }

    /**
     * 表名 转 下划线命名
     * @param $tableName
     * @return mixed
     */
    public static function tableName2UnderLine($tableName)
    {
        //去除表前缀即可
        $db_prefix = \think\Env::get("db_prefix", "");
        return str_replace($db_prefix, '', $tableName);
    }

    /**
     * 获取字段信息
     * @param $tableName
     * @return array
     */
    public static function getFieldsInfo($tableName)
    {
        $datas = Db::query("SHOW FULL COLUMNS FROM {$tableName}");
        //Field:字段名(必须是下划线命名)
        //Type:类型：int(11)、varchar(50)、tinyint(4)、mediumtext
        //Comment:注释
        //Default:默认值：null、0、''
        //Extra:额外的：auto_increment
        //Key:索引：PRI
        //Null:是否空：NO
        $infos = [];
        foreach ($datas as $k => $v) {
            $infos[$v['Field']] = $v;
        }
        return $infos;
    }


    //更新表注释
    public static function updateTableComment($tableName, $menuName, $crud, $tplNames)
    {
        //todo 判断是否要更新
        $comment = json_encode([
            'menuName' => $menuName,
            'crud' => $crud,
            'tplNames' => $tplNames
        ], JSON_UNESCAPED_UNICODE);
        $sql = "ALTER TABLE {$tableName} COMMENT='{$comment}'";
        Log::record("更新表注释:" . $sql);
        Db::execute($sql);
    }


    /**
     * 更新字段注释
     * @param $tableName
     * @param $datas
     * @return string
     */
    public static function updateFieldsComment($tableName, $datas)
    {
        $arr = [];
        foreach ($datas as $k => $v) {
            $comment = $v['Remark'];
            if (empty($comment)) {
                $config = [
                    'show_list' => $v['ShowList'] == 'on',
                    'show_search' => $v['ShowSearch'] == 'on',
                    'show_edit' => $v['ShowEdit'] == 'on',
                    'component' => $v['Component'],
                ];
            } elseif (!ifStartWith($comment, "{")) {
                $config = [
                    'comment' => $comment,
                    'show_list' => $v['ShowList'] == 'on',
                    'show_search' => $v['ShowSearch'] == 'on',
                    'show_edit' => $v['ShowEdit'] == 'on',
                    'component' => $v['Component'],
                ];
            } else {
                $config = json_decode($comment, true);
                $config = is_array($config) ? $config : [];
                $config['show_list'] = $v['ShowList'] == 'on';
                $config['show_search'] = $v['ShowSearch'] == 'on';
                $config['show_edit'] = $v['ShowEdit'] == 'on';
                $config['component'] = $v['Component'];
            }

            $comment = json_encode($config, JSON_UNESCAPED_UNICODE);
            if ($comment != $v['Remark']) {
                if (!empty($v['Extra'])) {
                    $arr[] = "MODIFY COLUMN {$v['Field']} {$v['Type']} NOT NULL {$v['Extra']} COMMENT '{$comment}'";
                } else {
                    $default = $v['Default'];
                    if ($default === null) {
                        $default = "";
                    } else {
                        $default = "DEFAULT '{$default}'";
                    }
                    $arr[] = "MODIFY COLUMN {$v['Field']} {$v['Type']} NOT NULL {$default} COMMENT '{$comment}'";
                }
            }
        }

        if ($arr) {
            $sql = "ALTER TABLE {$tableName} " . implode("," . PHP_EOL, $arr);
            Log::record("修改字段注释:" . $sql);
            Db::execute($sql);
        }
    }


    /**
     * 查询表是否存在数据库
     * @param $tableName
     * @return mixed 1 存在 0 不存在
     */
    public static function tableExists($tableName)
    {
        $db_name = \think\Env::get("db_name", "");
        $table_exists = Db::query("SELECT EXISTS (SELECT 1 FROM information_schema.tables  WHERE table_schema = '{$db_name}' AND table_name = '{$tableName}') AS `table_exists`;");
        return $table_exists[0]['table_exists'];
    }

    //生成中间表
    public static function createRelationTable($tableName, $fieldsInfo)
    {
        foreach ($fieldsInfo as $vo) {
            if ($vo['Component'] == 'xm-select') {
                //生成中间表
                $fieldName = $vo['Field'];
                $relationTableName = "{$tableName}_" . deleteEndDS($fieldName, '_id');
                $foreignKey = deleteStartDS("{$tableName}_id", "tplay_");

                if (!self::tableExists($relationTableName)) {
                    $sql = "CREATE TABLE IF NOT EXISTS `{$relationTableName}` (`{$fieldName}` INT(11) NOT NULL,`{$foreignKey}` INT(11) NOT NULL,PRIMARY KEY (`{$fieldName}`, `{$foreignKey}`) USING BTREE)COLLATE='utf8_general_ci' ENGINE=InnoDB;";
                    Log::record("生成中间表:" . $sql);
                    Db::execute($sql);
                }
            }
        }
    }
}