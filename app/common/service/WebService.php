<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/2/14
 * Time: 18:09
 */

namespace app\common\service;


use think\Cache;
use think\Controller;
use think\Log;
use think\Request;

class WebService extends Controller
{
    /**
     * 安装检测
     */
    public function checkInstalled()
    {
        if ($this->request->module() != 'install' && !isInstalled()) {
            // 检测是否安装过,未安装直接跳转到安装导向
            $this->redirect("/install");
        }
    }

    /**
     * 检查是否开启用户中心
     */
    public function checkOpenUserModule()
    {
        if (!\think\Env::get('is_open_user_module', false)) {
            $this->error('未开放用户模块哦', '/');
        }
    }

    /**
     * 记录请求的详细数据
     * @param string $msg [描述信息]
     * @return string
     */
    public function record($msg = "")
    {
        Log::record($msg, 'notice');
        if ($this->request->isGet() && $this->request->get()) {
            Log::record("GET PARAMS:  " . json_encode($this->request->get(), JSON_UNESCAPED_UNICODE), 'notice');
        } elseif ($this->request->isPost() && $this->request->param()) {
            Log::record("POST PARAMS:  " . json_encode($this->request->param(), JSON_UNESCAPED_UNICODE), 'notice');
        }
        $callbackBody = file_get_contents('php://input');
        if ($callbackBody) {
            Log::record("Body PARAMS:  " . $callbackBody, 'notice');
        }
        return $msg;
    }

    /**
     * 输出调试信息
     * @param $msg
     */
    public function info($msg)
    {
        if ($this->request->isCli()) {
            echo $msg . PHP_EOL;
        } else {
            Log::record($msg, 'alert');
        }
    }

    /**
     * 记录错误信息日志
     * @param $e \Exception
     */
    public static function errorlog($e)
    {
        Log::error("异常信息:" . $e->getMessage());
        Log::error('请求数据:' . var_export(Request::instance()->param(), true));
        Log::error($e->getTraceAsString());
    }

    /**
     * 数据缓存化
     * @param $name [缓存名称]
     * @param $callback [获取数据的方法]
     * @param int $expire [缓存时间:-1不缓存、0永久缓存、3600是一个小时]
     * @return mixed|string
     */
    public static function getByCache($name, $callback, $expire = 0)
    {
        if (\think\Env::get('app_debug', false)) {
            $expire = -1;
        }

        $key = 'WebService_getByCache_' . $name;
        if ($expire >= 0 && cache($key) !== false) {
            return cache($key);
        }

        $value = $callback();
        Log::record($key . ":" . json_encode($value, 320), 'cache');

        Cache::set($key, $value, $expire);
        return $value;
    }
}