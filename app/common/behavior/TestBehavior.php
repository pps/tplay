<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2024/9/29
 * Time: 19:02
 * 1464674022@qq.com
 */

namespace app\common\behavior;


use think\Log;

class TestBehavior
{
    // 应用初始化
    public function appInit(&$params)
    {
        Log::record('app_init: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 应用开始
    public function appBegin(&$params)
    {
        Log::record('app_begin: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 模块初始化
    public function moduleInit(&$params)
    {
        Log::record('module_init: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 控制器开始
    public function actionBegin(&$params)
    {
        Log::record('action_begin: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 视图输出过滤
    public function viewFilter(&$params)
    {
        Log::record('view_filter: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 应用结束
    public function appEnd(&$params)
    {
        Log::record('app_end: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 日志write方法
    public function logWrite(&$params)
    {
        Log::record('log_write: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 日志写入完成
    public function logWriteDone(&$params)
    {
//        Log::record('log_write_done: ' . microtime(true), 'TestBehavior');//影响很多地方
        return true;
    }

    // 响应发送
    public function responseSend(&$params)
    {
        Log::record('response_send: ' . microtime(true), 'TestBehavior');
        return true;
    }

    // 输出结束
    public function responseEnd(&$params)
    {
        Log::record('response_end: ' . microtime(true), 'TestBehavior');
        return true;
    }
}