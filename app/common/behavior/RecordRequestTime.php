<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2024/9/29
 * Time: 17:40
 * 1464674022@qq.com
 */

namespace app\common\behavior;


use think\Log;
use think\Request;
use think\Response;

class RecordRequestTime
{
    //控制器开始
    public function actionBegin(&$params)
    {
        // 获取请求对象
        // 每个请求都有一个唯一的请求对象，因此存储在请求对象属性中的开始时间不会受到其他请求的影响。
        $request = Request::instance();
        // 记录请求开始时间
        $request->startTime = microtime(true);
//        Log::record('记录请求开始时间：' . $request->startTime, 'alert');
        return true;
    }

    //输出结束
    public function responseEnd(&$params)
    {
        // 记录请求结束时间
        $endTime = microtime(true);
//        Log::record('记录请求结束时间：' . $endTime, 'alert');

        // 获取请求对象
        $request = Request::instance();
        // 从请求对象的属性中获取开始时间
        $startTime = $request->startTime??$endTime;
//        Log::record('请求开始时间：' . $startTime, 'alert');

        // 计算请求持续时间
        $duration = $endTime - $startTime;
//        Log::record('请求时长：' . $duration, 'alert');

        // 如果请求时间超过2秒，则记录日志
        if ($duration > 3) {
            // 记录日志
            Log::record("请求处理时间{$duration}秒：" . $request->url(), 'alert');
        }
        return true;
    }
}