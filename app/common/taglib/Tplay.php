<?php

namespace app\common\taglib;

use think\template\TagLib;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/1/14
 * Time: 13:51
 */
class Tplay extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'config' => ['attr' => 'rows,sort,where', 'close' => 0], //闭合标签，默认为不闭合
    ];

    /**
     * 这是一个闭合标签
     */
    public function tagConfig($tag)
    {
        $data = [];
        if (isset($tag['rows'])) {
            $data['articlelist_rows'] = $tag['rows'];
        }
        if (isset($tag['sort'])) {
            $data['articlelist_sort'] = $tag['sort'];
        }
        if (isset($tag['where'])) {
            $data['articlelist_where'] = $tag['where'];
        }
        return '<?php if($catalog){ $catalog->save(' . var_export($data, true) . ');} ?>';
    }
}