<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Test extends Command
{
    /**
     * php think tplay:Test -abc AAA BBB ccc
     */
    protected function configure()
    {
        $this->setName('tplay:Test')
            ->addOption('optionA', 'a', Option::VALUE_NONE, 'optionA')
            ->addOption('optionB', 'b', Option::VALUE_NONE, 'optionB')
            ->addOption('optionC', 'c', Option::VALUE_NONE, 'optionC')
            ->addArgument('argsA', Argument::OPTIONAL, "argsA", "argsA")
            ->addArgument('argsB', Argument::OPTIONAL, "argsB", "argsB")
            ->addArgument('argsC', Argument::OPTIONAL, "argsC", "argsC")
            ->setDescription('this is test');
    }

    protected function execute(Input $input, Output $output)
    {
        $optionA = $input->getOption("optionA");
        $optionB = $input->getOption("optionB");
        $optionC = $input->getOption("optionC");
        $output->writeln("options: ");
        var_dump($optionA, $optionB, $optionC);

        $argsA = $input->getArgument("argsA");
        $argsB = $input->getArgument("argsB");
        $argsC = $input->getArgument("argsC");
        $output->writeln("args: ");
        var_dump($argsA, $argsB, $argsC);
    }
}