<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\command;

use app\admin\model\Admin;
use app\common\model\Templet;
use file\DirHelper;
use think\console\input\Argument;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Db;
use think\Env;
use file\ZipHelper;

class InstallDemo extends Command
{
    /**
     * 安装测试数据
     */
    protected function configure()
    {
        $this->setName('tplay:InstallDemo')
            ->addOption('delete', 'd', Option::VALUE_NONE, 'delete data')//清空数据,仅测试时使用,线上使用后记得跑路
            ->addArgument('admin_pass', Argument::OPTIONAL, "admin password", "123456")//需要验证管理员密码
            ->addArgument('demo_name', Argument::OPTIONAL, "demo source name", "blog_demo")//安装包名
            ->setDescription('install test data & demo code');
    }

    protected function execute(Input $input, Output $output)
    {
        //验证管理员密码
        $admin = Admin::get(1);
        if (!$admin || $admin->password != password($input->getArgument("admin_pass"))) {
            $output->writeln("<error>admin pass error</error>");
            return;
        }


        if ($input->getOption("delete")) {

            //清空数据
            $prefix = \think\Env::get("db_prefix", "");
            Db::execute("TRUNCATE {$prefix}admin_log");
            Db::execute("TRUNCATE {$prefix}article");
            Db::execute("TRUNCATE {$prefix}article_cate");
            Db::execute("TRUNCATE {$prefix}attachment");
            Db::execute("TRUNCATE {$prefix}catalog");
            Db::execute("TRUNCATE {$prefix}cate_catalog");
            Db::execute("TRUNCATE {$prefix}config");
            Db::execute("TRUNCATE {$prefix}config_option");
            Db::execute("TRUNCATE {$prefix}config_tab");
            Db::execute("TRUNCATE {$prefix}messages");
            Db::execute("TRUNCATE {$prefix}point_log");
            Db::execute("TRUNCATE {$prefix}user");
            $output->writeln("delete success");

        } else {

            $demo_name = $input->getArgument("demo_name");
            $source = APP_PATH . "install/data/$demo_name.zip";
            if (!file_exists($source)) {
                $output->error("demo source not exists");
                exit;
            }

            //解压
            $dest = APP_PATH . 'install/data/';
            if (false === (new ZipHelper())->unzip($source, $dest)) {
                $output->error("unzip fail");
                exit;
            }

            //复制static
            $source = APP_PATH . "install/data/$demo_name/static";
            $dest = Templet::DEFAULT_STATIC_PATH . 'static';
            DirHelper::copyDir($source, $dest);

            //复制template
            $source = APP_PATH . "install/data/$demo_name/template";
            $dest = Templet::getTempletDir();
            DirHelper::copyDir($source, $dest);

            //connect db
            $db_prefix = Env::get('db_prefix', 'tplay_');
            $db_name = Env::get('db_name', 'tplay');
            $search = [
                "tplay_",
                "USE `tplay`",
                "CREATE DATABASE IF NOT EXISTS `tplay`",
            ];
            $replace = [
                $db_prefix,
                "USE `" . $db_name . "`",
                "CREATE DATABASE IF NOT EXISTS `" . $db_name . "`",
            ];

            //导入sql
            $demo_sql = file_get_contents(APP_PATH . "install/data/$demo_name/db.sql");
            $sql_array = preg_split("/;[\r\n]+/", str_replace($search, $replace, $demo_sql));
            foreach ($sql_array as $k => $v) {
                if (!empty($v)) {
                    Db::query($v);
                }
            }
            $output->writeln("install success");
        }
    }
}