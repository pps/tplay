<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\command;

use app\common\model\Article;
use app\common\model\Catalog;
use app\common\service\Constant;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Db;

class ImportDedecms extends Command
{
    /**
     * 导入dedecms数据
     */
    protected function configure()
    {
        $this->setName('tplay:ImportDedecms')
            ->addArgument('db_name', Argument::OPTIONAL, "db_name", "dedecms")
            ->addArgument('db_prefix', Argument::OPTIONAL, "db_prefix", "dede_")
            ->setDescription('import dedecms data to tplay data');
    }

    protected function execute(Input $input, Output $output)
    {
        $db_name = $input->getArgument("db_name");
        $db_prefix = $input->getArgument("db_prefix");

        $dedecms = Db::connect([
            // 数据库类型
            'type' => 'mysql',
            // 服务器地址
            'hostname' => \think\Env::get("db_host", "localhost"),
            // 数据库名
            'database' => $db_name,
            // 数据库用户名
            'username' => \think\Env::get("db_username", "root"),
            // 数据库密码
            'password' => \think\Env::get("db_password", ""),
            // 数据库连接端口
            'hostport' => \think\Env::get("db_port", "3306"),
        ]);


        //导入栏目
        $arctype = $dedecms->query("select * from {$db_prefix}arctype");
        foreach ($arctype as $k => $v) {
            if (Catalog::get($v['id'])) {
                continue;
            }
            (new Catalog())->save([
                'id' => $v['id'],
                'pid' => $v['reid'],
                'title' => $v['typename'],
                'path' => str_replace('{cmspath}', '', appendEndDS($v['typedir'], '/')) . $v['defaultname'],
                'sort' => $v['sortrank'],
                'type' => $v['ispart'] == 0 ? Catalog::TYPE_ARTICLE_LIST : Catalog::TYPE_CATALOG,
                'catalog_templet' => str_replace(['{style}/', '.htm'], '', $v['ispart'] == 0 ? $v['templist'] : $v['tempindex']),
                'article_templet' => str_replace(['{style}/', '.htm'], '', $v['temparticle']),
                'article_rule' => str_replace('{typedir}', '{cpath}', $v['namerule']),
                'articlelist_rule' => str_replace('{typedir}', '{cpath}', $v['namerule2']),
                'seo_title' => $v['seotitle'],
                'seo_keyword' => $v['keywords'],
                'seo_description' => $v['description'],
            ]);
            $output->writeln("import Catalog id:" . $v['id']);
        }


        //导入文章
        $archives = $dedecms->query("select * from {$db_prefix}archives where arcrank <> -2");
        foreach ($archives as $k => $v) {
            if (Article::get($v['id'])) {
                continue;
            }

            //文章内容
            $addonarticle = $dedecms->query("select * from {$db_prefix}addonarticle where aid=?", [$v['id']]);
            $content = $addonarticle[0]['body'];

            //文章标签
            $taglist = $dedecms->query("select * from {$db_prefix}taglist where aid=?", [$v['id']]);
            $tags = [];
            foreach ($taglist as $tag) {
                $tags[] = $tag['tag'];
            }

            (new Article())->save([
                'id' => $v['id'],
                'title' => $v['title'],
                'seo_title' => $v['shorttitle'],
                'seo_keyword' => $v['keywords'],
                'seo_description' => $v['description'],
                'filename' => $v['filename'],
                'page_views' => $v['click'],
                'catalog_id' => $v['typeid'],
                'content' => $content,
                'tag' => implode(',', $tags),
                'status' => $v['arcrank'] == -1 ? Constant::STATUS_CLOSE : Constant::STATUS_OPEN,
                'istop_time' => ifContain($v['flag'], 'c') ? time() : 0,
                'create_time' => $v['senddate'],
                'update_time' => $v['pubdate']
            ]);

            $output->writeln("import Article id:" . $v['id']);
        }
    }
}