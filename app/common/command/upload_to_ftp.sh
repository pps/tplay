#!/bin/bash

# 配置项目根路径
webroot="/wwwroot/tplay"
# CMS生成的HTML目录
path_article_static = "${webroot}/public/uploads/q1464674022"

# 配置ftp
host="127.0.0.1"
user="xxxxxx"
pwd="xxxxxx"
port="21"
ftproot="/"


# 生成文章html
su -c "php ${webroot}/public/index.php admin/catalog/exportHtml" -s /bin/sh www

# 重建webUpdate目录
rm -rf ${webroot}/public/uploads/webUpdate/*
su -c "mkdir -p ${webroot}/public/uploads/webUpdate" -s /bin/sh www
rm -f ${webroot}/public/uploads/webUpdate.gz

# 复制path_article_static目录中一天内更新的文件到webUpdate目录
cd ${path_article_static}
find ./ -mtime -1 -type f |xargs tar -zcvf ${webroot}/public/uploads/webUpdate.gz > /dev/null
su -c "tar -zxvf ${webroot}/public/uploads/webUpdate.gz -C ${webroot}/public/uploads/webUpdate" -s /bin/sh www

# 删除不上传的文件
rm -f ${webroot}/public/uploads/webUpdate/robots.txt

# 上传ftp覆盖
/usr/local/ncftp/bin/ncftpput -m -R -u ${user} -p ${pwd} -P ${port} ${host} ${ftproot} ${webroot}/public/uploads/webUpdate/*

# 生成日志
echo "success $(date +%Y-%m-%d_%H:%M:%S)" >> ${webroot}/public/uploads/webUpdate_$(date +%Y-%m).log
du -sh ${webroot}/public/uploads/webUpdate/ >> ${webroot}/public/uploads/webUpdate_$(date +%Y-%m).log
find ${webroot}/public/uploads/webUpdate/ -type f | wc -l |xargs echo "files=" >> ${webroot}/public/uploads/webUpdate_$(date +%Y-%m).log

