<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\command;

use app\common\model\Article;
use app\common\model\Catalog;
use app\common\service\CmsService;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class ExportTagList extends Command
{
    /**
     * 批量生成标签页html
     */
    protected function configure()
    {
        $this->setName('tplay:ExportTagList');
    }

    protected function execute(Input $input, Output $output)
    {
        $catalog = (new Catalog())->where('title', '标签页')->find();
        /** @var Catalog $catalog */
        if(!$catalog){
            exit('没有配置标签页栏目');
        }

        $articles = (new Article())->where('tag', '<>', '')->select();
        $tags = [];
        foreach ($articles as $article) {
            /** @var Article $article */
            $tags = array_merge($tags, $article->getTags());
        }
        $tags = array_unique($tags);

        $service = new CmsService();
        $catalogPath = $catalog->getRealPath();
        foreach ($tags as $tag) {
            $catalog2TagPath = str_replace(['index', ' '], [$tag, '_'], $catalogPath);
            $service->info("标签页:" . $catalog2TagPath);
            $catalog->tag = $tag;//通过动态变量传递数据
            $content = $service->perviewCatalog($catalog);
            $service->createFile($catalog2TagPath, $content);
        }
    }
}