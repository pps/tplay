<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2025/2/20
 * Time: 16:47
 */

namespace app\common\command;

use file\CsvHelper;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Db;
use time\DateHelper;

class CreateTableByCsv extends Command
{
    protected function configure()
    {
        $this->setName('tplay:CreateTableByCsv')
            ->addArgument('filepath', Argument::OPTIONAL, "filepath", ".");
    }

    protected function execute(Input $input, Output $output)
    {
        $filepath = $input->getArgument("filepath");//csv文件路径，点表示当前目录
        $csv_data = CsvHelper::csvToArray($filepath);

        $prefix = \think\Env::get("db_prefix", "");
        $tablename = 'csv' . DateHelper::getNow('Ymd');

        $sql = "CREATE TABLE IF NOT EXISTS `{$prefix}{$tablename}` (
`id` INT(11) NOT NULL AUTO_INCREMENT,";

        foreach ($csv_data as $line) {
            foreach ($line as $k => $row) {
                $sql .= "`field{$k}` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '{$row}' COLLATE 'utf8_general_ci',";
            }
            break;
        }

        $sql .= "PRIMARY KEY (`id`) USING BTREE
) COMMENT=''
COLLATE='utf8_general_ci'
ENGINE=InnoDB;";

        Db::execute($sql);
        var_dump($sql);
    }
}