<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use \think\Model;

class PointLog extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;

    const TYPE_ADMIN_UPDATE = 1;
    const TYPE_REGISTER = 2;
    const TYPE_RECHARGE = 3;
    const TYPE_SIGN_IN = 4;

    const TYPES = [
        self::TYPE_ADMIN_UPDATE => '管理员修改',
        self::TYPE_REGISTER => '赠送（注册）',
        self::TYPE_RECHARGE => '购买/充值',
        self::TYPE_SIGN_IN => '赠送（签到）',
    ];

    //type_text
    public function getTypeTextAttr($value, $data)
    {
        return self::TYPES[$data['type']]??'';
    }
}
