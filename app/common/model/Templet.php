<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;

use file\FileHelper;
use think\Env;

class Templet
{
    //模板目录
    const DEFAULT_TEMPLET_PATH = ROOT_PATH . "app" . DS . 'common' . DS . 'view' . DS . 'template' . DS;
    //上传文件目录
    const UPLOAD_PATH = ROOT_PATH . "public" . DS . "uploads" . DS;
    //默认HTML目录
    const DEFAULT_STATIC_PATH = self::UPLOAD_PATH . 'q1464674022' . DS;

    private $name;

    /**
     * 设置文件名，并返回模板对象
     * @param $name
     * @return $this
     * @throws \Exception
     */
    public function setName($name)
    {
        $this->name = trim($name);
        $filepath = $this->getRealPath();
        if (!file_exists($filepath)) {
            throw new \Exception($filepath . '模板文件不存在');
        }
        return $this;
    }

    /**
     * 模板文件路径
     * @return string
     */
    public function getRealPath()
    {
        return self::getTempletDir() . $this->name . '.html';
    }

    /**
     * 读取模板内容
     * @return mixed
     */
    public function getContent()
    {
        return FileHelper::read($this->getRealPath());
    }

    /**
     * cms静态化目录，生成静态页的根目录 ，需要写权限
     * @return string
     */
    public static function getRootDir()
    {
        $path = Env::get('path_article_static', '');
        return empty($path) ? self::DEFAULT_STATIC_PATH : appendEndDS($path);
    }

    /**
     * 模板目录
     * @return string
     */
    public static function getTempletDir()
    {
        $path = Env::get('path_article_templet', '');
        return empty($path) ? self::DEFAULT_TEMPLET_PATH : appendEndDS($path);
    }


    /**
     * 获取所有模板信息
     * @param string $name [过滤文件名]
     * @return array
     */
    public static function getTemplets($name = "")
    {
        //定义要查看的目录
        $rootpath = self::getTempletDir();
        $files = [];
        if (!file_exists($rootpath)) {
            return $files;
        }
        //遍历目录
        $handle = opendir($rootpath);
        while (false !== ($f = readdir($handle))) {
            //过滤点
            if ($f == "." || $f == "..") {
                continue;
            }
            //拼路径
            $filepath = $rootpath . $f;

            //防止中文乱码
//            $filename = iconv("gb2312", "utf-8", $f);
            $filename = $f;

            $info = pathinfo($filepath);
            $ext = $info['extension']??'';
            $filename = str_replace("." . $ext, "", $filename);
            //过滤查询
            if (isset($name) and !empty($name)) {
                if (!ifContain($filename, $name)) {
                    continue;
                }
            }
            //过滤文件夹
            if (filetype($filepath) == "dir") {
                continue;
            }
            $files[] = [
                "filename" => $filename,//文件名
                "fileext" => $ext,//扩展名
                "filetype" => filetype($filepath),//文件or文件夹
                "filesize" => filesize($filepath),
                "create_time" => date('Y-m-d H:i:s', filectime($filepath)),
                "update_time" => date('Y-m-d H:i:s', filemtime($filepath)),
            ];
        }
        IS_WIN ? asort($files) : sort($files);
        return $files;
    }

}
