<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use think\Env;
use think\Model;

class Emailconfig extends Model
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;

    //发送邮箱激活邮件
    public static function sendemail($to_email, $code, $token)
    {
        $activation_url = url('user/common/activation', '', false, true) . '?code=' . $code . '&name=' . $to_email . '&__token__=' . $token;
        $data = self::get(1);
        $title = $data['title'];
        $content = str_replace(['{nickname}', '{passport}', '{activation_url}', '{email}', '{time}'], [$to_email, $to_email, $activation_url, $to_email, date('Y-m-d H:i:s', time())], $data['content']);
        $from_email = $data['from_email'];
        $from_name = $data['from_name'];
        $smtp = $data['smtp'];
        $username = $data['username'];
        $password = $data['password'];

        list($res, $error) = sendMail($to_email, $title, $content, $from_email, $from_name, $smtp, $username, $password);
        return $res;
    }

    //from_email
    public function getFromEmailAttr($value, $data)
    {
        return $value ?: Env::get('email_account', '');
    }

    //username
    public function getUsernameAttr($value, $data)
    {
        return $value ?: Env::get('email_username', '');
    }

    //password
    public function getPasswordAttr($value, $data)
    {
        return $value ?: Env::get('email_password', '');
    }
}
