<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use think\Model;

class ConfigOption extends Model
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;

    //关联配置
    public function config()
    {
        return $this->belongsTo('Config', 'pid');
    }
}
