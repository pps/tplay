<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;

use app\common\service\Constant;
use think\Model;

class Attachment extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;

    //status_text
    public function getStatusTextAttr($value, $data)
    {
        return Constant::STATUS[$data['status']]??'';
    }

    //audit_time
    public function getAuditTimeAttr($value, $data)
    {
        return $value ? date('Y-m-d H:i:s', $value) : '';
    }
}
