<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;

use app\common\service\Constant;
use file\PathHelper;
use string\Format;
use think\Exception;

class ArticleCate extends Tree
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;


    //status_text 分类状态
    public function getStatusTextAttr($value, $data)
    {
        $status = [0 => '隐藏', 1 => '显示'];
        return $status[$data['status']];
    }

    //article_count 文章数量
    public function getArticleCountAttr($value, $data)
    {
        return $this->hasMany('Article')->where('status', Constant::STATUS_OPEN)->count();
    }

    /**
     * 新增或编辑节点时 更新tree_path字段
     * @param $pid
     * @return mixed
     * @throws Exception
     */
    public function updateTreePath($pid)
    {
        if ($this->id == $pid) {
            throw new Exception('上级节点不能选自己');
        }
        if ($pid) {
            /** @var Tree $pcate */
            $pcate = (new self())->find($pid);
            if (!$pcate) {
                throw new Exception('该上级节点不存在，请重新选择');
            }
            if (in_array($this->id, $pcate->getParentsIds())) {
                throw new Exception('上级节点不能选自己下级');
            }
            $new_tree_path = $pcate->tree_path . '-' . $this->id;
        } else {
            $new_tree_path = $this->id;
        }

        //新增或有变化时
        if (!isset($this->tree_path) || $new_tree_path != $this->tree_path) {
            if (false == $this->save(['tree_path' => $new_tree_path])) {
                throw new Exception('更新tree_path失败');
            }
        }
    }

    /**
     * cms发布路径
     * 如果存在上级英文名，则拼接上级目录
     * 如果没有分类英文名称，则使用分类拼音
     * @param bool $auto 是否自动生成英文别名
     * @return mixed|string
     */
    public function tpath($auto = false)
    {
        $tpath = "";
        if ($this->pid) {
            $pnode = $this->getDirectParentNode();
            if ($pnode && $pnode->tpath()) {
                $tpath = $pnode->tpath();
            }
        }
        $enname = $this->en_name;
        if (!$this->en_name && $auto) {
            $enname = Format::toPinyin($this->getData('title'));
        }
        if ($tpath && $enname) {
            return $tpath . appendStartDS($enname, '/');
        } else {
            return $enname ?: $tpath;
        }
    }

    /**
     * 按cms规则生成路径
     * @param $catalogPath [uri使用相对路径,生成文件用物理路径]
     * @param $rule [列表路径规则]
     * @param $cid [栏目id]
     * @param $tid [分类id]
     * @param $page [第几页]
     * @return mixed
     * @throws \Exception
     */
    public static function getRulePath($catalogPath, $rule, $cid, $tid, $page = 1)
    {
        if (empty($rule)) {
            throw new \Exception("null articlesRule error");
        }
        $rule = replaceDS($rule);//处理分隔符
        $search = ["{cpath}", "{cid}", "{tid}", "{page}", "{tpath}"];
        $replace = [
            PathHelper::getDir($catalogPath),
            $cid,
            $tid,
            $page,
            $tid && ifContain($rule, "{tpath}") ? self::get($tid)->tpath() : "",
        ];
        $rulePath = replaceDS(str_replace($search, $replace, $rule));

        if (ifContain($catalogPath, Templet::getRootDir())) {
            //生成操作时,处理路径不在根目录里的
            if (!ifContain($rulePath, Templet::getRootDir()))
                $rulePath = Templet::getRootDir() . deleteStartDS($rulePath);
        } else {
            //获取URI时,处理路径{aid}.html
            $rulePath = appendStartDS($rulePath);
        }
        return $rulePath;
    }

    /**
     * 获取分类 cms uri
     * @param int $catalogId [分类设置了多个栏目时，默认选择首个栏目]
     * @return mixed
     * @throws \Exception
     */
    public function getUri($catalogId = 0)
    {
        $catalogs = $this->catalogs;
        if (!$catalogs) {
            throw new \Exception("分类{$this->id}找不到对应的栏目");
        }
        $catalog = $catalogs[0];
        if (count($catalogs) > 0) {
            foreach ($catalogs as $catalog_) {
                if ($catalog_->id == $catalogId)
                    $catalog = $catalog_;
            }
        }
        return replaceUrlDS(self::getRulePath($catalog->getUri(), $catalog->articlelist_rule, $catalog->id, $this->id));
    }

    /**
     * 分类设置了多个栏目时，通过指定路径获取 cms uri
     * @param string $path_pattern
     * @return mixed|string
     */
    public function getUriByPattern($path_pattern = "/m/")
    {
        $catalogs = $this->catalogs;
        foreach ($catalogs as $catalog) {
            if (ifContain($catalog->path, $path_pattern)) {
                return $this->getUri($catalog->id);
            }
        }
        return $this->getUri();
    }

    /**
     * 获取多个栏目属性集合
     * @param string $field
     * @return string
     */
    public function getCatalogValues($field = "title")
    {
        $catalogs = $this->catalogs;
        if ($catalogs) {
            $name = [];
            foreach ($catalogs as $catalog) {
                $name[] = $catalog->getData($field);
            }
            return implode(',', $name);
        }
        return "";
    }


    //获取一个栏目
    public function getFirstCatalog()
    {
        $catalogs = $this->catalogs;
        if (empty($catalogs)) {
            throw new \Exception("分类{$this->id}找不到对应的栏目");
        }
        //有多个栏目时，默认第一个栏目，排序最大的
        return $catalogs[0];
    }


    //关联文章
    public function articles()
    {
        return $this->hasMany('Article')->where('status', Constant::STATUS_OPEN);
    }

    //关联栏目
    public function catalogs()
    {
        return $this->belongsToMany('Catalog', 'app\common\model\CateCatalog')->order("sort desc,id");
    }
}
