<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;


use app\common\service\Constant;

class Messages extends Tree
{
    //定义Tree属性名称
    public $fieldPid = 'to_msg_id';

    //定义type值
    const TYPE_SYSTEM = 0;//系统公告
    const TYPE_ADMIN = 1;//管理员消息
    const TYPE_MESSAGE = 2;//留言


    //status_text
    public function getStatusTextAttr($value, $data)
    {
        return Constant::STATUS[$data['status']]??'';
    }

    //read_text
    public function getReadTextAttr($value, $data)
    {
        $reads = ['未读', '已读'];
        return $reads[$data['read']]??'';
    }

    //message
    public function getMessageAttr($value, $data)
    {
        return htmlspecialchars($value);//防止xss
    }

    /**
     * 截取N个字
     * @return mixed|string
     */
    public function getShortText($len = 50)
    {
        if (mb_strlen($this->message, "utf-8") > $len) {
            return subStrCN($this->message, $len) . "...";
        }
        return $this->message;
    }

    /**
     * 本消息是否有人回复
     * @return bool
     */
    public function hasReply()
    {
        return !empty($this->tree_path);
    }

    //获取父级的留言
    public function getPmsgs()
    {
        if ($this->hasReply()) {
            $pIds = $this->getParentsIds(false);
            $pmsgs = (new Messages())->where(['id' => ['in', $pIds], 'status' => Constant::STATUS_PASS])->order("tree_path desc")->select();
            return $pmsgs;
        } else {
            return [];
        }
    }

    /**
     * 获取本消息的回复个数
     */
    public function replyCount()
    {
        return $this->hasMany('Messages', 'to_msg_id')->where('status', Constant::STATUS_PASS)->count();
    }

    //关联管理员的回复
    public function adminReplys()
    {
        return $this->hasMany('Messages', 'to_msg_id')->where(['status' => Constant::STATUS_PASS, 'type' => self::TYPE_ADMIN]);
    }

    //关联发送人
    public function fromUser()
    {
        return $this->belongsTo('app\common\model\User', 'from_user_id');
    }

    //关联接收人
    public function toUser()
    {
        return $this->belongsTo('app\common\model\User', 'to_user_id');
    }
}
