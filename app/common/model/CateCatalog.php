<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;

use think\model\Pivot;

/**
 * 分类和栏目关系表 - 多对多
 * Class CateCatalog
 * @package app\common\model
 */
class CateCatalog extends Pivot
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;
}