<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;

class FileManageModel
{
    /**
     * 文件管理根目录
     * @return string
     */
    public static function getRootDir()
    {
        return Templet::getRootDir();
    }

    /**
     * 获取所有文件信息
     * @param string $name [过滤文件名]
     * @param string $relativePath [过滤子目录]
     * @return array
     */
    public static function getFileList($name = "", $relativePath = "")
    {
        //定义要查看的目录
        $rootpath = self::getRootDir() . DS . $relativePath;

        $files = [];
        if (!file_exists($rootpath)) {
            return $files;
        }
        //遍历目录
        $handle = opendir($rootpath);
        while (false !== ($f = readdir($handle))) {
            //过滤点
            if ($f == "." || $f == "..") {
                continue;
            }
            //拼路径
            $filepath = rtrim($rootpath, DS) . DS . $f;

            /**
             * 中文乱码问题，中文必须utf-8编码才能显示
             * 1.尽量不用中文命名文件
             * 2.windows是bgk编码，生成的文件上传到linux，就会有问题
             * 3.PHP.ini配置默认是 default_charset = "utf-8 或  gbk"，修改配置需要重启
             * 4.转码可能导致其他地方有问题，根据需要多次转码，可以解决乱码
             */
            $filename = $f;
            //过滤
            if (isset($name) and !empty($name)) {
                if (!ifContain($filename, $name)) {
                    continue;
                }
            }

            $info = pathinfo($filepath);
            $ext = $info['extension']??'';
            $filename = replaceOnce("." . $ext, "", $filename);

            $key = filetype($filepath) . '-' . $ext . '-' . $filename;
            $files[$key] = [
                "filename" => $filename,//文件名
                "fileext" => $ext,//扩展名
                "filetype" => filetype($filepath),//文件or文件夹
                "filesize" => filesize($filepath),
                "fileatime" => date('Y-m-d H:i:s', fileatime($filepath)),//最后访问的时间
                "filectime" => date('Y-m-d H:i:s', filectime($filepath)),//最后改变的时间
                "filemtime" => date('Y-m-d H:i:s', filemtime($filepath)),//最后修改的时间,是文件的内容改变
            ];
        }
        //按文件类型和文件名排序
        ksort($files);
        //重置索引为数字，否則layui表格全选不支持勾选
        $data = [];
        foreach ($files as $k => $v) {
            $data[] = $v;
        }
        return $data;
    }
}
