<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/6/11
 * Time: 14:07
 */

namespace app\common\model;

use think\Cache;
use think\Db;
use think\Model;

class Webconfig extends Model
{
    private static $web_config;

    const EDITORS = ["wangEditor", "tinymce", "ueditor", "markdown"];

    /**
     * 获取数据库的配置项
     * @param $name [配置名]
     * @param int $expire [缓存时间:-1不缓存、0永久缓存、3600是一个小时]
     * @return mixed
     */
    public static function getValue($name, $expire = -1)
    {
        $key = 'web_config_cache_' . $name;
        if ($expire >= 0 && cache($key) !== false) {
            return cache($key);
        }
        if (!self::$web_config) {
            self::$web_config = Db::name('webconfig')->where('id', 1)->find();
        }
        $value = self::$web_config[$name]??'';
        if ($expire >= 0) {
            Cache::set($key, $value, $expire);
        }
        return $value;
    }


    /**
     * 获取.env文件中配置项
     * @param $name [配置名]
     * @param  string $default 默认值
     * @param int $expire [缓存时间:-1不缓存、0永久缓存、3600是一个小时]
     * @return mixed
     */
    public static function getEnvValue($name, $default = null, $expire = -1)
    {
        $key = 'web_evn_config_cache_' . $name;
        if ($expire >= 0 && Cache::get($key) !== false) {
            return Cache::get($key);
        }

        $value = $default;
        if (is_file(ROOT_PATH . '.env')) {
            $env = parse_ini_file(ROOT_PATH . '.env', true);//返回的值都是字符串或数组
            $value = $env[$name]??$default;
        }

        if ($expire >= 0) {
            Cache::set($key, $value, $expire);
        }

        return $value;
    }


    /**
     * 修改.env 文件中的配置项
     * @param $name [配置名]
     * @param $value [新的配置值]
     * @return bool
     */
    public static function setEnvValue($name, $value)
    {
        $env_content = file_get_contents(ROOT_PATH . ".env");

        $lines = explode("\n", $env_content);//拆分

        $newlines = [];
        $changeline = false;
        foreach ($lines as $line) {
            $line = trim($line);
            if (!empty($line) && ifContain($line, $name) && trim(explode('=', $line)[0]) == $name) {
                $newlines[] = $name . " = " . $value;
                $changeline = true;//修改
            } else {
                $newlines[] = $line;
            }
        }

        if (!$changeline) {
            $newlines[] = $name . " = " . $value;
            $changeline = true;//新增
        }

        //更新文件
        $new_env_content = "";
        foreach ($newlines as $k => $line) {
            if (count($newlines) == $k + 1) {
                $new_env_content .= $line;
            } else {
                $new_env_content .= $line . PHP_EOL;
            }
        }
        file_put_contents(ROOT_PATH . ".env", $new_env_content);

        //删除缓存
        $key = 'web_evn_config_cache_' . $name;
        Cache::rm($key);
        return $changeline;
    }
}