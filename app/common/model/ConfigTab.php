<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use app\common\service\Constant;
use think\Model;

class ConfigTab extends Model
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;


    //name_label
    public function getNameLabelAttr($value, $data)
    {
        return $value ?: '配置名';
    }

    //value_label
    public function getValueLabelAttr($value, $data)
    {
        return $value ?: '配置值';
    }

    //desc_label
    public function getDescLabelAttr($value, $data)
    {
        return $value ?: '备注';
    }

    //image_label
    public function getImageLabelAttr($value, $data)
    {
        return $value ?: '配图';
    }

    //color_label
    public function getColorLabelAttr($value, $data)
    {
        return $value ?: "配色";
    }

    //value_open
    public function getValueOpenAttr($value, $data)
    {
        return $value == 1 ? true : false;
    }

    //desc_open
    public function getDescOpenAttr($value, $data)
    {
        return $value == 1 ? true : false;
    }

    //image_open
    public function getImageOpenAttr($value, $data)
    {
        return $value == 1 ? true : false;
    }

    //color_open
    public function getColorOpenAttr($value, $data)
    {
        return $value == 1 ? true : false;
    }

    //pid_open
    public function getPidOpenAttr($value, $data)
    {
        return $value == 1 ? true : false;
    }

    /**
     * 获取tabs显示到系统设置
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function showTabs()
    {
        return $this->where('status', Constant::STATUS_OPEN)->order('sort desc')->select();
    }

    /**
     * 获取所有tabs
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function allTabs()
    {
        return $this->order('sort desc')->select();
    }

    //关联配置
    public function configs()
    {
        return $this->hasMany('Config');
    }
}
