<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use app\common\service\Constant;
use string\Encryption;
use think\Model;

class User extends Model
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;

    const TYPE_ACCOUNT = 0;
    const TYPE_WECHAT = 1;
    const TYPE_QQ = 2;
    const TYPE_PHONE = 3;
    const TYPE_EMAIL = 4;

    const USER_TYPES = [
        self::TYPE_ACCOUNT => '账号密码',
        self::TYPE_WECHAT => '微信',
        self::TYPE_QQ => 'QQ',
        self::TYPE_PHONE => '手机号',
        self::TYPE_EMAIL => '邮箱',
    ];

    const CATE_USER = 0;
    const CATE_AGENT = 1;
    const CATE_MANAGE = 2;

    const USER_CATES = [
        self::CATE_USER => '普通用户',
        self::CATE_MANAGE => '网站管理',
    ];

    const STATUS = [
        Constant::STATUS_WAIT => '待激活',
        Constant::STATUS_PASS => '已激活',
        Constant::STATUS_UNPASS => '已被封'
    ];

    const GRADE = ["非会员", "月度会员", "季度会员", "年度会员"];

    /**
     * 是否为会员
     * @return bool
     */
    public function isVip()
    {
        return !empty($this->level) && !$this->isVipExpire();
    }

    /**
     * 是否过期，如果stop_time为空不算过期
     * @return bool
     */
    public function isVipExpire()
    {
        if (empty($this->stop_time))
            return false;
        return $this->stop_time < time();
    }

    /**
     * 是否为代理
     * @return bool
     */
    public function isAgent()
    {
        return $this->user_cate == self::CATE_AGENT;
    }

    /**
     * 是否为管理
     * @return bool
     */
    public function isManage()
    {
        return $this->user_cate == self::CATE_MANAGE;
    }

    /**
     * 计算会员到期时间
     * @param $vipId
     * @return false|int|mixed
     */
    public function createStopTime($vipId)
    {
        $starttime = $this->isVip() ? $this->stop_time : time();
        switch ($vipId) {
            case 1: //月
                return $starttime + 31 * 24 * 3600;
            case 2: //季度
                return $starttime + 93 * 24 * 3600;
            case 3: //年
                return strtotime('+1 year', $starttime);
            default:
                return $starttime;
        }
    }

    //nickname
    public function getNicknameAttr($value, $data)
    {
        return htmlspecialchars($value);
    }

    //level_text
    public function getLevelTextAttr($value, $data)
    {
        return self::GRADE[$data['level']]??'';
    }

    //status_text
    public function getStatusTextAttr($value, $data)
    {
        return self::STATUS[$data['status']]??'';
    }

    //stop_time_text
    public function getStopTimeTextAttr($value, $data)
    {
        if (empty($this->level)) {
            return '';
        }
        if (empty($this->stop_time)) {
            return '永久';
        }
        if ($this->stop_time < time()) {
            return '已过期';
        }
        return $this->stop_time ? date('Y-m-d H:i:s', $this->stop_time) : '';
    }

    //register_time
    public function getRegisterTimeAttr($value, $data)
    {
        return $value ? date('Y-m-d H:i:s', $value) : '';
    }

    //login_time
    public function getLoginTimeAttr($value, $data)
    {
        return $value ? date('Y-m-d H:i:s', $value) : '';
    }

    //user_type_text 账号类型
    public function getUserTypeTextAttr($value, $data)
    {
        return self::USER_TYPES[$data['user_type']]??"";
    }

    //user_cate_text 用户角色
    public function getUserCateTextAttr($value, $data)
    {
        return self::USER_CATES[$data['user_cate']]??"";
    }

    //invite_code 邀请码
    public function getInviteCodeAttr($value, $data)
    {
        return Encryption::inviteEncode($this->id);
    }

    //child_count 邀请人数
    public function getChildCountAttr()
    {
        return $this->hasMany('User', 'pid')->where('status', Constant::STATUS_PASS)->count();
    }

    //推荐人
    public function parent()
    {
        return $this->belongsTo('User', 'pid');
    }
}
