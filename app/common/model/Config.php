<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\common\model;

use app\common\service\Constant;
use think\Model;

class Config extends Model
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;

    const TYPE_INPUT = 0;//固定值
    const TYPE_SELECT = 1;//多选
    const TYPE_RADIO = 2;//单选
    const TYPE_REDIRECT = 3;

    const TYPES = [
        self::TYPE_INPUT => '固定值',
        self::TYPE_SELECT => '多选',
        self::TYPE_RADIO => '单选',
        self::TYPE_REDIRECT => '跳转',
    ];

    //value_text
    public function getValueTextAttr($value, $data)
    {
        if ($this->getData('type') == self::TYPE_INPUT || $this->getData('type') == self::TYPE_REDIRECT) {
            return $this->value;
        } elseif ($this->getData('type') == self::TYPE_SELECT) {
            //多选，显示已配置个数
            $count = (new ConfigOption())->where('pid', $this->id)->where('status', Constant::STATUS_OPEN)->count();
            return $count . '个';
        } elseif ($this->getData('type') == self::TYPE_RADIO) {
            //显示单选值
            return (new ConfigOption())->where('pid', $this->id)->where('single_status', Constant::STATUS_OPEN)->column('name');
        } else {
            return '';
        }
    }

    //type_text
    public function getTypeTextAttr($value, $data)
    {
        return self::TYPES[$this->getData('type')];
    }

    //tab_text
    public function getTabTextAttr($value, $data)
    {
        if (!$this->tab) {
            return "";
        } else {
            return $this->tab->getData('name')??'';
        }
    }

    //name_label
    public function getNameLabelAttr($value, $data)
    {
        if ($value) {
            return $value;
        } else {
            return $this->tab->name_label??'';
        }
    }

    //value_label
    public function getValueLabelAttr($value, $data)
    {
        if ($value) {
            return $value;
        } else {
            return $this->tab->value_label??'';
        }
    }

    //desc_label
    public function getDescLabelAttr($value, $data)
    {
        if ($value) {
            return $value;
        } else {
            return $this->tab->desc_label??'';
        }
    }


    //image_label
    public function getImageLabelAttr($value, $data)
    {
        if ($value) {
            return $value;
        } else {
            return $this->tab->image_label??'';
        }
    }


    //color_label
    public function getColorLabelAttr($value, $data)
    {
        if ($value) {
            return $value;
        } else {
            return $this->tab->color_label??'';
        }
    }

    //value_open
    public function getValueOpenAttr($value, $data)
    {
        if ($value == 0) {
            return false;//
        } else {
            return $this->tab->value_open??0;
        }
    }

    //desc_open
    public function getDescOpenAttr($value, $data)
    {
        if ($value == 0) {
            return false;//
        } else {
            return $this->tab->desc_open??0;
        }
    }

    //image_open
    public function getImageOpenAttr($value, $data)
    {
        if ($value == 1) {
            return true;
        } else {
            return $this->tab->image_open??0;
        }
    }

    //color_open
    public function getColorOpenAttr($value, $data)
    {
        if ($value == 1) {
            return true;
        } else {
            return $this->tab->color_open??0;
        }
    }

    //pid_open
    public function getPidOpenAttr($value, $data)
    {
        if ($value == 1) {
            return true;
        } else {
            return $this->tab->pid_open??0;
        }
    }

    //如果remark是json格式，用此方法获取对象
    public function getJsonConfig()
    {
        $config = json_decode($this->remark, true);
        return is_array($config) ? $config : [];
    }

    //关联标签
    public function tab()
    {
        return $this->belongsTo('ConfigTab', 'tab_id');
    }

    //关联子配置
    public function configOptions()
    {
        return $this->hasMany('ConfigOption', 'pid');
    }
}
