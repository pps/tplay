<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/11/28
 * Time: 9:18
 */

namespace app\common\model;


use app\common\service\Constant;
use think\Db;
use think\Model;

/**
 * 提供树形数据结构的方法
 * Class Tree
 * @package app\common\model
 */
class Tree extends Model
{
    //定义Tree属性名称
    public $fieldId = 'id';
    public $fieldPid = 'pid';
    public $fieldStatus = 'status';
    public $fieldTreePath = 'tree_path';
    public $fieldSort = 'sort';


    /**
     * 递归方式,获得树形数据结构
     * 节点多的时候，效率比较慢
     * @param $nodelist [节点集合]
     * @param int $pid [起始id,顶级id]
     * @param int $level 节点层级
     * @return array
     */
    public function treelist($nodelist, $pid = 0, $level = 0)
    {
        static $cates = array();
        foreach ($nodelist as $item) {
            if ($item[$this->fieldPid] == $pid) {
                $item['level'] = $level + 1;
                $item['str'] = $level == 0 ? "" : str_repeat('&emsp;&nbsp;', $level) . '└ ';
                $cates[] = $item;
                $this->treelist($nodelist, $item[$this->fieldId], $item['level']);
            }
        }
        return $cates;
    }

    /**
     * sql排序方式,获得树形数据结构
     * 通过tree_path排序获得树形结构，速度比递归快，要保证tree_path的正确性
     * @param $nodelist [节点集合,默认所有节点]
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function treelist2($nodelist = null)
    {
        if (!$nodelist) {
            //sql方式没办法用sort字段一起排序
            $nodelist = $this->where($this->fieldStatus, Constant::STATUS_OPEN)->order($this->fieldTreePath)->select();
        }
        /** @var Tree $cate */
        foreach ($nodelist as $i => $cate) {
            $deep = $cate->getDepth();
            $nodelist[$i]['str'] = $deep == 1 ? "" : str_repeat('&emsp;&nbsp;', $deep) . '└ ';
        }
        return $nodelist;
    }

    /**
     * 更新所有节点tree_path属性
     * @param int $topId [起始id,顶级id]
     */
    public function updateTreePathAll($topId = 0)
    {
        $allCates = $this->select();
        $cates = $this->treelist($allCates, $topId);
        //update tree_path
        foreach ($cates as $cate) {
            $pdoc = null;
            //find pdoc
            foreach ($cates as $pcate) {
                if ($pcate->id == $cate[$this->fieldPid]) {
                    $pdoc = $pcate;
                    break;
                }
            }
            //set tree_path
            $ppath = isset($pdoc) ? $pdoc->tree_path . '-' : "";
            $cate->tree_path = $ppath . $cate->id;
            $cate->allowField(true)->save();
        }
    }

    /**
     * 获取当前是第几层节点
     * @return int
     */
    public function getDepth()
    {
        $var = explode('-', $this->tree_path);
        return count($var);
    }

    /**
     * 获取顶层的节点id
     * @return mixed
     */
    public function getTopId()
    {
        $var = explode('-', $this->tree_path);
        return $var[0];
    }

    /**
     * 判断当前节点是否属于隐藏状态(包括上级节点隐藏)
     * @return bool
     */
    public function isHide()
    {
        if ($this->getData($this->fieldStatus) == Constant::STATUS_CLOSE) {
            return true;
        }
        //上级是否隐藏
        $ids = explode('-', $this->tree_path);
        $exits = $this->where($this->fieldId, 'in', $ids)->where($this->fieldStatus, Constant::STATUS_CLOSE)->value($this->fieldId);
        if ($exits) {
            return true;
        }
        return false;
    }

    /**
     * 是否只是父级隐藏
     * @return bool
     */
    public function isParentHide()
    {
        $path = str_replace($this->id, '', $this->tree_path);
        $ids = explode('-', $path);
        $exits = $this->where($this->fieldId, 'in', $ids)->where($this->fieldStatus, Constant::STATUS_CLOSE)->value($this->fieldId);
        if ($exits) {
            return true;
        }
        return false;
    }

    /**
     * 获取所有隐藏的节点id集合(包括上级节点隐藏)
     * @return array
     */
    public function getHideIdsAll()
    {
        $hidePids = $this->where([$this->fieldPid => ['<>', 0], $this->fieldStatus => Constant::STATUS_CLOSE])->column($this->fieldId);
        $hideIds = [];
        foreach ($hidePids as $pid) {
            $hideIds = array_merge($hideIds, $this->getChildsIdByPid($pid));
        }
        return $hideIds;
    }

    /**
     * 获取父节点
     * @return null|static
     */
    public function getDirectParentNode()
    {
        return self::get($this->getData($this->fieldPid));
    }

    /**
     * 获取上级节点id集合
     * @param $contain [是否包含自己]
     * @return array
     */
    public function getParentsIds($contain = true)
    {
        $ids = explode('-', $this->tree_path);
        if ($contain === false) {
            unset($ids[array_search($this->id, $ids)]);
        }
        return $ids;
    }

    /**
     * 获取上级节点名字集合
     * @param $contain [是否包含自己]
     * @param $field string [指定获取字段]
     * @return array
     */
    public function getParentsNames($contain = true, $field = "title")
    {
        $ids = $this->getParentsIds($contain);
        $names = $this->where($this->fieldId, 'in', $ids)->order($this->fieldTreePath)->column($field);
        return $names;
    }

    /**
     * 获取所有子节点
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getDirectChildNodes()
    {
        return self::where($this->fieldPid, $this->id)->select();
    }

    /**
     * 获取一个子节点
     * @return array|false|\PDOStatement|string|Model
     */
    public function getDirectChildNode()
    {
        return self::where($this->fieldPid, $this->id)->find();
    }

    /**
     * 获取所有子节点id
     * @param $id [指定节点id]
     * @return array
     */
    public function getDirectChildIdsByPid($id)
    {
        return $this->where($this->fieldPid, $id)->column($this->fieldId);
    }

    /**
     * 获取所有下级(子孙)节点id
     * @param $id [起始id,顶级id]
     * @param $contain [是否包含指定节点]
     * @param $status [筛选节点状态]
     * @return array
     */
    public function getChildsIdByPid($id, $contain = true, $status = false)
    {
        $tablename = $this->getTable();
        $where_status = $status === false ? '' : "a.status={$status} and";
        $sql = "select a.id from {$tablename} a,(select id,tree_path p from {$tablename} where id=?) b where {$where_status} a.id = b.id or a.tree_path like concat(b.p,'-%')";
        $cates = Db::query($sql, [$id]);
        $ids = [];
        foreach ($cates as $cate) {
            if ($contain === false && $id == $cate[$this->fieldId]) {
                continue;
            }
            $ids[] = $cate[$this->fieldId];
        }
        return $ids;
    }

}