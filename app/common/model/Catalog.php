<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\common\model;


use app\common\service\Constant;
use think\Exception;

class Catalog extends Tree
{
    // 关闭自动写入time字段
    protected $autoWriteTimestamp = false;

    const TYPE_CATALOG = 0;
    const TYPE_ARTICLE_LIST = 1;
    const TYPE_DIRECTORY = 2;

    const TYPES = [
        self::TYPE_DIRECTORY => '目录',
        self::TYPE_CATALOG => '栏目',
        self::TYPE_ARTICLE_LIST => '文章列表',
    ];

    //type_text 栏目类型
    public function getTypeTextAttr($value, $data)
    {
        return self::TYPES[$data['type']]??$data['type'];
    }

    //article_count 文章数量
    public function getArticleCountAttr($value, $data)
    {
        return $this->hasMany('Article')->where('status', Constant::STATUS_OPEN)->count();
    }

    //article_cate_count 分类数量
    public function getArticleCateCountAttr($value, $data)
    {
        return $this->belongsToMany('ArticleCate', 'app\common\model\CateCatalog')->where('status', Constant::STATUS_OPEN)->count();
    }

    /**
     * 新增或编辑节点时 更新tree_path字段
     * @param $pid
     * @return mixed
     * @throws Exception
     */
    public function updateTreePath($pid)
    {
        if ($this->id == $pid) {
            throw new Exception('上级节点不能选自己');
        }
        if ($pid) {
            /** @var Tree $pcate */
            $pcate = (new self())->find($pid);
            if (!$pcate) {
                throw new Exception('该上级节点不存在，请重新选择');
            }
            if (in_array($this->id, $pcate->getParentsIds())) {
                throw new Exception('上级节点不能选自己下级');
            }
            $new_tree_path = $pcate->tree_path . '-' . $this->id;
        } else {
            $new_tree_path = $this->id;
        }

        //新增或有变化时
        if (!isset($this->tree_path) || $new_tree_path != $this->tree_path) {
            if (false == $this->save(['tree_path' => $new_tree_path])) {
                throw new Exception('更新tree_path失败');
            }
        }
    }

    /**
     * 栏目下拉数据
     */
    public function treelistForCatalog()
    {
        $catalogs = $this->where('type', self::TYPE_DIRECTORY)->order([$this->fieldSort, $this->fieldId])->select();
        return $this->treelist($catalogs);
    }

    /**
     * 文章下拉框数据
     */
    public function treelistForArticle()
    {
        $catalogs = $this->where(['type' => ['in', [self::TYPE_ARTICLE_LIST, self::TYPE_DIRECTORY]], 'status' => Constant::STATUS_OPEN])->order("tree_path")->select();

        $whileflag = true;
        while ($whileflag) {
            $count = 0;
            foreach ($catalogs as $key => $catalog) {
                if ($catalog->getData('type') == self::TYPE_DIRECTORY) {
                    $flag = false;
                    foreach ($catalogs as $key2 => $catalog2) {
                        if ($catalog2->pid == $catalog->id) {
                            $flag = true;
                            break;
                        }
                    }
                    if (!$flag) {
                        $count++;
                        unset($catalogs[$key]);
                    }
                } else if ($catalog->articleCateCount) {
                    $count++;
                    unset($catalogs[$key]);
                }
            }
            if ($count == 0) {
                $whileflag = false;
            }
        }

        return $this->treelist2($catalogs);
    }

    /**
     * 栏目HTML生成路径
     * @return string
     */
    public function getRealPath()
    {
        return Templet::getRootDir() . deleteStartDS(replaceDS($this->tpath()));
    }

    /**
     * cms发布路径
     * 如果存在上级目录，则拼接上级目录
     */
    public function tpath()
    {
        $tpath = "";
        if ($this->pid) {
            $pnode = $this->getDirectParentNode();
            if ($pnode && $pnode->tpath()) {
                $tpath = $pnode->tpath();
            }
        }
        $enname = $this->path;
        if ($tpath && $enname) {
            return $tpath . appendStartDS($enname, '/');
        } else {
            return $enname ?: $tpath;
        }
    }

    /**
     * 获取栏目 cms uri
     * @return mixed
     */
    public function getUri()
    {
        return replaceUrlDS($this->getData('path'));
    }


    /**
     * 获取栏目模板
     * @return Templet|null
     */
    public function getCatalogTemplet()
    {
        return (new Templet)->setName($this->catalog_templet);
    }

    /**
     * 获取文章模板
     * @return Templet|null
     */
    public function getArticleTemplet()
    {
        return (new Templet)->setName($this->article_templet);
    }

    //关联分类
    public function articleCates()
    {
        return $this->belongsToMany('ArticleCate', 'app\common\model\CateCatalog')->where('status', Constant::STATUS_OPEN)->order('sort desc');
    }

    //分类个数
    public function articleCateCount()
    {
        return $this->belongsToMany('ArticleCate', 'app\common\model\CateCatalog')->where('status', Constant::STATUS_OPEN)->count();
    }

    /**
     * 获取一个分类
     * @return mixed
     * @throws \Exception
     */
    public function getFirstArticleCate()
    {
        $cates = $this->articleCates;
        if (empty($cates)) {
            throw new \Exception("栏目{$this->id}找不到对应的分类");
        }
        return current($cates);
    }

}