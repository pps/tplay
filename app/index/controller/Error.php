<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/9/20
 * Time: 17:42
 */

namespace app\index\controller;


use app\user\controller\base\Permissions;
use think\Controller;

//空控制器
class Error extends Controller
{
    //空操作
    public function _empty($name)
    {
        if ('login' == $name) {
            $this->redirect(Permissions::$login_url);
        }
        abort(404);
    }
}