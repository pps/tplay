<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\index\controller;

use app\common\service\CmsService;
use app\index\controller\base\Permissions;

class Index extends Permissions
{
    //前置操作
    protected $beforeActionList = [
        'checkLogin' => ['only' => 'home'],//登入检查
    ];

    public function index()
    {
        (new CmsService())->perviewSite();
        if (file_exists(APP_PATH . 'install')) {
            return $this->fetch('default');
        } else {
            return "本页面为 " . PRODUCT_NAME . " 系统默认页。";
        }
    }

    //要登入才能访问home
    public function home()
    {
        return 'home';
    }
}
