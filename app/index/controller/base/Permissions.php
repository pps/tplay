<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\index\controller\base;

use app\common\service\WebService;

class Permissions extends \app\user\controller\base\Permissions
{
    protected function _initialize()
    {
        (new WebService())->checkInstalled();
        //登入验证
        if ($this->isLogin()) {
            $this->checkLoginExprid();
            $this->assign('user', $this->getUser());
        }
    }
}
