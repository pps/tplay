<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

// +----------------------------------------------------------------------
// curl方法
// +----------------------------------------------------------------------

/**
 * get 请求
 * @param $url
 * @param array $params
 * @param array $header
 * @param string $cookie
 * @return array
 */
function getUrl($url, $params = [], $header = [], $cookie = "")
{
    if ($params) {
        $url .= '?' . (is_array($params) ? http_build_query($params) : $params);
    }
    $curl = new \network\CurlHelper($url);
    $curl->setHeader($header);
    $curl->setCookie($cookie);
    $curl->exec();
    return [$curl->getResponse(), $curl->getHttpCode(), $curl->getError()];
}

/**
 * post 请求
 * @param $url
 * @param array|string $params
 * @param array $header
 * @param string $cookie
 * @return array
 */
function postUrl($url, $params = [], $header = [], $cookie = "")
{
    $curl = new \network\CurlHelper($url, $params);
    $curl->setHeader($header);
    $curl->setCookie($cookie);
    $curl->exec();
    return [$curl->getResponse(), $curl->getHttpCode(), $curl->getError()];
}

/**
 * 提交json格式的请求
 * @param $url
 * @param array $params
 * @return array
 */
function postJson($url, $params = [])
{
    $header = ['Content-Type: application/json; charset=utf-8', 'Content-Length:' . strlen(json_encode($params))];
    $curl = new \network\CurlHelper($url, json_encode($params));
    $curl->setHeader($header);
    $curl->exec();
    return [$curl->getResponse(), $curl->getHttpCode(), $curl->getError()];
}


/**
 * 有的接口会通过cookie防采集
 * @param $url
 * @param $expire int 默认缓存一小时
 * @return mixed
 * @throws Exception
 */
function getBySetcookie($url, $expire = 3600)
{
    $cookieName = "cache-setcookie-key";
    $cookieVal = isset($_COOKIE[$cookieName]) ? $_COOKIE[$cookieName] : "";
    list($response, $httpCode) = getUrl($url, null, null, $cookieVal);
    if ($httpCode == 302) {
        $curl = new \network\CurlHelper($url);
        $curl->showResponseHeader();//输出响应头
        $curl->exec();
        $response = $curl->getResponse();
        // 正则获取set-cookie
        if (preg_match_all('/Set-Cookie: (.*?);/m', $response, $cookie)) {
            $realCookie = implode(';', $cookie['1']);
            setcookie($cookieName, $realCookie, time() + $expire);//缓存
            list($response) = getUrl($url, null, null, $cookieVal);
        } else {
            throw new Exception("获取Set-Cookie失败");
        }
    } else if ($httpCode == 301) {
        throw new Exception("获取失败，返回301跳转");
    } else if ($httpCode != 200) {
        throw new Exception("获取失败，httpCode = $httpCode");
    }
    return $response;
}

/**
 * curl获取sse数据
 * 注意：不能在流式输出过程中执行header()函数
 * @param $url
 * @param array $content
 * @param array $header
 * @param null $callback
 */
function tocurlSse($url, $content = [], $header = [], $callback = null)
{
    header('Content-Type: text/event-stream');
    header('X-Accel-Buffering: no');

    /**
     * https://blog.csdn.net/echojson/article/details/106403558
     * php.ini默认设置了output_buffering = 4096，php已经设置了一个缓冲区。则 ob_get_level() 得到的值会是1
     * 输出缓冲区有点类似堆栈，临时放入一个存储空间中。类似队列一样，但是不是先进先出，而是先开后关
     */
    if (ob_get_level() == 0) {//缓存嵌套级别
        ob_start();//每执行一次，就会开启一个新的缓存区
    }

    if (!isset($header['Content-Type'])) {
        $header['Content-Type'] = 'text/event-stream';
    }

    $curl = new \network\CurlHelper($url, $content);
    $curl->setHeader($header);
    $curl->setBufferSize(1024);
    $curl->setTimeout(10, 0);
    $curl->setSseCallback($callback);
    $curl->exec();
}

/**
 * 简单curl请求方法
 * @param String $url 请求的地址
 * @param array|string $content POST数据
 * @param array $header header数据，支持数组['x:y','x2:zh',...]和字典类型['x'=>'y','x2'=>'zh',...]
 * @return String
 */
function tocurl($url, $content = [], $header = ['User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64)'])
{
    $ch = curl_init();
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
    }
    if (!isset ($header [0])) { // 将键值数组{key:val}转为索引数组['key:val']
        foreach ($header as $hk => $hv) {
            unset ($header [$hk]);
            $header [] = $hk . ':' . $hv;
        }
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);//timeout
    if ($content) {
        //post请求
        curl_setopt($ch, CURLOPT_POST, true);
        // $post_data为array类型时multipart/form­data请求，string类型时application/x-www-form-urlencoded
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    }

    $response = curl_exec($ch);
    if ($error = curl_error($ch)) {
        curl_close($ch);
        die ($error);
    }
    curl_close($ch);
    return $response;
}