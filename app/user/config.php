<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

//配置文件
return [
    //全局替换
    'view_replace_str' => [
        '__PUBLIC__' => '/static/public',
        '__CSS__' => '/static/user/css',
        '__JS__' => '/static/user/js',
        '__IMG__' => '/static/user/images',
        '__LAYUI__' => '/static/public/layui-v2.7.6/layui'
    ],
    // 默认的空控制器名
    'empty_controller' => 'Index',
];