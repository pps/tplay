<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\user\controller;

use app\common\model\Messages;
use app\common\model\User;
use app\common\service\Constant;
use app\user\controller\base\Permissions;
use string\Filter;
use think\Log;

class Message extends Permissions
{
    //前置操作
    protected $beforeActionList = [
        'checkLogin' => ['except' => 'index'],//不登入检查
        'checkActivate' => ['except' => 'index'],//不检查是否激活账号
    ];

    //检查是否激活账号
    protected function checkActivate()
    {
        if (Constant::STATUS_PASS != $this->getUser()['status']) {
            $this->error('账号' . $this->getUser()->status_text);
        }
    }

    /**
     * 留言墙
     * 评论区、留言区 大致都是相同的，是否能做成各种页面可以通用的接口？
     * 考虑到数据量问题，数据隔离问题，还是不搞太复杂，还是分别建表和接口比较好维护
     * @return mixed
     */
    public function index()
    {
        $msgs = (new Messages())
            ->where(['type' => Messages::TYPE_MESSAGE])
            ->where(['status' => Constant::STATUS_PASS])
            ->order('id desc')->paginate(20);

        //获取父级的留言
        /** @var Messages $msg */
        foreach ($msgs as $k => $msg) {
            $msgs[$k]['pmsgs'] = $msg->getPmsgs();
        }

        $this->assign('msgs', $msgs);
        return $this->fetch();
    }

    //提交留言
    public function saveMsg()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $validate = new \think\Validate([
                ['message|内容', 'require|max:500|sqlInj'],
                ['captcha', 'require|captcha', '验证码不能为空|验证码不正确'],
            ]);
            $validate->extend('sqlInj', function ($value) use ($post) {
                try {
                    Filter::checkSqlInj($value);
                } catch (\Exception $e) {
                    Log::record($post['message'], 'notice');
                    Log::record($e->getMessage(), 'notice');
//                    return '含有敏感字符';
                }
                return true;
            });
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }

            stopCC(function () {
                $this->error("您的操作频繁，请休息一会儿");
            });

            $to_msg_id = 0;
            $to_user_id = 0;
            if (preg_match('/\((\d*)楼\)/', $post['message'], $matches)) {

                $to_msg_id = $matches[1];
                $msg = (new Messages())
                    ->where(['type' => Messages::TYPE_MESSAGE])
                    ->where(['id' => $to_msg_id])
                    ->find();

                if (empty($msg)) {
                    $to_msg_id = 0;
                } else {
                    $to_user_id = $msg->from_user_id;
                    $post['message'] = explode($matches[0]??')', $post['message'], 2)[1];
                }
            }

            $model = new Messages();
            $data = [
                'from_user_id' => $this->getUserId(),
                'to_user_id' => $to_user_id,
                'to_msg_id' => $to_msg_id,
                'ip' => $this->request->ip(),
                'message' => $post['message'],
                'type' => Messages::TYPE_MESSAGE,
                'status' => Constant::STATUS_PASS
            ];
            if (false == $model->allowField(true)->save($data)) {
                $this->error('提交失败');
            } else {
                if ($to_msg_id) {
                    $node = $model;
                    $pnode = (new Messages())->find($to_msg_id);
                    if ($pnode) {
                        if (empty($pnode->tree_path)) {
                            $pnode->tree_path = $pnode->id;
                            $pnode->save();
                        }
                        $tree_path = $pnode->tree_path . '-' . $node->id;
                        $node->save(['tree_path' => $tree_path]);
                    }
                }
                $this->success('提交成功');
            }
        }
    }
}
