<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\user\controller\base;

use app\common\model\User;
use app\common\service\LoginService;
use app\common\service\WebService;
use think\Controller;

class Permissions extends Controller
{
    private $user;

    /**
     * 登入地址
     * @var string
     */
    public static $login_url = 'user/common/login';


    protected function _initialize()
    {
        $service = new WebService();
        $service->checkInstalled();
        $service->checkOpenUserModule();
        stopCC();
        //登入验证
        if ($this->isLogin()) {
            $this->checkLoginExprid();
            $this->assign('user', $this->getUser());
        }
    }

    /**
     * 判断用户是否登入
     * @return mixed
     */
    protected function isLogin()
    {
        return session('?' . LoginService::LOGIN_INFO . '.userId');
    }

    /**
     * 登入鉴权
     */
    protected function checkLogin()
    {
        if (!$this->isLogin()) {
            $this->error("请先登入", self::$login_url, '', 0);
        }
    }

    /**
     * 判断登入过期，并退出登入
     */
    protected function checkLoginExprid()
    {
        $data = session(LoginService::LOGIN_INFO);
        if (!isset($data['exptime']) || $data['exptime'] < time()) {
            LoginService::clear_session();
            $this->error('登入超时,请重新登入', self::$login_url, '');
        }
    }

    /**
     * 获取user_id
     * @return mixed
     */
    protected function getUserId()
    {
        $data = session(LoginService::LOGIN_INFO);
        if (!$data['userId']) {
            $this->error('请先登入', self::$login_url, '');
        }
        return $data['userId'];
    }

    /**
     * 获取user对象
     * @return null|User
     */
    protected function getUser()
    {
        if (!$this->user) {
            $user = User::get($this->getUserId());
            if (!$user) {
                LoginService::clear_session();
                $this->error('找不到用户信息,请重新登入', self::$login_url, '');
            }
            $this->user = $user;
        }
        return $this->user;
    }
}
