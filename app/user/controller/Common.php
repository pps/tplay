<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\user\controller;

use app\common\model\Emailconfig;
use app\common\model\User;
use app\common\service\Constant;
use app\common\service\LoginService;
use app\user\controller\base\Permissions;
use string\Encryption;
use string\Format;
use think\Cache;
use think\captcha\Captcha;
use think\Cookie;
use think\Db;
use think\Env;
use think\Session;

class Common extends Permissions
{

    /**
     * 登录
     */
    public function login()
    {
        if (Session::has(LoginService::LOGIN_INFO) == false) {
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['name|账号', 'require|max:50'],
                    ['password|密码', 'require|length:6,16'],
                    ['captcha', 'require|captcha', '验证码不能为空|验证码不正确'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }

                //连续错误5次账号暂停
                $error_count = Cache::get('error_count' . $post['name']);
                if ($error_count >= 5) {
                    $this->error('登入频繁,请休息10分钟');
                }

                $user = Db::name('user')->where('passport', $post['name'])->find();
                if (empty($user)) {
                    $this->error('账号不存在,请先注册');
                } else {
                    $post['password'] = password($post['password']);
                    if ($user['password'] != $post['password']) {
                        //记录次数
                        if (empty($error_count)) {
                            Cache::set('error_count' . $post['name'], 1, 600);
                        } else {
                            Cache::set('error_count' . $post['name'], ++$error_count, 600);
                        }
                        $this->error('密码错误');
                    } else {
//                        if (User::STATUS_PASS != $user['status']) {
//                            $this->error('账号未激活');
//                        }
                        //是否记住账号
                        if (!empty($post['remember']) and $post['remember'] == 1) {
                            if (Cookie::has('usermember')) {
                                Cookie::delete('usermember');
                            }
                            Cookie::forever('usermember', $post['name']);
                        } else {
                            if (Cookie::has('usermember')) {
                                Cookie::delete('usermember');
                            }
                        }
                        //获取登入令牌
                        $token_data = LoginService::getAccessToken($user['id'], time(), 3600 * 24);
                        //更新登录时间
                        Db::name('user')->where('id', $user['id'])->update(['login_time' => time()]);
                        //清空次数
                        Cache::rm('error_count' . $post['name']);
                        $this->success('登录成功,正在跳转...', 'user/index/index', $token_data);
                    }
                }
            } else {
                if (Cookie::has('usermember')) {
                    $this->assign('usermember', Cookie::get('usermember'));
                }
                return $this->fetch();
            }
        } else {
            $this->redirect('user/index/index');
        }
    }

    /**
     * 注册
     */
    public function register()
    {
        if (Session::has(LoginService::LOGIN_INFO) == false) {
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['name', 'require|max:50|email', '账号不能为空|邮箱格式不正确'],
                    ['nickname|昵称', 'max:20|chsDash'],
                    ['password', 'require|confirm|length:6,16', '密码不能为空|两次密码不一致|密码长度须6到16个字符'],
                    ['password_confirm', 'require', '重复密码不能为空'],
                    ['captcha', 'require|captcha', '验证码不能为空|验证码不正确'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }

                //同一ip限制一天20次
                $ip = $this->request->ip();
                $reg_count = Cache::get('reg_count' . $ip);
                if ($reg_count >= 20) {
                    $this->error('注册频繁,请休息一下');
                }

                $user = (new User())->where('passport', $post['name'])->find();
                if (!empty($user)) {
                    if ($user->status == Constant::STATUS_UNPASS) {
                        $this->error('该账号已禁用');
                    } else {
                        $this->error('该账号已存在，请不要重复注册');
                    }
                } else {

                    $pid = 0;
                    if (Cookie::get('invite_code')) {
                        $id = Encryption::inviteDecode(Cookie::get('invite_code'));
                        if ($id && (new User())->where('id', $id)->count()) {
                            $pid = $id;
                        }
                    }
                    $code = Encryption::randomStr(6, '0123456789');//激活码
                    $userModel = new User();
                    $data = [
                        'pid' => $pid,
                        'passport' => $post['name'],
                        'nickname' => empty($post['nickname']) ? Format::hideStr($post['name']) : $post['nickname'],
                        'password' => password($post['password']),
                        'user_type' => User::TYPE_EMAIL,
                        'user_cate' => User::CATE_USER,
                        'status' => Constant::STATUS_WAIT,
                        'ip' => $this->request->ip(),
                        'create_time' => time(),
                        'remark' => $code
                    ];
                    if (false === $userModel->save($data)) {
                        //记录次数
                        if (empty($reg_count)) {
                            Cache::set('reg_count' . $ip, 1, 600);
                        } else {
                            Cache::set('reg_count' . $ip, ++$reg_count, 600);
                        }
                        $this->error('注册失败，请联系管理员处理');
                    } else {
                        //记录次数
                        if (empty($reg_count)) {
                            Cache::set('reg_count' . $ip, 1, 3600 * 12);
                        } else {
                            Cache::set('reg_count' . $ip, $reg_count + 2, 3600 * 12);
                        }
                        //测试注册
                        if (\think\App::$debug) {
//                            if (password($post['password']) == password(Env::get('db_password', ''))) {
//                                $activation_url = url('user/common/activation', '', false, true) . '?code=' . $code . '&name=' . $post['name'] . '&__token__=' . $this->request->token();
//                                $this->success('注册成功,请前往邮箱激活后登入...', self::$login_url, ['activation_url' => $activation_url]);
//                            }
                        }
                        if (false == Emailconfig::sendemail($post['name'], $code, $this->request->token())) {
                            $this->error('邮箱验证发送失败，请联系管理员处理');//发送失败请去后台配置邮箱并测试
                        } else {
                            $this->success('注册成功,请前往邮箱激活后登入...', self::$login_url);
                        }
                    }
                }
            } else {
                if ($this->request->has('invite')) {
                    Cookie::set('invite_code', $this->request->param('invite'), 24 * 3600);
                }
                return $this->fetch();
            }
        } else {
            $this->redirect('user/index/index');
        }
    }


    /**
     * 激活邮箱
     */
    public function activation()
    {
        $post = $this->request->get();
        $validate = new \think\Validate([
            ['name', 'require|max:50|email|token', '账号不能为空|邮箱格式不正确'],
            ['code', 'require', '激活码不能为空']
        ]);
        if (!$validate->check($post)) {
            $this->error('提交失败：' . $validate->getError());
        }

        $code = $this->request->param('code');
        $passport = $this->request->param('name');

        $user = Db::name('user')->where('passport', $passport)->find();
        if (empty($user)) {
            $this->error('账号不存在');
        } else {
            if (intval($code) == $user['remark']) {
                Db::name('user')->where('id', $user['id'])->update(['status' => Constant::STATUS_PASS]);
                $this->success('账号激活成功', self::$login_url);
            }
            $this->error('账号激活失败,请联系管理员');
        }
    }

    //使用中文验证码
    public function captcha()
    {
        $id = $this->request->param("t", "");//验证码标识
        $captcha = new Captcha();
        $captcha->useZh = true;//使用中文验证码
        $captcha->zhSet = '们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这如果';
        $captcha->length = 4;//验证码位数
        return $captcha->entry($id);//如果你需要在一个页面中生成多个验证码的话，entry方法需要传入可标识的信息
    }

    //空操作
    public function _empty()
    {
        $this->redirect('user/index/index');
    }
}
