<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\user\controller;

use app\common\model\Emailconfig;
use app\common\model\Messages;
use app\common\model\User;
use app\common\service\Constant;
use app\common\service\LoginService;
use app\user\controller\base\Permissions;
use string\Encryption;
use think\Db;
use think\Session;

class Index extends Permissions
{
    //前置操作
    protected $beforeActionList = [
        'checkLogin' => '',//登入检查
        'checkActivate' => ['except' => 'logout,home,activate'],//不检查是否激活账号
    ];

    //检查是否激活账号
    protected function checkActivate()
    {
        if (Constant::STATUS_PASS != $this->getUser()['status']) {
            exit($this->fetch('activate_email'));
        }
    }

    //系统公告
    public function index()
    {
        $model = new Messages();
        $msgs = $model->where(['status' => Constant::STATUS_PASS, 'type' => Messages::TYPE_SYSTEM])->order('update_time desc')->limit(10)->select();
        $this->assign('msgs', $msgs);
        return $this->fetch();
    }

    //我的主页
    public function home()
    {
        return $this->fetch();
    }

    //我的留言
    public function message()
    {
        $model = new Messages();
        $msgs = $model->where(['from_user_id' => $this->getUserId()])->order('create_time desc')->paginate(10);
        $this->assign('msgs', $msgs);
        $replys_count = $model->where(['status' => Constant::STATUS_PASS, 'to_user_id' => $this->getUserId()])->count();
        $this->assign('replys_count', $replys_count);
        return $this->fetch();
    }

    //收到的回复
    public function reply()
    {
        $model = new Messages();
        $msgs_count = $model->where(['from_user_id' => $this->getUserId()])->count();
        $this->assign('msgs_count', $msgs_count);
        $replys = $model->where(['status' => Constant::STATUS_PASS, 'to_user_id' => $this->getUserId()])->order('create_time desc')->paginate(10);
        $this->assign('replys', $replys);
        return $this->fetch();
    }


    //查看我的留言
    public function showMsg()
    {
        $id = $this->request->param('id');
        if (!$id) {
            $this->error('404');
        }

        $msg = (new Messages())->where(['id' => $id, 'from_user_id' => $this->getUserId()])->find();
        if (!$msg) {
            $this->error('404');
        }

        if ($msg->status != Constant::STATUS_PASS) {
            $this->error("审核未通过");
        }
        $msg->save(['read' => 1]);
        $msg->pmsgs = $msg->getPmsgs();
        $this->assign('msg', $msg);
        return $this->fetch();
    }

    //查看收到的回复
    public function showReply()
    {
        $id = $this->request->param('id');
        if (!$id) {
            $this->error('404');
        }

        $msg = (new Messages())->where(['id' => $id, 'to_user_id' => $this->getUserId()])->find();
        if (!$msg) {
            $this->error('404');
        }
        $msg->save(['read' => 1]);

        if ($msg->type == Messages::TYPE_ADMIN) { //处理管理员消息
            $msg = (new Messages())->where(['id' => $msg->to_msg_id, 'from_user_id' => $this->getUserId()])->find();
        }

        $msg->pmsgs = $msg->getPmsgs();
        $this->assign('msg', $msg);
        return $this->fetch('show_msg');
    }

    //基本设置
    public function setting()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $validate = new \think\Validate([
                ['head_pic|头像', 'max:255'],
                ['nickname|昵称', 'max:30'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
            $data = [];
            if (isset($post['head_pic'])) {
                $data['head_pic'] = htmlspecialchars($post['head_pic']);
            }
            if (isset($post['nickname'])) {
                $data['nickname'] = htmlspecialchars($post['nickname']);
            }
            if (!empty($data)) {
                if (false == $this->getUser()->save($data)) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功');
                }
            }
        }
        return $this->fetch();
    }


    //修改密码
    public function editPassword()
    {
        if ($this->request->isPost()) {
            $id = Session::get(LoginService::LOGIN_INFO . '.user_id');
            $post = $this->request->post();
            $validate = new \think\Validate([
                ['password', 'require', '原密码不能为空'],
                ['password', 'require|confirm|length:6,16', '新密码不能为空|确认密码不一致|密码长度须6到16个字符'],
                ['password_confirm', 'require', '确认密码不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
            $user = Db::name('user')->where('id', $id)->find();
            if (password($post['password_old']) == $user['password']) {
                if (false == Db::name('user')->where('id', $id)->update(['password' => password($post['password'])])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功');
                }
            } else {
                $this->error('原密码错误');
            }
        }
    }

    //重新发送邮件
    public function activate()
    {
        $cachekey = 'cache_activate_' . $this->getUserId();
        if (cache($cachekey)) {
            $this->error('邮件已发送，若没收到请稍后再重试');
        }
        $code = $this->getUser()->getData('login_time');
        if (!$code) {
            $code = Encryption::randomStr(6, '0123456789');
            $this->getUser()->save(['login_time' => $code]);
        }
        $to_email = $this->getUser()->passport;
        if (false == Emailconfig::sendemail($to_email, $code, $this->request->token())) {
            $this->error('邮箱验证发送失败，请联系管理员处理');//发送失败请去后台配置邮箱并测试
        } else {
            cache($cachekey, time(), 5 * 60);
            $this->success('发送成功,请前往邮箱激活...');
        }
    }

    //退出登入
    public function logout()
    {
        LoginService::clear_session();
        if (Session::has(LoginService::LOGIN_INFO)) {
            $this->error('退出失败');
        } else {
            $this->success('正在退出...', self::$login_url);
        }
    }
}
