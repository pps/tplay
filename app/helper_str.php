<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

// +----------------------------------------------------------------------
// 判断字符串匹配
// +----------------------------------------------------------------------

/**
 * 判断字符串是否以指定字符(串)开头
 * @param $str
 * @param $pattern
 * @return bool
 */
function ifStartWith($str, $pattern)
{
    return (strpos($str, $pattern) === 0) ? true : false;
}

//判断字符串是否以指定字符(串)结束
function ifEndWith($str, $pattern)
{
    $length = strlen($pattern);//这里只支持英文
    if ($length == 0) {
        return true;
    }
    return substr($str, -$length) === $pattern;
}

/**
 * 判断字符串$str是否包含$pattern
 * @param $str
 * @param $pattern
 * @return bool
 */
function ifContain($str, $pattern)
{
    return strpos($str, $pattern) !== false;
}

/**
 * 判断字符串是url格式
 * @param $str
 * @return bool
 */
function isUrl($str)
{
    $preg = "/http[s]?:\/\/[\w.]+[\w\/]*[\w.]*\??[\w=&\+\%]*/is";
    if (preg_match($preg, $str)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 判断蜘蛛访问
 * @param $str $_SERVER['HTTP_USER_AGENT']
 * @return bool
 */
function isSpiderRobot($str)
{
    $ua = strtolower($str);
    $botchar = "/(baidu|google|spider|soso|yahoo|sohu|yodao|robozilla|bing|sogou|twiceler|msn|haosou|360|ia_archiver|iaarchiver|slurp|bot)/i";
    return preg_match($botchar, $ua) ? true : false;
}

/**
 * 判断是否微信中打开
 * @param $str $_SERVER['HTTP_USER_AGENT']
 * @return bool
 */
function isWeixin($str)
{
    if (strpos($str, 'MicroMessenger') !== false) {
        return true;
    } else {
        return false;
    }
}

// +----------------------------------------------------------------------
// 匹配提取
// +----------------------------------------------------------------------

/**
 * 正则匹配出html中所有图片地址
 * @param $content string html
 * @return mixed
 */
function matchImgs($content)
{
    $pattern_src = '/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/';
    $num = preg_match_all($pattern_src, $content, $match_src);
    $pic_arr = $match_src[1]; //获得图片数组
    return $pic_arr;
}


// +----------------------------------------------------------------------
// 字符串截取
// +----------------------------------------------------------------------

/**
 * 截取中文字符串
 * @param $str
 * @param $len [保留長度]
 * @return bool|string
 */
function subStrCN($str, $len)
{
    if (mb_strlen($str, "utf-8") > $len) {
        return $str = mb_substr($str, 0, $len, 'utf-8');
    }
    return $str;
}

/**
 * 截取字符串
 * @param $str
 * @param $len
 * @return bool|string
 */
function subStrEN($str, $len)
{
    if (strlen($str) > $len) {
        return $str = substr($str, 0, $len);
    }
    return $str;
}


/**
 * 按指定长度，拆分中文字符串
 * @param $string
 * @param int $len
 * @return array
 * @throws Exception
 */
function mbStrSplit($string, $len = 1)
{
    if (0 >= $len) {
        throw new Exception('too short');
    }
    $start = 0;
    $array = [];
    $strlen = mb_strlen($string);
    while ($strlen) {
        $array[] = mb_substr($string, $start, $len, "utf8");
        $string = mb_substr($string, $len, $strlen, "utf8");
        $strlen = mb_strlen($string);
    }
    return $array;
}


// +----------------------------------------------------------------------
// 替换
// +----------------------------------------------------------------------


/**
 * 类似str_replace,但只替换一个
 * 思路首先是找到待替换的关键词的位置，然后利用substr_replace函数直接替换之。
 * @param $needle
 * @param $replace
 * @param $subject
 * @return mixed
 */
function replaceOnce($needle, $replace, $subject)
{
    // Looks for the first occurence of $needle in $haystack
    // and replaces it with $replace.
    $pos = strpos($subject, $needle);
    if ($pos === false) {
        return $subject;
    }
    return substr_replace($subject, $replace, $pos, strlen($needle));
}

/**
 * 类似str_replace,但可指定替换次数
 * 殊字符做了转义处理
 * @param $search
 * @param $replace
 * @param $subject
 * @param int $limit 指定匹配个数
 * @return mixed
 */
function replaceByLimit($search, $replace, $subject, $limit = 1)
{
    // constructing mask(s)...
    if (is_array($search)) {
        foreach ($search as $k => $v) {
            $search[$k] = '`' . preg_quote($search[$k], '`') . '`';
        }
    } else {
        $search = '`' . preg_quote($search, '`') . '`';
    }
    // replacement
    return preg_replace($search, $replace, $subject, $limit);
}


// +----------------------------------------------------------------------
// 分隔符
// +----------------------------------------------------------------------


//路径前追加文件分隔符
function appendStartDS($path, $separator = DS)
{
    return ifStartWith($path, $separator) ? $path : $separator . $path;
}

//删除路径前的文件分隔符
function deleteStartDS($path, $separator = DS)
{
    //echo ltrim("a_ai123","a_"); 等于  i123 ? 他是一个一个字符判断是否在a和_字符中，存在就删了
    return ifStartWith($path, $separator) ? substr($path, strlen($separator)) : $path;
}

//路径后追加文件分隔符
function appendEndDS($path, $separator = DS)
{
    return ifEndWith($path, $separator) ? $path : $path . $separator;
}

//路径后删除文件分隔符
function deleteEndDS($path, $separator = DS)
{
    $pos = strrpos($path, $separator);
    return ifEndWith($path, $separator) ? substr($path, 0, $pos) : $path;
}

//替换为当前系统文件分隔符
function replaceDS($path)
{
    $WIN_DIRECTORY_SEPARATOR = "\\";
    $LINUX_DIRECTORY_SEPARATOR = "/";
    if (IS_WIN) {
        return str_replace($LINUX_DIRECTORY_SEPARATOR, $WIN_DIRECTORY_SEPARATOR, $path);
    } else {
        return str_replace($WIN_DIRECTORY_SEPARATOR, $LINUX_DIRECTORY_SEPARATOR, $path);
    }
}

//替换为url分隔符
function replaceUrlDS($uri)
{
    return str_replace("\\", "/", $uri);
}
