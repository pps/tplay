<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */


namespace app\admin\model;

use think\Model;

class AdminCate extends Model
{

    public static function init_superadmin()
    {
        $cate = AdminCate::get(1);
        if (!$cate) {
            $cate = new AdminCate();
        }
        $all_menu_id = implode(',', (new AdminMenu())->column('id'));
        if ($all_menu_id == $cate->permissions) {
            return true;
        }
        return $cate->save(['name' => '超级管理员', 'permissions' => $all_menu_id]);
    }

    public function admin()
    {
        //关联管理员表
        return $this->hasOne('Admin');
    }
}
