<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */


namespace app\admin\model;

use think\Model;

class AdminLog extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;

	public function admin()
    {
        return $this->belongsTo('Admin');
    }

    public function menu()
    {
        return $this->belongsTo('AdminMenu');
    }
}
