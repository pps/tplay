<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */


namespace app\admin\model;

use think\Model;

class AdminMenu extends Model
{
    public function treelist($nodelist, $id = 0, $level = 0)
    {
        static $cates = array();
        foreach ($nodelist as $value) {
            if ($value['pid'] == $id) {
                $value['level'] = $level + 1;
                $value['str'] = $level == 0 ? "" : str_repeat('&emsp;&nbsp;', $level) . '└ ';
                $cates[] = $value;
                $this->treelist($nodelist, $value['id'], $value['level']);
            }
        }
        return $cates;
    }

    public function log()
    {
        return $this->hasOne('AdminLog');
    }
}
