<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\model;

use \think\Model;

class Urlconfig extends Model
{
    /**
     * 获取登录地址(美化后的)
     * @return string
     */
    public function getLoginUrl()
    {
        return url('admin/common/login', '', true, true);
    }

    /**
     * 是否了开启安全入口
     */
    public function isWeekBackend()
    {
        $has = $this->where('url', 'admin/common/login')->where('aliases', 'admin_login')->count();
        return $has > 0;
    }

    /**
     * 获取安全入口key
     * @return mixed|string
     */
    public function getBackendPass()
    {
        $backendurl = url('admin/common/login', '', false);//有可能被缓存影响
        if ($backendurl == "/admin_login" || $backendurl == "/admin/common/login") {
            return "";
        } else {
            return str_replace('/admin_login/', '', $backendurl);
        }
    }

    /**
     * 获取网站维护的key
     * @return mixed|string
     */
    public function getCloseSiteKey()
    {
        $pass = $this->getBackendPass();
        return empty($pass) ? "1464674022" : $pass;
    }
}
