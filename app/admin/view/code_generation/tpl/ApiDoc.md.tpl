# {$menuName}

{php}echo "\n\n";{/php}


{if condition="$crud['select']"}
## 查询一个{$menuName} {php}echo PHP_EOL;{/php}

接口地址：/api/{$tableUnderLineName}/getOne

请求方式：get / post

请求数据：

| 参数名 | 数据类型  | 说明  |
| ---   | ---   | ---  |
|  id  |  int,require | 指定ID |


响应数据：(json格式)
```
{
    "code": 1, // 返回状态，1代表成功，0代表失败
    "msg": "success", // 返回消息
    "time": "1586490789", // 响应时间戳
    "data": null // 结果数据,json格式,没数据时为 null
}
```

## 查询所有{$menuName} {php}echo PHP_EOL;{/php}

接口地址：/api/{$tableUnderLineName}/getList

请求方式：get / post

请求数据：

| 参数名 | 数据类型  | 说明  |
| ---   | ---   | ---  |
|  page    |  int   | 第几页,默认1           |
|  per_page |  int,<=:1000 | 每页几条,默认20        |


响应数据：(json格式)
```
{
    "code": 1, // 返回状态，1代表成功，0代表失败
    "msg": "success", // 返回消息
    "time": "1586490789", // 响应时间戳
    "data": null // 结果数据,json格式,没数据时为 null
}
```
{/if}


{if condition="$crud['create']"}

## 新增{$menuName} {php}echo PHP_EOL;{/php}

接口地址：/api/{$tableUnderLineName}/create

请求方式：get / post

请求数据：

| 参数名 | 数据类型  | 说明  |
| ---   | ---   | ---  |
{volist name="$fieldsInfo" id="vo"}
{php}
       $field_key = $vo['Field'];
       if(in_array($field_key,['id','create_time','update_time'])){
           continue;
       }
       if(!empty($vo['Comment'])){
           $field_comment = $vo['Comment'] . " | ";
       }else{
           $field_comment = " | ";
       }
       $field_val = "";
       if($vo['Default'] === null){
           $field_val.= "require";
       }
       if(ifContain($vo["Type"],'int')){
           $field_val .= empty($field_val) ? "int" : ",int";
       }
       if(ifStartWith($vo["Type"],'varchar')){
           $maxLen = str_replace(['varchar(',')'],['',''],$vo["Type"]);
           $field_val .= empty($field_val) ? "max:".$maxLen : ",max:".$maxLen;
       }
{/php}

{notempty name="$field_val"}
|  {$field_key} |  {$field_val} | {$field_comment} {php}echo PHP_EOL;{/php}
{/notempty}

{/volist}


响应数据：(json格式)
```
{
    "code": 1, // 返回状态，1代表成功，0代表失败
    "msg": "success", // 返回消息
    "time": "1586490789", // 响应时间戳
    "data": null // 结果数据,json格式,没数据时为 null
}
```
{/if}


{if condition="$crud['delete']"}

## 删除{$menuName} {php}echo PHP_EOL;{/php}

接口地址：/api/{$tableUnderLineName}/delete

请求方式：get / post

请求数据：

| 参数名 | 数据类型  | 说明  |
| ---   | ---   | ---  |
| id |  int,require | 数据ID |


响应数据：(json格式)
```
{
    "code": 1, // 返回状态，1代表成功，0代表失败
    "msg": "success", // 返回消息
    "time": "1586490789", // 响应时间戳
    "data": null // 结果数据,json格式,没数据时为 null
}
```
{/if}



{if condition="$crud['update']"}

## 修改{$menuName} {php}echo PHP_EOL;{/php}

接口地址：/api/{$tableUnderLineName}/update

请求方式：get / post

请求数据：

| 参数名 | 数据类型  | 说明  |
| ---   | ---   | ---  |
| id |  int,require | 数据ID |
{volist name="$fieldsInfo" id="vo"}
{php}
       $field_key = $vo['Field'];
       if(in_array($field_key,['id','create_time','update_time'])){
           continue;
       }
       if($vo['Default'] === null){
          continue;
       }
       if(!empty($vo['Comment'])){
            $field_comment = $vo['Comment'] . " | ";
       }else{
            $field_comment = " | ";
       }
       $field_val = "";
       if(ifContain($vo["Type"],'int')){
           $field_val .= empty($field_val) ? "int" : ",int";
       }
       if(ifStartWith($vo["Type"],'varchar')){
           $maxLen = str_replace(['varchar(',')'],['',''],$vo["Type"]);
           $field_val .= empty($field_val) ? "max:".$maxLen : ",max:".$maxLen;
       }
{/php}

{notempty name="$field_val"}
|  {$field_key} |  {$field_val} | {$field_comment} {php}echo PHP_EOL;{/php}
{/notempty}

{/volist}


响应数据：(json格式)
```
{
    "code": 1, // 返回状态，1代表成功，0代表失败
    "msg": "success", // 返回消息
    "time": "1586490789", // 响应时间戳
    "data": null // 结果数据,json格式,没数据时为 null
}
```
{/if}