/**
 * Created by CodeGeneration
 * User: Tplay
 * Date: {:date('Y/m/d')} {php}echo PHP_EOL;{/php}
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use think\Db;
{php}
    //获取所有的多选下拉
    $xmselects = [];
    foreach($fieldsInfo as $vo){
        if($vo['Component'] == 'xm-select'){
            $xmselects[] = $vo;
            $AssociationTable = app\common\service\GenerationService::tableName2Hump($tableName . '_' . $vo['Field']);
            echo "use app\common\model\\".$AssociationTable.";".PHP_EOL;
        }
    }
{/php}

class {$tableHumpName} extends Permissions
{

{if condition="$crud['select']"}
    public function index()
    {
        $model = new \app\common\model\{$tableHumpName}();
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $where = [];
{volist name="$fieldsInfo" id="vo"}
                {php}
                    if(empty($vo['ShowSearch'])){
                        continue;
                    }
                {/php}

{if condition="$vo['Field'] == 'id'"}
            if (isset($post['ids']) and !empty($post['ids'])) {
                $where['id'] = ['in', $post['ids']];
            }
{php}continue;{/php}
{/if}

{if condition="in_array($vo['Component'],['input','text','img','checkbox','editor'])"}
            if (!empty($post["{$vo['Field']}"])) {
                $where["{$vo['Field']}"] = ['like', '%' . $post["{$vo['Field']}"] . '%'];
            }
{php}continue;{/php}
{/if}

{if condition="in_array($vo['Component'],['number','switch','select','radio'])"}
{if condition="$vo['Config']['istop']??false === true"}
            if (isset($post['{$vo['Field']}']) and ($post['{$vo['Field']}'] == 1 or $post['{$vo['Field']}'] === '0')) {
                $where['{$vo['Field']}'] = $post['{$vo['Field']}'] == 1 ? ['<>', 0] : 0;
            }
{else/}
            if (isset($post["{$vo['Field']}"]) and "" != $post["{$vo['Field']}"]) {
                $where["{$vo['Field']}"] = $post["{$vo['Field']}"];
            }
{/if}
{php}continue;{/php}
{/if}

{if condition="$vo['Component'] == 'time'"}
            if (isset($post["{$vo['Field']}"]) and !empty($post["{$vo['Field']}"])) {
                $timerang = explode(' - ', $post["{$vo['Field']}"]);
                $min_time = strtotime($timerang[0]);
                $max_time = $timerang[0] == $timerang[1] ? $min_time + 24 * 3600 - 1 : strtotime($timerang[1]??'');
                $where["{$vo['Field']}"] = [['>=', $min_time], ['<=', $max_time]];
            }
{php}continue;{/php}
{/if}

{if condition="$vo['Component'] == 'xm-select'"}
{php}
    $AssociationTableName = $tableName . '_' . $vo['Field'];//关联表
    $AssociationTable = app\common\service\GenerationService::tableName2Hump($AssociationTableName);//关联类名
    $foreignKey = app\common\service\GenerationService::tableName2UnderLine($tableName . '_id');//外键
{/php}

    if (isset($post['{$vo['Field']}']) and '' != $post['{$vo['Field']}']) {
        $ids = (new {$AssociationTable}())->where('{$vo['Field']}', 'in', $post['{$vo['Field']}'])->distinct(true)->column('{$foreignKey}');
        $where['id'] = ['in', $ids];
    }
{php}continue;{/php}
{/if}
{/volist}

            $count = $model->where($where)->count();
            $data = $model->where($where)->page($post['page']??0, $post['limit']??15)->order({if condition="array_key_exists('sort',$fieldsInfo)"}'sort desc,id desc'{else/}'id desc'{/if})->select();

            foreach ($data as $key => $value) {
{volist name="$fieldsInfo" id="vo"}
{if condition="$vo['Component'] == 'img' and ifContain($vo['Type'],'int')"}
                $value['{$vo['Field']}_url'] = getAttachmentUrl($value['{$vo['Field']}']);
{/if}
{if condition="in_array($vo['Component'],['switch','select','checkbox','radio','xm-select'])"}
                $value['{$vo['Field']}_text'] = $value->{$vo['Field']}_text;
{/if}
{/volist}
                $data[$key] = $value;
            }

            return array('code' => 0, 'count' => $count, 'data' => $data);
        } else {

    {volist name="$xmselects" id="vo"}
        $this->assign('{$vo['Field']}_xmselect_data_json', json_encode($model::get{$vo['HumpName']}List()));
    {/volist}

    {volist name="$fieldsInfo" id="vo"}
        {if condition="in_array($vo['Component'],['switch','select','checkbox','radio'])"}
        $this->assign('{$vo['Field']}_list', $model::get{$vo['HumpName']}List());
        {/if}
    {/volist}
            return $this->fetch();
        }
    }
{/if}



{if condition="$crud['update'] or $crud['create']"}

    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new \app\common\model\{$tableHumpName}();
        $post = $this->request->post();
        if ($this->request->isPost()) {
{volist name="$fieldsInfo" id="vo"}
{php}
            if(empty($vo['ShowEdit'])){
                continue;
            }
            if(in_array($vo['Field'],['id'])){
                continue;
            }
{/php}
{if condition="$vo['Component'] == 'time'"}
            if (isset($post['{$vo['Field']}']) && !empty($post['{$vo['Field']}'])) {
                $post['{$vo['Field']}'] = strtotime($post['{$vo['Field']}']);
            }
{/if}
{if condition="$vo['Component'] == 'checkbox'"}
            if (isset($post['{$vo['Field']}']) && !empty($post['{$vo['Field']}'])) {
                $post['{$vo['Field']}'] = implode(',',$post['{$vo['Field']}']);
            }
{/if}
{/volist}

            //验证
            $validate = new \think\Validate([
{volist name="$fieldsInfo" id="vo"}
                    {php}
                        if(empty($vo['ShowEdit'])){
                            continue;
                        }
                        if(in_array($vo['Component'],['xm-select'])){
                            continue;
                        }
                        $field_key = $vo['Field'];
                        if(in_array($field_key,['id','create_time','update_time'])){
                            continue;
                        }
                        if(!empty($vo['Comment'])){
                            $field_key.= "|" . $vo['Comment'];
                        }
                        $field_val = "";
                        if($vo['Default'] === null){
                            $field_val.= "require";
                        }
                        if(ifContain($vo["Type"],'int')){
                            $field_val .= empty($field_val) ? "number" : "|number";
                        }
                        if(ifStartWith($vo["Type"],'varchar')){
                            $maxLen = str_replace(['varchar(',')'],['',''],$vo["Type"]);
                            $field_val .= empty($field_val) ? "max:".$maxLen : "|max:".$maxLen;
                        }
                    {/php}
{notempty name="$field_val"}
                ['{$field_key}', '{$field_val}'],
{/notempty}
{/volist}
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        } else {
    {volist name="$xmselects" id="vo"}
        $this->assign('{$vo['Field']}_xmselect_data_json', json_encode($model::get{$vo['HumpName']}List()));
        $this->assign('{$vo['Field']}_xmselect_init', '');
    {/volist}
    {volist name="$fieldsInfo" id="vo"}
    {if condition="in_array($vo['Component'],['switch','select','checkbox','radio'])"}
        $this->assign('{$vo['Field']}_list', $model::get{$vo['HumpName']}List());
    {/if}
    {/volist}
    }

        if ($id > 0) {
{if condition="$crud['update']"}
            //修改
            $data = $model->where('id', $id)->find();
            if (empty($data)) {
                $this->error('id不正确');
            }
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {

    {volist name="$xmselects" id="vo"}
        $res = $this->save_relation_{$vo['Field']}($post, $id);
    {/volist}

                    $this->success('修改成功', 'index');
                }
            } else {
                $this->assign('data', $data);

    {volist name="$xmselects" id="vo"}
        $this->assign('{$vo['Field']}_xmselect_init', $data->{$vo['Field']}_value);
    {/volist}

                return $this->fetch();
            }
{/if}
        } else {
{if condition="$crud['create']"}
            //新增
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {

    {volist name="$xmselects" id="vo"}
        $res = $this->save_relation_{$vo['Field']}($post, $model->id);
    {/volist}
                    $this->success('添加成功', 'index');
                }
            } else {
                return $this->fetch();
            }
{/if}
        }
    }
{/if}



{volist name="$xmselects" id="vo"}
{php}
    $AssociationTable = app\common\service\GenerationService::tableName2Hump($tableName . '_' . $vo['Field']);
    $foreignKey = app\common\service\GenerationService::tableName2UnderLine($tableName . '_id');//外键
{/php}
    /**
    * @param $post
    * @param $id
    * @return bool
    */
    private function save_relation_{$vo['Field']}($post, $id)
    {
        $res = false;
        //保存中间表
        $relationModel = new {$AssociationTable}();
        $new_ids = explode(',', $post['{$vo['Field']}']);
        $old_ids = $relationModel->where('{$foreignKey}', $id)->column('{$vo['Field']}');

        foreach ($old_ids as $old_id) {
            if (!in_array($old_id, $new_ids)) {
                $relationModel->where(['{$vo['Field']}' => $old_id, '{$foreignKey}' => $id])->delete();
                $res = true;
            }
        }
        foreach ($new_ids as $new_id) {
            if (empty($new_id) && 0 !== $new_id) {
                continue;
            }
            if (!in_array($new_id, $old_ids)) {
                (new {$AssociationTable}())->save(['{$vo['Field']}' => $new_id, '{$foreignKey}' => $id]);
                $res = true;
            }
        }
        return $res;
    }
{/volist}




{if condition="$crud['delete']"}

    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            if (false == (new \app\common\model\{$tableHumpName}())->where('id', $id)->delete()) {
                $this->error('删除失败');
            } else {
                $this->success('删除成功', 'index');
            }
        }
    }

    public function deletes()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $ids = $post['ids'];
            if ((new \app\common\model\{$tableHumpName}())->where('id', 'in', $ids)->delete()) {
                $this->success('删除成功');
            }
        }
    }
{/if}


{volist name="$fieldsInfo" id="vo"}
{if condition="$vo['Component'] == 'switch'"}
{if condition="$vo['Config']['istop']??false === true"}
    
    public function switch_{$vo['Field']}()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $status = $post['status'] == 1 ? time() : 0;
            if (false == (new \app\common\model\{$tableHumpName}())->where('id', $post['id'])->update(['{$vo['Field']}' => $status])) {
                $this->error('设置失败');
            } else {
                $this->success('置顶成功', 'index');
            }
        }
    }
{else/}
    public function switch_{$vo['Field']}()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (false == (new \app\common\model\{$tableHumpName}())->where('id', $post['id'])->update(['{$vo['Field']}' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }
{/if}
{/if}
{/volist}


{if condition="array_key_exists('sort',$fieldsInfo)"}

    public function sort()
    {
        if ($this->request->isPost() && $this->request->has('ids')) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['ids'] as $k => $id) {
                $sort = Db::name('{$tableUnderLineName}')->where('id', $id)->value('sort');
                $newsort = $post['sorts'][$k]??$sort;
                if ($sort != $newsort) {
                    if (false == Db::name('{$tableUnderLineName}')->where('id', $id)->update(['sort' => $newsort])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            $this->success('成功更新' . $i . '个数据', 'index');
        } else {
            $this->error('无数据更新', 'index');
        }
    }
{/if}

}