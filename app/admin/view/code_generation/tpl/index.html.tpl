{php}
    //获取所有的多选下拉
    $xmselects = [];
    foreach($fieldsInfo as $vo){
        if($vo['Component'] == 'xm-select'){
            $xmselects[] = $vo;
        }
    }
{/php}
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{literal}__LAYUI__{/literal}/css/layui.css" media="all">
    <link rel="stylesheet" href="{literal}__PUBLIC__{/literal}/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="{literal}__CSS__{/literal}/admin.css" media="all">
</head>
<body style="padding:10px;">
<div class="tplay-body-div">

{if condition="$crud['create']"}
    <div class="layui-tab">
        <ul class="layui-tab-title">
            <li class="layui-this">列表</li>
            <li><a href="{literal}{:url('{/literal}publish{literal}')}{/literal}" class="a_menu">新增</a></li>
        </ul>
    </div>
{/if}

    <script type="text/html" id="toolbarDemo">
        <div class="layui-btn-container">
{if condition="array_key_exists('sort',$fieldsInfo) && ($fieldsInfo['sort']['ShowList'] == true)"}
         <button class="layui-btn layui-btn-sm" lay-submit lay-filter="admin">排序</button>
{/if}
{if condition="$crud['delete']"}
         <button class="layui-btn layui-btn-danger layui-btn-sm" lay-event="deletes">批量删除</button>
{/if}
        </div>
    </script>

    <form class="layui-form serch" action="index" method="post">
        <div class="layui-form-item" style="float: left;">

{php}
foreach($fieldsInfo as $vo){
    if(empty($vo['ShowSearch'])){
        continue;
    }

    if($vo['Field'] == 'id'){
        echo '<div class="layui-input-inline">
                    <input type="text" name="ids" autocomplete="off" placeholder="请输入ID,多个id逗号分隔" class="layui-input layui-btn-sm">
                </div>';
        continue;
    }

    if(in_array($vo['Component'],['input','text','img','editor'])){
        echo '<div class="layui-input-inline">
            <input type="text" name="'.$vo['Field'].'" autocomplete="off" placeholder="'. $vo['Comment'] .'（模糊搜索）" class="layui-input layui-btn-sm">
        </div>';
        continue;
    }

    if($vo['Component'] == 'number'){
    echo '<div class="layui-input-inline" style="width:100px">
                    <input type="number" name="'.$vo['Field'].'" autocomplete="off" placeholder="'. $vo['Comment'] .'" class="layui-input layui-btn-sm">
                </div>';
        continue;
    }

    if($vo['Component'] == 'time'){
    echo '<div class="layui-input-inline">
            <input type="text" class="layui-input" id="time_range_'.$vo['Field'].'" autocomplete="off" placeholder="'. $vo['Comment'] .'" name="'.$vo['Field'].'">
        </div>';
        continue;
    }

    if(in_array($vo['Component'],['switch','select','checkbox','radio'])){
       echo '<div class="layui-input-inline" style="width: 100px">
                <select name="'.$vo['Field'].'" lay-search="">
                    <option value="">'.$vo['Comment'].'</option>
                    {literal}
                    {foreach name="'.$vo['Field'].'_list" item="vo" key="k"}
                        <option value="{$k}">{$vo}</option>
                    {/foreach}
                    {/literal}
                </select>
            </div>';
        continue;
    }

    if(in_array($vo['Component'],['xm-select'])){
        echo '<div class="layui-input-inline" style="width:200px">
                <div id="'.$vo['Field'].'XmSelect" style="width: 200px;"></div>
            </div>';
        continue;
    }

}
{/php}

            <button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="serch">立即提交</button>
        </div>
    </form>

{if condition="$crud['update'] or $crud['delete']"}
    <script type="text/html" id="barDemo">
        <div class="layui-btn-group">
{if condition="$crud['update']"}
            <button class="layui-btn layui-btn-xs a_menu" lay-event="edit"><i class="layui-icon" style="margin-right: 0;"></i></button>
{/if}
{if condition="$crud['delete']"}
            <button class="layui-btn layui-btn-xs delete" lay-event="del"><i class="layui-icon" style="margin-right: 0;"></i></button>
{/if}
        </div>
    </script>
{/if}

    <table class="layui-table" id="table" lay-filter="table"></table>
    {literal}{include file="public/foot"}{/literal}

    <script type="text/javascript">
        layui.use(['table', 'layer', 'form', 'laydate'], function () {
            var table = layui.table,
                form = layui.form,
                layer = layui.layer;
            var laydate = layui.laydate;
            //第一个实例
            table.render({
                id: 'table'
                , elem: '#table'
                , size: 'sm' //小尺寸的表格
{if condition="array_key_exists('sort',$fieldsInfo) or $crud['delete']"}
                , toolbar: '#toolbarDemo'
{/if}
                , limit: 15
                , limits: [15, 20, 30, 40, 50, 100]
                , url: "{literal}{:url('{/literal}index{literal}')}{/literal}" //数据接口
                , page: true //开启分页
                , cols: [[ //表头
                    {type: 'checkbox'},
{volist name="$fieldsInfo" id="vo"}
{php}
    if(empty($vo['ShowList'])){
        continue;
    }
{/php}

{if condition="$vo['Field'] == 'id'"}
                    {field: 'id', title: 'ID', width: 60},
{php}continue;{/php}
{/if}

{if condition="$vo['Field'] == 'sort'"}
                    {field: 'sort', title: '排序', width: 60, templet: function (row) {
                        return '<input type="text" name="sorts[]" value="' + row.sort + '" style="width: 20px;" class="sort"><input type="hidden" name="ids[]" value="' + row.id + '">';
                    }},
{php}continue;{/php}
{/if}

{if condition="$vo['Field'] == 'filesize'"}
            {field: "filesize", title: '文件大小',templet:function (row) {return format_bytes(row.filesize,'');}},
{php}continue;{/php}
{/if}

{if condition="$vo['Component'] == 'img'"}
{if condition="ifContain($vo['Type'],'int')"}
                    {field: "{$vo['Field']}", title: '{$vo['Comment']}', width: 70, align: 'center', templet: function (row) {
                        return (row.{$vo['Field']}_url == '') ? '' : '<a href="' + row.{$vo['Field']}_url + '" class="tooltip" target="_blank"><img src="' + row.{$vo['Field']}_url + '"></a>';
                    }},
{else/}
                    {field: "{$vo['Field']}", title: '{$vo['Comment']}', width: 70, align: 'center', templet: function (row) {
                        return (row.{$vo['Field']} == '') ? '' : '<a href="' + row.{$vo['Field']} + '" class="tooltip" target="_blank"><img src="' + row.{$vo['Field']} + '"></a>';
                    }},
{/if}
{php}continue;{/php}
{/if}

{if condition="$vo['Component'] == 'switch'"}
{php}
$temp_str = ($vo['Config']['istop']??false === true) ? "!= 0" : "== 1";
{/php}
                    {field: "{$vo['Field']}", title: '{$vo['Comment']}', width: 60, templet: function (row) {
                        return '<a href="javascript:;" style="font-size:18px;" class="{$vo['Field']}" data-id="' + row.id + '" data-val="' + row.{$vo['Field']} + '">' + (row.{$vo['Field']} {$temp_str} ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>') + '</a>';
                    }},
{php}continue;{/php}
{/if}

{if condition="in_array($vo['Component'],['input','number','time','text','editor'])"}
                    {field: "{$vo['Field']}", title: '{$vo['Comment']}'},
{php}continue;{/php}
{/if}

{if condition="in_array($vo['Component'],['select','checkbox','radio','xm-select'])"}
        {field: "{$vo['Field']}_text", title: '{$vo['Comment']}'},
        {php}continue;{/php}
        {/if}
{/volist}

{if condition="$crud['update'] or $crud['delete']"}
                    {field: 'action', title: '操作', toolbar: '#barDemo', fixed: 'right'}
{/if}
                ]],
                done: function () {
                    if (isExitsFunction('showThumb')) {
                        showThumb()
                    }
{volist name="$fieldsInfo" id="vo"}
{if condition="$vo['Component'] == 'switch' && !empty($vo['ShowList'])"}
                    switchStatus('.{$vo['Field']}', "{literal}{:url('switch_{/literal}{$vo['Field']}{literal}')}{/literal}");
{/if}
{if condition="$vo['Component'] == 'time' && !empty($vo['ShowSearch'])"}
                    createTimeRang('time_range_{$vo['Field']}');
{/if}
{/volist}
                }
            });


            form.on('submit(serch)', function (data) {
                table.reload('table', {
                    where: data.field
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
                return false;
            });

            {if condition="$crud['update'] or $crud['delete']"}

            table.on('tool(table)', function (obj) {
                if (obj.event == 'edit') {
                 {if condition="$crud['update']"}

                    if (window.parent.document.querySelector("#container > div > ul")) {
                        window.parent.tab.tabAdd({
                            icon: "fa-bookmark",
                            id: "{$tableName}" + obj.data.id,
                            title: obj.data.title == null ? "{$menuName}" + obj.data.id : obj.data.title,
                            url: "/admin/{$tableUnderLineName}/publish?id=" + obj.data.id
                        });
                    }else{
                        location.href = "/admin/{$tableUnderLineName}/publish?id=" + obj.data.id
                    }
{/if}
                }
{if condition="$crud['delete']"}
                else if (obj.event == 'del') {
                    layer.confirm('确定要删除?', function (index) {
                        $.ajax({
                            url: "{literal}{:url('{/literal}delete{literal}')}{/literal}",
                            dataType: 'json',
                            data: {id: obj.data.id},
                            success: function (res) {
                                layer.msg(res.msg);
                                if (res.code == 1) {
                                    table.reload('table');
                                }
                            }
                        })
                    })
                }
{/if}
            });
{/if}

{if condition="$crud['delete']"}

            //监听事件
            table.on('toolbar(table)', function (obj) {
                if (obj.event == 'deletes') {
                    var checkStatus = table.checkStatus(obj.config.id);//获取选中的数据
                    var data = checkStatus.data;
                    if (data.length > 0) {
                        var ids = [];//数组
                        data.forEach(function (item, key) {
                            ids[key] = item.id;
                        })
                        layer.confirm('是否删除？', function (index, layero) {
                            $.ajax({
                                url: "{literal}{:url('{/literal}deletes{literal}')}{/literal}",
                                dataType: 'json',
                                data: {"ids": ids},
                                type: 'post',
                                success: function (res) {
                                    layer.msg(res.msg);
                                    if (res.code == 1) {
                                        table.reload('table');
                                    }
                                }
                            })
                            layer.close(index)
                        });
                    } else {
                        layer.msg('请先勾选需要操作的记录');
                    }
                }
            });
{/if}
        });
    </script>

{if condition="array_key_exists('sort',$fieldsInfo)"}
    <script>
        // 排序
        layui.use(['layer', 'form'], function () {
            var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form;
            $(window).on('load', function () {
                form.on('submit(admin)', function (data) {
                    $sort_eles = $('.sort');
                    $data = {};
                    if ($sort_eles.length > 0) {
                        for (var i = 0; i < $sort_eles.length; i++) {
                            $data['sorts[' + i + ']'] = $sort_eles[i].value;
                            $data['ids[' + i + ']'] = $sort_eles[i].nextSibling.value;
                        }
                    }
                    console.log($data)
                    $.ajax({
                        url: "{literal}{:url('{/literal}sort{literal}')}{/literal}",
                        data: $data,
                        type: 'post',
                        async: false,
                        success: function (res) {
                            if (res.code == 1) {
                                layer.alert(res.msg, function (index) {
                                    location.reload();
                                })
                            } else {
                                layer.msg(res.msg);
                            }
                        }
                    })
                    return false;
                });
            });
        });
    </script>
{/if}

    {volist name="$xmselects" id="vo"}
        <script>
            //加载组件-多选下拉
            layui.config({
                base: '__PUBLIC__/layui/extend/xmSelect/'
            }).extend({
                xmSelect: 'xm-select'
            }).use(['xmSelect'], function () {
                var xmSelect = layui.xmSelect;

                //渲染多选
                var demo1 = xmSelect.render({
                    el: '#{$vo['Field']}XmSelect',//容器id
                    tips: '{$vo['Comment']}',//输入框文字
                    name: '{$vo['Field']}',//输入框name
                    size: 'small',
                    toolbar: {
                        show: true,
                    },
                    filterable: true,
                    paging: true,
                    pageSize: 5,
                    model: {
                        label: {
                            type: 'block',
                            block: {
                                showCount: 1,//最大显示数量, 0:不限制
                                showIcon: true,//是否显示删除图标
                            }
                        }
                    },
                    data: {${$vo['Field']}_xmselect_data_json}, //数据
                })
            })
        </script>
    {/volist}
</div>
</body>
</html>
