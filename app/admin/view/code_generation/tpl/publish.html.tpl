{php}
    //获取所有的多选下拉
    $xmselects = [];
    foreach($fieldsInfo as $vo){
        if($vo['Component'] == 'xm-select'){
            $xmselects[] = $vo;
        }
    }
{/php}
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$menuName}编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{literal}__LAYUI__{/literal}/css/layui.css" media="all">
    <link rel="stylesheet" href="{literal}__PUBLIC__{/literal}/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="{literal}__CSS__{/literal}/admin.css" media="all">
    <script src="{literal}__LAYUI__{/literal}/layui.js"></script>
    <script src="{literal}__PUBLIC__{/literal}/jquery/jquery.min.js"></script>
</head>
<style>
    .layui-upload-img {
        cursor: pointer;
        width: 150px;
        height: 150px;
        background: url('{literal}__PUBLIC__{/literal}/images/uploadimg.jpg');
        background-size: contain;
        border-radius: 2px;
        border-width: 1px;
        border-style: solid;
        border-color: #e6e6e6;
    }
</style>
<body style="padding:10px;">
<div class="tplay-body-div">

    {literal}{empty name="$data"}{/literal}
    <div class="layui-tab">
        <ul class="layui-tab-title">
            <li><a href="index" class="a_menu">列表</a></li>
            <li class="layui-this">{literal}{notempty name="$data"}编辑{else/}新增{/notempty}{/literal}</li>
        </ul>
    </div>
    {literal}{/empty}{/literal}

    <div style="margin-top: 20px;"></div>
    <form class="layui-form" id="publish" method="post">

{volist name="$fieldsInfo" id="vo"}
{php}
  if(empty($vo['ShowEdit'])){
    continue;
  }
  if($vo['Field'] == 'id'){
    continue;
  }
    if(isset($vo['Config']['layui-word-aux'])){
        $layui_word_aux = '<div class="layui-form-mid layui-word-aux">'.$vo['Config']['layui-word-aux'].'</div>';
    }else if($vo['Default']===null){
        $layui_word_aux = '<div class="layui-form-mid layui-word-aux">必填</div>';
    }else{
        $layui_word_aux = '';
    }
{/php}

{if condition="$vo['Component'] == 'input'"}
                <!-- 输入框 -->
              <div class="layui-form-item">
                <label class="layui-form-label">{$vo['Comment']|default=$vo['Field']}</label>
                <div class="layui-input-inline" style="max-width:300px;">
                  <input name="{$vo['Field']}" {if condition="$vo['Default']===null"}lay-verify="required"{/if} autocomplete="off" placeholder="请输入" class="layui-input" type="text" {literal}{notempty name="$data"}value="{$data.{/literal}{$vo['Field']}{literal}}"{/notempty}{/literal}>
                </div>
                  {$layui_word_aux}
              </div>
{/if}

{if condition="$vo['Component'] == 'number'"}
              <!-- 数字 -->
              <div class="layui-form-item">
                <label class="layui-form-label">{$vo['Comment']|default=$vo['Field']}</label>
                <div class="layui-input-inline" style="max-width:300px;">
                  <input name="{$vo['Field']}" {if condition="$vo['Default']===null"}lay-verify="required"{/if} autocomplete="off" placeholder="请输入" class="layui-input" type="number" {literal}{notempty name="$data"}value="{$data.{/literal}{$vo['Field']}{literal}}"{/notempty}{/literal}>
                </div>
                  {$layui_word_aux}
              </div>
{/if}

{if condition="$vo['Component'] == 'time'"}
                <!-- 时间 -->
                <div class="layui-form-item">
                    <label class="layui-form-label">{$vo['Comment']|default=$vo['Field']}</label>
                    <div class="layui-input-inline" style="max-width:300px;">
                        <input name="{$vo['Field']}" id="{$vo['Field']}" {if condition="$vo['Default']===null"}lay-verify="required"{/if} autocomplete="off" placeholder="请输入" class="layui-input" type="text" {literal}{notempty name="$data"}value="{$data.{/literal}{$vo['Field']}{literal}}"{/notempty}{/literal}>
                    </div>
                    {$layui_word_aux}
                </div>
{/if}


{if condition="$vo['Component'] == 'text'"}
              <!-- 纯文本段落 -->
              <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">{$vo['Comment']|default=$vo['Field']}</label>
                <div class="layui-input-inline" style="width:80%;">
                  <textarea name="{$vo['Field']}" {if condition="$vo['Default']===null"}lay-verify="required"{/if} class="layui-textarea">{literal}{notempty name="$data"}{$data.{/literal}{$vo['Field']}{literal}}{/notempty}{/literal}</textarea>
                </div>
                  {$layui_word_aux}
              </div>
{/if}

{if condition="$vo['Component'] == 'editor'"}
        <!-- 文章富文本 -->
                {literal}
                    {php}$web_config = \think\Db::name('webconfig')->where('id', 1)->find();{/php}
                    {switch name="$web_config.article_editor" }
                    {case value="ueditor"} {include file="article/publish_ueditor" item="data" field="{/literal}{$vo['Field']}{literal}"}{/case}
                    {case value="tinymce"} {include file="article/publish_tinymce" item="data" field="{/literal}{$vo['Field']}{literal}"}{/case}
                    {case value="markdown"} {include file="article/publish_markdown" item="data" field="{/literal}{$vo['Field']}{literal}"}{/case}
                    {default /} {include file="article/publish_wangEditor" item="data" field="{/literal}{$vo['Field']}{literal}"}
                    {/switch}
                {/literal}
{/if}


{if condition="$vo['Component'] == 'img'"}
        <!-- 图片 -->
        <div class="layui-upload">
            <label class="layui-form-label">{$vo['Comment']|default=$vo['Field']}</label>
            <div class="layui-upload-list">
                <img class="layui-upload-img" id="upload_img_{$vo['Field']}" {literal}{notempty name="$data.{/literal}{$vo['Field']}{literal}"}src="{$data.{/literal}{$vo['Field']}{literal}|getAttachmentUrl}"{/notempty}{/literal}>
                <input type="hidden" id="upload_value_{$vo['Field']}" name="{$vo['Field']}" value='{literal}{notempty name="$data.{/literal}{$vo['Field']}{literal}"}{$data.{/literal}{$vo['Field']}{literal}}{/notempty}{/literal}'>
            </div>
        </div>
{/if}


{if condition="$vo['Component'] == 'switch'"}
        <!-- 开关 -->
        <div class="layui-form-item">
            <label class="layui-form-label">{$vo['Comment']}</label>
            <div class="layui-input-block">
                {php}
                    echo '{literal}
                    <input lay-skin="switch" lay-text="开启|关闭" type="checkbox" lay-filter="switch_'.$vo['Field'].'" {notempty name="$data"}{if condition="$data.'.$vo['Field'].' eq 1" }checked="" {/if}{/notempty} value="1">
                {/literal}';
                {/php}
            </div>
{$layui_word_aux}
        </div>
{/if}


{if condition="$vo['Component'] == 'select'"}
        <!-- 下拉框 -->
        <div class="layui-form-item">
          <label class="layui-form-label">{$vo['Comment']}</label>
            <div class="layui-input-inline" style="max-width:300px;">
                <select name="{$vo['Field']}" lay-filter="" lay-search=""{if condition="$vo['Default']===null"} lay-verify="required"{/if}>
                    <option value="">请选择</option>
                    {php}
                        echo '{literal}
                        {foreach name="'.$vo['Field'].'_list" item="vo" key="k"}
                        <option value="{$k}" {notempty name="$data"}{eq name="$data.'.$vo['Field'].'" value="$k"} selected=""{/eq}{/notempty}>{$vo}</option>
                        {/foreach}
                    {/literal}';
                    {/php}

                </select>
            </div>
{$layui_word_aux}
        </div>
{/if}

{if condition="$vo['Component'] == 'checkbox'"}
        <!-- 复选框 -->
                <div class="layui-form-item">
                  <label class="layui-form-label">{$vo['Comment']}</label>
                    <div class="layui-input-block">
                        {php}
                            echo '{literal}
                            {foreach name="'.$vo['Field'].'_list" item="vo" key="k"}
                            <input type="checkbox" name="'.$vo['Field'].'[]" value="{$k}" title="{$vo}" lay-skin="primary" {notempty name="$data"}{in name="$vo" value="$data.'.$vo['Field'].'" } checked {/in}{/notempty}>
                            {/foreach}
                        {/literal}';
                        {/php}
                    </div>
{$layui_word_aux}
                </div>
{/if}

{if condition="$vo['Component'] == 'radio'"}
        <!-- 单选框 -->
                <div class="layui-form-item">
                  <label class="layui-form-label">{$vo['Comment']}</label>
                    <div class="layui-input-block">
                        {php}
                            echo '{literal}
                            {foreach name="'.$vo['Field'].'_list" item="vo" key="k"}
                            <input type="radio" name="'.$vo['Field'].'" value="{$k}" title="{$vo}" {notempty name="$data"}{eq name="$data->getData(\''.$vo['Field'].'\')" value="$k" } checked {/eq}{/notempty}>
                            {/foreach}
                        {/literal}';
                        {/php}
                    </div>
{$layui_word_aux}
                </div>
{/if}

{if condition="$vo['Component'] == 'xm-select'"}
    <!-- 多选下拉框 -->
    <div class="layui-form-item">
        <label class="layui-form-label">{$vo['Comment']}</label>
        <div class="layui-input-inline" style="max-width:300px;">
            <div id="{$vo['Field']}_XmSelect" style="max-width:300px;"></div>
        </div>
        {$layui_word_aux}
    </div>
{/if}

{/volist}

        {literal}
        {notempty name="$data"}
          <input type="hidden" name="id" value="{$data.id}">
        {/notempty}
       {/literal}
      <div class="layui-form-item">
        <div class="layui-input-block">
          <button class="layui-btn" lay-submit lay-filter="admin">立即提交</button>
          <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
      </div>
    </form>

    <script>
        layui.use(['layer', 'form', 'laydate'], function () {
            var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                laydate = layui.laydate;

            $(window).on('load', function () {
                form.on('submit(admin)', function (data) {
                    $.ajax({
                        url: "{literal}{:url('{/literal}publish{literal}')}{/literal}",
                        data: $('#publish').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (res) {
                            if (res.code == 1) {
                                if (res.msg == "添加成功" || window.top == window.self) {
                                    layer.alert(res.msg, function (index) {
                                        location.href = res.url;
                                    })
                                } else {

                                    if (window.parent.document.querySelector("#container > div > ul")) {
                                        layer.confirm(res.msg, {
                                            btn: ['关闭', '继续编辑']
                                        }, function (index) {
                                            window.parent.tab.close('{$tableName}{literal}{$data.id|default=0}{/literal}');
                                        }, function (index, layero) {
                                            location.reload();
                                        });
                                    }else{
                                        location.href = res.url;
                                    }

                                }
                            } else {
                                layer.msg(res.msg);
                            }
                        }
                    })
                    return false;
                });

{volist name="$fieldsInfo" id="vo"}
{empty name="$vo['ShowEdit']"}
{php}continue;{/php}
{/empty}

{if condition="$vo['Component'] == 'time'"}
                    laydate.render({
                        elem: '#{$vo['Field']}', //指定元素
                        type: 'datetime',
                    });
{/if}

{if condition="$vo['Component'] == 'img'"}
                $('#upload_img_{$vo['Field']}').click(function () {
                    layer.open({
                        type: 2,
                        title: '选择图片',
                        area: ['570px', '485px'],
                        id: 'layerDemo', //防止重复弹出
                        anim: 4,
                        content: "{:url('attachment/selectimage')}?suffix=_{$vo['Field']}&isStr={if condition="ifContain($vo['Type'],'int')"}false{else/}true{/if}",
                        cancel: function () {
                            //右上角关闭回调
                        }
                    });
                })
{/if}

                {if condition="$vo['Component'] == 'switch'"}
                form.on('switch(switch_{$vo['Field']})', function(data){
                    $.ajax({
                        url: "{:url('switch_{$vo['Field']}')}",
                        data: {id: '{$data.id|default=0}', status: data.elem.checked ? 1 : 0},
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (res) {
                            layer.msg(res.msg);
                            if (res.code == 0) {
                                setTimeout(function () {
                                    location.reload();
                                }, 1500)
                            }
                        }
                    })
                });
                {/if}
{/volist}

            });
        });
    </script>


    {volist name="$xmselects" id="vo"}
        <script>
            //加载组件-多选下拉
            layui.config({
                base: '__PUBLIC__/layui/extend/xmSelect/'
            }).extend({
                xmSelect: 'xm-select'
            }).use(['xmSelect'], function () {
                var xmSelect = layui.xmSelect;

                //渲染多选
                var demo1 = xmSelect.render({
                    el: '#{$vo['Field']}_XmSelect',//容器id
                    tips: '多选项',//输入框文字
                    name: '{$vo['Field']}',//输入框name
                    size: 'small',
                    toolbar: {
                        show: true,
                    },
                    filterable: true,
                    paging: true,
                    pageSize: 5,
                    model: {
                        label: {
                            type: 'block',
                            block: {
                                showCount: 1,//最大显示数量, 0:不限制
                                showIcon: true,//是否显示删除图标
                            }
                        }
                    },
                    data: {${$vo['Field']}_xmselect_data_json}, //数据
                    initValue: [{${$vo['Field']}_xmselect_init}], //初始化选中的数据, 需要在data中存在
                })
            })
        </script>
    {/volist}
</div>
</body>
</html>