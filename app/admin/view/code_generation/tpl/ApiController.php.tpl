/**
 * Created by CodeGeneration
 * User: Tplay
 * Date: {:date('Y/m/d')} {php}echo PHP_EOL;{/php}
 */

namespace app\api\controller;

use app\api\controller\base\Permissions;

class {$tableHumpName} extends Permissions
{

{if condition="$crud['select']"}
    public function getOne()
    {
        $post = $this->request->param();
        $validate = new \think\Validate([
            ['id', 'number|require'],
        ]);
        if (!$validate->check($post)) {
            $this->json_error('提交失败：' . $validate->getError());
        }

        $where['id'] = $post['id'];

        $data = (new \app\common\model\{$tableHumpName}())->where($where)->find();
        if (empty($data)) {
            $this->json_error("没有数据");
        }
        $this->json_success("查询成功", $data);
    }

    public function getList()
    {
        $post = $this->request->param();
        $validate = new \think\Validate([
            ['page', 'number'],
            ['per_page', 'number|<=:1000']
        ]);
        if (!$validate->check($post)) {
            $this->json_error('提交失败：' . $validate->getError());
        }

        $where = [];
        $per_page = $this->request->param('per_page', 20, 'intval');
        $datalist = (new \app\common\model\{$tableHumpName}())->where($where)->paginate($per_page, true);
        if (empty($datalist)) {
            $this->json_error("没有数据");
        }
        $this->json_success("查询成功", $datalist);
    }
{/if}

{if condition="$crud['create']"}

    public function create()
    {
        $post = $this->request->param();
        //验证
        $validate = new \think\Validate([
{volist name="$fieldsInfo" id="vo"}
{php}
                $field_key = $vo['Field'];
                if(in_array($field_key,['id','create_time','update_time'])){
                    continue;
                }
                if(!empty($vo['Comment'])){
                    $field_key.= "|" . $vo['Comment'];
                }
                $field_val = "";
                if($vo['Default'] === null){
                    $field_val.= "require";
                }
                if(ifContain($vo["Type"],'int')){
                    $field_val .= empty($field_val) ? "number" : "|number";
                }
                if(ifStartWith($vo["Type"],'varchar')){
                    $maxLen = str_replace(['varchar(',')'],['',''],$vo["Type"]);
                    $field_val .= empty($field_val) ? "max:".$maxLen : "|max:".$maxLen;
                }
{/php}
{notempty name="$field_val"}
            ['{$field_key}', '{$field_val}'],
{/notempty}
{/volist}
        ]);
        if (!$validate->check($post)) {
            $this->error('提交失败：' . $validate->getError());
        }

        $model = new \app\common\model\{$tableHumpName}();
        $res = $model->allowField(true)->save($post);
        if ($res) {
            $this->json_success("创建成功", $model);
        } else {
            $this->json_error("创建失败");
        }
    }
{/if}

{if condition="$crud['delete']"}

    public function delete()
    {
        $post = $this->request->param();
        $validate = new \think\Validate([
            ['id', 'require|number'],
        ]);
        if (!$validate->check($post)) {
            $this->json_error('提交失败：' . $validate->getError());
        }

        $item = (new \app\common\model\{$tableHumpName}())->where(['id' => $post['id']])->find();
        if (!$item) {
            $this->json_error('找不到该id');
        }

        if (!$item->delete()) {
            $this->json_error('删除失败');
        }
        $this->json_success('删除成功');
    }
{/if}

{if condition="$crud['update']"}

    public function update()
    {
        $post = $this->request->param();
        //验证
        $validate = new \think\Validate([
            ['id', 'require|number'],
{volist name="$fieldsInfo" id="vo"}
{php}
                $field_key = $vo['Field'];
                if(in_array($field_key,['id','create_time','update_time'])){
                    continue;
                }
                if($vo['Default'] === null){
                    continue;
                }
                if(!empty($vo['Comment'])){
                    $field_key.= "|" . $vo['Comment'];
                }
                $field_val = "";
                if(ifStartWith($vo["Type"],'int') || ifStartWith($vo["Type"],'tinyint')){
                    $field_val .= empty($field_val) ? "number" : "|number";
                }
                if(ifStartWith($vo["Type"],'varchar')){
                    $maxLen = str_replace(['varchar(',')'],['',''],$vo["Type"]);
                    $field_val .= empty($field_val) ? "max:".$maxLen : "|max:".$maxLen;
                }
{/php}
{notempty name="$field_val"}
            ['{$field_key}', '{$field_val}'],
{/notempty}
{/volist}
        ]);
        if (!$validate->check($post)) {
            $this->json_error('提交失败：' . $validate->getError());
        }

        $item = (new \app\common\model\{$tableHumpName}())->where(['id' => $post['id']])->find();
        if (!$item) {
            $this->json_error('找不到该id');
        }

        if ($item->allowField(true)->save($post)) {
            $this->json_success("修改成功");
        } else {
            $this->json_error("修改失败");
        }
    }
{/if}

}