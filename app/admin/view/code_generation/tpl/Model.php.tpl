/**
 * Created by CodeGeneration
 * User: Tplay
 * Date: {:date('Y/m/d')} {php}echo PHP_EOL;{/php}
 */

namespace app\common\model;

use think\Model;
use app\common\service\Constant;

class {$tableHumpName} extends Model
{

{php}
    if(!array_key_exists('create_time',$fieldsInfo) && !array_key_exists('update_time',$fieldsInfo)){
        echo '    protected $autoWriteTimestamp = false;';
    }else if(!array_key_exists('update_time',$fieldsInfo)){
        echo '    protected $updateTime = false;';
    }else if(!array_key_exists('create_time',$fieldsInfo)){
        echo '    protected $createTime = false;';
    }
{/php}


{volist name="$fieldsInfo" id="vo"}
{php}
    if(in_array($vo['Field'],['id','create_time','update_time'])){
        continue;
    }
    $const_name = strtoupper($vo['HumpName']);
    $options = $vo['Config']['options']??null;
{/php}

{if condition="$vo['Component'] == 'time'"}
    {php}echo PHP_EOL;{/php}
    //{$vo['Field']} {php}echo PHP_EOL;{/php}
    public function get{$vo['HumpName']}Attr($value, $data)
    {
        return $value ? date('Y-m-d H:i:s', $value) : '';
    }
{/if}

{if condition="in_array($vo['Component'],['switch','select','checkbox','radio'])"}
    {php}echo PHP_EOL;{/php}
    /**
     * {$vo['Comment']}字典
     * {if condition="is_numeric($options)"}@param bool $status_open [false所有的，true上架的]{/if}{php}echo PHP_EOL;{/php}
     */
    public static function get{$vo['HumpName']}List({if condition="is_numeric($options)"}$status_open = true{/if})
    {
{if condition="is_array($options)"}
        return {php}echo str_replace(["array (",")","\n"," "],["[","]","",""],var_export($options?:[],true));{/php};
{elseif condition="is_numeric($options)"/}
        $where = ['pid' => {$options}];
        if ($status_open) {
            $where['status'] = Constant::STATUS_OPEN;
        }
        return (new ConfigOption())->where($where)->order('sort desc')->field('name,value')->column('name','value');
{else}
        return [0 => '0', 1 => '1'];
{/if}
    }


    //{$vo['Field']}_text {php}echo PHP_EOL;{/php}
    public function get{$vo['HumpName']}TextAttr($value, $data)
    {
        return self::get{$vo['HumpName']}List()[$data['{$vo['Field']}']]??'';
    }
{/if}

{if condition="in_array($vo['Component'],['xm-select'])"}
    {php}
        $options = $options?:0;
        $AssociationTable = app\common\service\GenerationService::tableName2Hump($tableName . '_' . $vo['Field']);//关联对象
        $foreignKey = app\common\service\GenerationService::tableName2UnderLine($tableName . '_id');//外键
        $localKey = 'id';
    {/php}

    /**
     * {$vo['Comment']}字典
     * {if condition="is_numeric($options)"}@param bool $status_open [false所有的，true上架的]{/if}{php}echo PHP_EOL;{/php}
     */
    public static function get{$vo['HumpName']}List({if condition="is_numeric($options)"}$status_open = true{/if})
    {
{if condition="is_array($options)"}
        {php}
            $options = $options?:[];
            // 转换数组
            $newList = array_map(function ($value, $key) {
                return ['name' => $value, 'value' => $key];
            }, array_values($options), array_keys($options));
            $code = str_replace(["array (",")","\n"," "],["[","]","",""],var_export($newList,true));
        {/php}
        return {$code};
{elseif condition="is_numeric($options)"/}
        $where = ['pid' => {$options}];
        if ($status_open) {
            $where['status'] = Constant::STATUS_OPEN;
        }
        $list = (new ConfigOption())->where($where)->order('sort desc')->field('name,value')->select();

        //如果value为空，则使用name当value
        foreach ($list as $k => $v) {
            if (empty($v->value)) {
                $list[$k]['value'] = $v['name'];
            }
        }
        return $list;
{else/}
        return [];
{/if}
    }

    //{$vo['Field']}_text {php}echo PHP_EOL;{/php}
    public function get{$vo['HumpName']}TextAttr($value, $data)
    {
        $list = self::get{$vo['HumpName']}List(false);
        $all = [];
        foreach ($list as $v) {
            $all[$v['value']] = $v['name'];
        }

        $values = (new {$AssociationTable}())->where('{$foreignKey}', $this->id)->column('{$vo['Field']}');
        $names = [];
        foreach ($values as $val) {
            $names[] = $all[$val]??'';
        }
        return implode(',', $names);
    }

    //{$vo['Field']}_value {php}echo PHP_EOL;{/php}
    public function get{$vo['HumpName']}ValueAttr($value, $data)
    {
        $values = (new {$AssociationTable}())->where('{$foreignKey}', $this->id)->column('{$vo['Field']}');
        $arr = [];
        foreach ($values as $v) {
            $arr[] = "'$v'";
        }
        return implode(',', $arr);
    }
{/if}
{/volist}

}
