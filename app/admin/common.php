<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

// +----------------------------------------------------------------------
// 如果只需要给admin模块添加函数，在此文件中定义函数即可，系统会自动加载
// +----------------------------------------------------------------------

/**
 * 字节大小 格式化为合适的单位
 * @param  number $size 字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string   格式化后的带单位的大小
 */
function formatBytes($size, $delimiter = '')
{
    return \string\Format::formatBytes($size, $delimiter);
}