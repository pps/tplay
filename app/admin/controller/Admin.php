<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2023/02/05
 * Time: 20:33
 */


namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\admin\model\Admin as adminModel;
use think\Db;
use think\Session;


class Admin extends Permissions
{

    /**
     * 管理员列表
     * @return mixed
     */
    public function index()
    {
        $model = new adminModel();
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $where = [];
            if (isset($post['keywords']) and !empty($post['keywords'])) {
                $where['nickname'] = ['like', '%' . $post['keywords'] . '%'];
            }
            if (isset($post['admin_cate_id']) and $post['admin_cate_id'] > 0) {
                $where['admin_cate_id'] = $post['admin_cate_id'];
            }
            if (isset($post['create_time']) and !empty($post['create_time'])) {
                $min_time = strtotime($post['create_time']);
                $max_time = $min_time + 24 * 60 * 60;
                $where['create_time'] = [['>=', $min_time], ['<=', $max_time]];
            }

            $count = $model->where($where)->count();
            $data = $model->where($where)->page($post['page']??0, $post['limit']??15)->order('admin_cate_id desc')->select();

            foreach ($data as $k => $v) {
                $v['cate_name'] = $v->admincate->name;
                $v['head_pic'] = getAttachmentUrl($v->thumb, '/static/public/images/tx.jpg');
                $data[$k] = $v;
            }

            return array('code' => 0, 'count' => $count, 'data' => $data);
        } else {
            $this->assign('cate', Db::name('admin_cate')->select());
            return $this->fetch();
        }
    }


    /**
     * 管理员的添加及修改
     * @return mixed
     */
    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new adminModel();
        if ($id > 0) {
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['password', 'confirm', '两次密码不一致'],
                    ['admin_cate_id', 'require', '请选择角色'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }
                //验证昵称是否存在
                $nickname = $model->where(['nickname' => $post['nickname'], 'id' => ['neq', $post['id']]])->select();
                if (!empty($nickname)) {
                    $this->error('提交失败：该昵称已被占用');
                }
                //密码处理
                if (!empty($post['password'])) {
                    $post['password'] = password($post['password']);
                } else {
                    unset($post['password']);
                }
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改管理员信息成功', 'admin/admin/index');
                }
            } else {
                $info['admin'] = $model->where('id', $id)->find();
                $info['admin_cate'] = Db::name('admin_cate')->select();
                $this->assign('info', $info);
                return $this->fetch();
            }
        } else {
            //是新增操作
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['name|账号', 'require|alphaDash'],
                    ['password', 'require|confirm', '密码不能为空|两次密码不一致'],
                    ['password_confirm', 'require', '重复密码不能为空'],
                    ['admin_cate_id', 'require', '请选择角色'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }
                //验证用户名是否存在
                $name = $model->where('name', $post['name'])->select();
                if (!empty($name)) {
                    $this->error('提交失败：该用户名已被注册');
                }
                //验证昵称是否存在
                $nickname = $model->where('nickname', $post['nickname'])->select();
                if (!empty($nickname)) {
                    $this->error('提交失败：该昵称已被占用');
                }
                //密码处理
                $post['password'] = password($post['password']);
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加管理员失败');
                } else {
                    $this->success('添加管理员成功', 'admin/admin/index');
                }
            } else {
                $info['admin_cate'] = Db::name('admin_cate')->select();
                $this->assign('info', $info);
                return $this->fetch();
            }
        }
    }

    /**
     * 管理员删除
     */
    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
            if ($id == 1) {
                $this->error('网站所有者不能被删除');
            }
            if ($id == Session::get(self::ADMIN_ID)) {
                $this->error('自己不能删除自己');
            }
            if (false == Db::name('admin')->where('id', $id)->delete()) {
                $this->error('删除失败');
            } else {
                $this->success('删除成功', 'admin/admin/index');
            }
        }
    }

    //重置密码
    public function resetpass()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
            if ($id == 1) {
                $this->error('admin不能被重置');
            }
            if ($id == Session::get(self::ADMIN_ID)) {
                $this->error('不能重置自己账号');
            }
            if (false == Db::name('admin')->where('id', $id)->update(['password' => password(123456)])) {
                $this->error('重置失败');
            } else {
                $this->success('重置成功', 'admin/admin/index');
            }
        }
    }

    /**
     * 管理员个人资料修改
     * @return mixed
     */
    public function personal()
    {
        $id = Session::get(self::ADMIN_ID);
        $model = new adminModel();
        if ($id > 0) {
            $admin = $model->where('id', $id)->find();
            if ($this->request->isPost()) {
                $thumb = $this->request->post('thumb');
                $nickname = $this->request->post('nickname');
                if (false == $admin->save(['thumb' => $thumb, 'nickname' => $nickname])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改个人信息成功', 'admin/admin/personal');
                }
            } else {
                $this->assign('info', $admin);
                return $this->fetch();
            }
        } else {
            $this->error('id不正确');
        }
    }

    /**
     * 修改密码
     * @return mixed
     */
    public function editPassword()
    {
        if ($this->request->isPost()) {
            $id = Session::get(self::ADMIN_ID);
            $post = $this->request->post();
            $validate = new \think\Validate([
                ['password', 'require', '原密码不能为空'],
                ['password', 'require|confirm', '新密码不能为空|确认密码不一致'],
                ['password_confirm', 'require', '确认密码不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
            $admin = Db::name('admin')->where('id', $id)->find();
            if (password($post['password_old']) == $admin['password']) {
                if (false == Db::name('admin')->where('id', $id)->update(['password' => password($post['password'])])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功', 'admin/main/index');
                }
            } else {
                $this->error('原密码错误');
            }
        } else {
            return $this->fetch();
        }
    }

}
