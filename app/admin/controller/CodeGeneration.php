<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\common\service\GenerationService;
use file\FileHelper;
use think\Db;
use think\Exception;

class CodeGeneration extends Permissions
{
    protected function _initialize()
    {
        stop_online();
        parent::_initialize();
    }

    /**
     * 展示表
     * @return mixed
     */
    public function index()
    {
        $dbName = \think\Env::get("db_name", "");

        //查询所有表信息
        $tables = Db::query("SELECT TABLE_NAME,TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=?", [$dbName]);

        foreach ($tables as $k => $table) {
            $tableName = $table['TABLE_NAME'];
            //排除/common/已经存在模型的表
            $humpName = GenerationService::tableName2Hump($tableName);
            $path = APP_PATH . 'common' . DS . 'model' . DS . $humpName . '.php';
            if (file_exists($path) && ifContain(file_get_contents($path), 'Created by')) {
                unset($tables[$k]);
            }
            //排除/admin/已经存在模型的表
            $path = APP_PATH . 'admin' . DS . 'model' . DS . $humpName . '.php';
            if (file_exists($path) && ifContain(file_get_contents($path), 'Created by')) {
                unset($tables[$k]);
            }
            //处理表注释
//            $comment = $table['TABLE_COMMENT'];
//            if (ifStartWith($comment, '{')) {
//                $config = json_decode($comment, true);
//                $tables[$k]['Remark'] = $comment;//记录原始注释
//                if (is_array($config)) {
//                    $tables[$k]['TABLE_COMMENT'] = $config['menuName'];//显示的注释
//                    $tables[$k]['Config'] = $config;//记录json_decode
//                }
//            }
        }

        $this->assign('tables', $tables);

        return $this->fetch();
    }


    /**
     * 展示字段信息
     * @return array
     */
    public function getFieldsInfo()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $validate = new \think\Validate([
                ['table', 'require|max:50', '请选择数据库表'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
            $nameAndComment = explode('|', $post['table']);
            $tableName = $nameAndComment[0]??'';
            $fieldsInfo = GenerationService::getFieldsInfo($tableName);
            foreach ($fieldsInfo as $k => $item) {
                try {
                    if (ifStartWith($item["Comment"], "{")) {
                        $config = json_decode($item["Comment"], true);
                        $fieldsInfo[$k]["Remark"] = $fieldsInfo[$k]["Comment"];//记录原始注释
                        if (is_array($config)) {
                            $fieldsInfo[$k]["Comment"] = $config['comment']??$item['Field'];//显示的注释
                            $fieldsInfo[$k]["Config"] = $config;//记录json_decode
                        } else {
                            $fieldsInfo[$k]["Comment"] = 'json解析失败';//显示的注释
                        }
                    } else {
                        $fieldsInfo[$k]["Comment"] = $fieldsInfo[$k]["Comment"] ?: $item['Field'];//显示的注释
                    }
                } catch (Exception $e) {
                    $this->json_layui_error($e->getMessage());
                }
            }
            $this->json_layui_success($fieldsInfo, count($fieldsInfo));
        }
    }


    /**
     * 生成
     */
    public function generation()
    {
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['table', 'require|max:50', '请选择数据库表'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        }

        $nameAndComment = explode('|', $post['table']);
        $tableName = $nameAndComment[0]??'';
        $menuName = $nameAndComment[1] ?: $tableName;
        $humpName = GenerationService::tableName2Hump($tableName);
        $underLineName = GenerationService::tableName2UnderLine($tableName);
        $crud = $post['crud']??[];
        $crud['create'] = array_key_exists('create', $crud);
        $crud['delete'] = array_key_exists('delete', $crud);
        $crud['update'] = array_key_exists('update', $crud);
        $crud['select'] = array_key_exists('select', $crud);
        $tplNames = $post['tplNames']??[];
        $tplNames['AdminController.php'] = array_key_exists('AdminController.php', $tplNames);
        $tplNames['Model.php'] = array_key_exists('Model.php', $tplNames);
        $tplNames['index.html'] = array_key_exists('index.html', $tplNames);
        $tplNames['publish.html'] = array_key_exists('publish.html', $tplNames);
        $tplNames['ApiController.php'] = array_key_exists('ApiController.php', $tplNames);
        $tplNames['ApiDoc.md'] = array_key_exists('ApiDoc.md', $tplNames);


        //模板目录
        $tpdir = APP_PATH . 'admin' . DS . 'view' . DS . 'code_generation' . DS . 'tpl' . DS;
        //生成路径
        if ($tplNames['AdminController.php']) {
            $AdminControllerPath = APP_PATH . 'admin' . DS . 'controller' . DS . $humpName . '.php';
        }
        if ($tplNames['Model.php']) {
            $ModelPath = APP_PATH . 'common' . DS . 'model' . DS . $humpName . '.php';
        }
        if ($tplNames['index.html']) {
            $IndexPath = APP_PATH . 'admin' . DS . 'view' . DS . $underLineName . DS . 'index.html';
        }
        if ($tplNames['publish.html']) {
            $PublishPath = APP_PATH . 'admin' . DS . 'view' . DS . $underLineName . DS . 'publish.html';
        }
        if ($tplNames['ApiController.php']) {
            $ApiControllerPath = APP_PATH . 'api' . DS . 'controller' . DS . $humpName . '.php';
        }
        if ($tplNames['ApiDoc.md']) {
            $ApiDocPath = APP_PATH . 'api' . DS . 'controller' . DS . $humpName . '.md';
        }


        //检查文件是否已存在
        if (!$this->request->has('cover')) {
            $checkMsg = "";
            if (isset($AdminControllerPath) && file_exists($AdminControllerPath)) {
                $checkMsg .= str_replace(APP_PATH, '', $AdminControllerPath) . " 已存在" . '</br>';
            }
            if (isset($ModelPath) && file_exists($ModelPath)) {
                $checkMsg .= str_replace(APP_PATH, '', $ModelPath) . " 已存在" . '</br>';
            }
            if ($crud['select']) {
                if (isset($IndexPath) && file_exists($IndexPath)) {
                    $checkMsg .= str_replace(APP_PATH, '', $IndexPath) . " 已存在" . '</br>';
                }
            }
            if ($crud['update'] or $crud['create']) {
                if (isset($PublishPath) && file_exists($PublishPath)) {
                    $checkMsg .= str_replace(APP_PATH, '', $PublishPath) . " 已存在" . '</br>';
                }
            }
            if (isset($ApiControllerPath) && file_exists($ApiControllerPath)) {
                $checkMsg .= str_replace(APP_PATH, '', $ApiControllerPath) . " 已存在" . '</br>';
            }
            if (isset($ApiDocPath) && file_exists($ApiDocPath)) {
                $checkMsg .= str_replace(APP_PATH, '', $ApiDocPath) . " 已存在" . '</br>';
            }
            if (!empty($checkMsg)) {
                $checkMsg .= "<span style='color:red;'>确认生成并覆盖？</span>";
                $this->error($checkMsg, null, 'confirm');
            }
        }


        //准备生成的数据
        $fieldsInfo = GenerationService::getFieldsInfo($tableName);
        foreach ($fieldsInfo as $k => $item) {
            $field = $item['Field'];
            try {
                $fieldsInfo[$k]["ShowList"] = $post[$field . "_ShowList"]??'';//'on' || ''
                $fieldsInfo[$k]["ShowSearch"] = $post[$field . "_ShowSearch"]??'';//'on' || ''
                $fieldsInfo[$k]["ShowEdit"] = $post[$field . "_ShowEdit"]??'';//'on' || ''
                $fieldsInfo[$k]["Component"] = $post[$field . "_Component"];//number
                $fieldsInfo[$k]["HumpName"] = GenerationService::underLine2Hump($field);
                $fieldsInfo[$k]["Remark"] = $fieldsInfo[$k]["Comment"];//记录Comment值
                if (ifStartWith($item["Comment"], "{")) {
                    $config = json_decode($item["Comment"], true);
                    $config = is_array($config) ? $config : [];
                    $fieldsInfo[$k]["Config"] = $config;
                    $fieldsInfo[$k]["Comment"] = $config['comment']??$field;//注释
                } else {
                    $fieldsInfo[$k]["Comment"] = $fieldsInfo[$k]["Comment"] ?: $item['Field'];//注释
                }
            } catch (Exception $e) {
                if (ifContain($e->getMessage(), 'Undefined index:')) {
                    $this->error("未定义字段 {$field} ,请刷新页面后重试", '', $fieldsInfo[$k]);
                } else {
                    $this->error('提交失败：' . $e->getMessage());
                }
            }
        }

        $tpData = [
            'tableName' => $tableName,
            'menuName' => $menuName,
            'tableHumpName' => $humpName,
            'tableUnderLineName' => $underLineName,
            'fieldsInfo' => $fieldsInfo,
            'crud' => $crud,
        ];

        //开始生成
        if (isset($AdminControllerPath)) {
            $content = $this->fetch($tpdir . 'AdminController.php.tpl', $tpData);
            FileHelper::save($AdminControllerPath, "<?php" . PHP_EOL . $content);
        }
        if (isset($ModelPath)) {
            $content = $this->fetch($tpdir . 'Model.php.tpl', $tpData);
            FileHelper::save($ModelPath, "<?php" . PHP_EOL . $content);
        }
        if (isset($IndexPath)) {
            if ($crud['select']) {
                $content = $this->fetch($tpdir . 'index.html.tpl', $tpData);
                FileHelper::save($IndexPath, $content);
            }
        }
        if (isset($PublishPath)) {
            if ($crud['update'] or $crud['create']) {
                $content = $this->fetch($tpdir . 'publish.html.tpl', $tpData);
                FileHelper::save($PublishPath, $content);
            }
        }
        if (isset($ApiControllerPath)) {
            $content = $this->fetch($tpdir . 'ApiController.php.tpl', $tpData);
            FileHelper::save($ApiControllerPath, "<?php" . PHP_EOL . $content);
        }
        if (isset($ApiDocPath)) {
            $content = $this->fetch($tpdir . 'ApiDoc.md.tpl', $tpData);
            FileHelper::save($ApiDocPath, $content);
        }

        //处理多选下拉,生成中间表
        GenerationService::createRelationTable($tableName, $fieldsInfo);

        //保存配置到数据库
        GenerationService::updateFieldsComment($tableName, $fieldsInfo);
//        GenerationService::updateTableComment($tableName, $menuName, $crud, $tplNames);

        $this->success("success", '', $tpData);
    }

}