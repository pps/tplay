<?php
// +----------------------------------------------------------------------
// | Tplay [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://tplay.pengyichen.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 听雨 < 389625819@qq.com >
// +----------------------------------------------------------------------


namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\admin\model\Urlconfig;
use think\Cache;
use think\Db;

class Urlsconfig extends Permissions
{
    public function index()
    {
        $model = new Urlconfig();
        $urlconfig = $model->order('create_time desc')->paginate(20);
        $this->assign('urlconfig', $urlconfig);
        return $this->fetch();
    }


    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new Urlconfig();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['url|需要美化的url', 'require'],
                ['aliases|美化后的url', 'require|regex:^[a-zA-Z_]+\S*$'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        }
        if ($id > 0) {
            //是修改操作
            if ($this->request->isPost()) {
                $urlconfig = $model->where('id', $id)->find();
                if (empty($urlconfig)) {
                    $this->error('id不正确');
                }
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    Cache::clear();
                    $this->success('修改成功,并清理缓存', 'admin/urlsconfig/index');
                }
            } else {
                $urlconfig = $model->where('id', $id)->find();
                if (!empty($urlconfig)) {
                    $this->assign('urlconfig', $urlconfig);
                    return $this->fetch();
                } else {
                    $this->error('id不正确');
                }
            }
        } else {
            //是新增操作
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {
                    Cache::clear();
                    $this->success('添加成功,并清理缓存', 'admin/urlsconfig/index');
                }
            } else {
                return $this->fetch();
            }
        }
    }


    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            if (false == Db::name('urlconfig')->where('id', $id)->delete()) {
                $this->error('删除失败');
            } else {
                Cache::clear();
                $this->success('删除成功,并清理缓存', 'admin/urlsconfig/index');
            }
        }
    }


    public function status()
    {
        $id = $this->request->param('id', 0, 'intval');
        if ($id > 0) {
            if ($this->request->isPost()) {
                //是提交操作
                $post = $this->request->post();
                $status = $post['status'];
                if (false == Db::name('urlconfig')->where('id', $id)->update(['status' => $status])) {
                    $this->error('设置失败');
                } else {
                    Cache::clear();
                    $this->success('设置成功,并清理缓存', 'admin/urlsconfig/index');
                }
            }
        } else {
            $this->error('id不正确');
        }
    }
}
