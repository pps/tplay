<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller\base;

use app\admin\model\Urlconfig;
use app\common\behavior\AdminLogBehavior;
use app\common\service\WebService;
use think\Controller;
use think\Db;
use think\exception\HttpResponseException;
use think\Hook;
use think\Session;

class Permissions extends Controller
{
    const ADMIN_ID = 'admin';
    const ADMIN_NAME = 'admin_name';
    const ADMIN_CATE_ID = 'admin_cate_id';
    const ADMIN_THUMB = 'admin_thumb';
    const BASE_CLASS = '\app\admin\controller\base\Permissions';

    protected function _initialize()
    {
        (new WebService())->checkInstalled();

        Hook::listen('admin_log');

        if ($this->request->isCli()) {
            return;
        }

        //检查ip黑名单
        $black_ip = \app\common\model\Webconfig::getValue('black_ip', 3600);
        if (!empty($black_ip)) {
            $black_ip = explode(',', $black_ip);
            $ip = $this->request->ip();
            if (in_array($ip, $black_ip)) {
                //退出登录
                if (Session::has(self::ADMIN_ID)) {
                    Session::delete(self::ADMIN_ID);
                }
                $this->error('你已被封禁！', 'admin/common/login');
            }
        }

        //检查是否登录
        if (!Session::has(self::ADMIN_ID)) {
            if ((new Urlconfig())->isWeekBackend()) {
                $this->redirect('admin/common/login');
            } else {
                $this->error('请使用安全入口登入', '/');
            }
        }

        //检查访问的url是否再用户的权限范围内,mysql查询自动忽略了大小写
        $where['module'] = $this->request->module();
        $where['controller'] = $this->request->controller();
        $where['function'] = $this->request->action();
        $where['type'] = 1;//权限节点

        //用户的权限菜单id
        $menus = $this->getPermission();
        $this->assign("permissions", $menus);

        $string = $this->request->query();
        $param_menu = Db::name('admin_menu')->where($where)->where('parameter', $string)->find();
        if ($param_menu) {
            if (false == in_array($param_menu['id'], $menus)) {
                (new AdminLogBehavior())->updateLastLog("缺少权限");
                $this->error('缺少权限');
            }
        } else if ($string) {
            $menu = Db::name('admin_menu')->where($where)->where('parameter', '')->find();
            if ($menu && !in_array($menu['id'], $menus)) {
                (new AdminLogBehavior())->updateLastLog("缺少权限");
                $this->error('缺少权限');
            }
        }
        if ($this->request->has('check_permission')) {
            $this->success('has permission');
        }
    }

    /**
     * 获取当前用户所有权限
     * @return array|mixed 菜单id数组
     */
    public function getPermission()
    {
        //用户的权限菜单id
        $menus = Db::name('admin_cate')->where('id', Session::get(self::ADMIN_CATE_ID))->value('permissions');
        return explode(',', $menus);
    }

    /**
     * layui表格成功的响应
     * @param $data null 响应数据
     * @param $count int 响应数据量
     * @param $msg string 测试调试信息
     */
    protected function json_layui_success($data = null, $count = 0, $msg = '')
    {
        $result = [
            'code' => 0,
            'count' => $count,
            'data' => $data,
            'msg' => $msg,
        ];
        throw new HttpResponseException(json($result));
    }


    /**
     * layui表格失败的响应
     * @param $msg string 错误消息
     * @param null $data 响应数据
     */
    protected function json_layui_error($msg = '执行失败', $data = null)
    {
        $result = [
            'code' => 1,
            'msg' => $msg,
            'data' => $data,
        ];
        throw new HttpResponseException(json($result));
    }

}
