<?php

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\admin\model\AdminMenu as menuModel;
use file\DirHelper;
use think\Db;
use think\Log;

class Menu extends Permissions
{
    public function index()
    {
        $model = new menuModel();
        $menus = $model->order('orders asc')->select();
        if ($this->request->isAjax()) {
            foreach ($menus as $k => $v) {
                $v['type_text'] = $v->type == 1 ? "权限节点" : "普通节点";
                $v['is_display_text'] = $v->is_display == 1 ? "显示在左侧菜单" : "操作节点";
                $menus[$k] = $v;
            }
            return $menus;
        } else {
            $menus_all = $model->treelist($menus);
            $this->assign('menus', $menus_all);
            $this->assign('noInsertRoutes', $this->noInsertRoutes());
            return $this->fetch();
        }
    }

    public function publish()
    {
        $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
        $model = new menuModel();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['name', 'require', '菜单名称不能为空'],
                ['pid', 'require', '请选择上级菜单'],
                // ['module', 'require', '请填写模块名称'],
                // ['controller', 'require', '请填写控制器名称'],
                // ['function', 'require', '请填写方法名称'],
                ['type', 'require', '请选择菜单类型'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        } else {
            $menus = $model->order('orders asc')->select();
            $menus_all = $model->treelist($menus);
            $this->assign('menus_all', $menus_all);
        }

        if ($id > 0) {
            if ($this->request->isPost()) {
                if ($id == $post['pid']) {
                    $this->error('不能选自己当上级分类');
                }
                $menu = $model->where('id', $id)->find();
                if (empty($menu)) {
                    $this->error('id不正确');
                }
                //如果关闭默认展开，给默认值0
                if (empty($post['is_open'])) {
                    $post['is_open'] = 0;
                }
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改菜单信息成功', 'admin/menu/index');
                }
            } else {
                $menu = $model->where('id', $id)->find();
                if (!empty($menu)) {
                    $this->assign('menu', $menu);
                    return $this->fetch();
                } else {
                    $this->error('id不正确');
                }
            }
        } else {
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加菜单失败');
                } else {
                    \app\admin\model\AdminCate::init_superadmin();
                    $this->success('添加菜单成功', 'admin/menu/index');
                }
            } else {
                $pid = $this->request->has('pid') ? $this->request->param('pid', null, 'intval') : null;
                if (!empty($pid)) {
                    $this->assign('pid', $pid);
                }
                return $this->fetch();
            }
        }
    }

    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
            if (Db::name('admin_menu')->where('pid', $id)->select() == null) {
                if (false == Db::name('admin_menu')->where('id', $id)->delete()) {
                    $this->error('删除失败');
                } else {
                    $this->success('删除成功', 'admin/menu/index');
                }
            } else {
                $this->error('该菜单下还有子菜单，不能删除');
            }
        }
    }

    //设置排序
    public function orders()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['orders'] as $id => $new_order) {
                $order = Db::name('admin_menu')->where('id', $id)->value('orders');
                if ($order != $new_order) {
                    if (false == Db::name('admin_menu')->where('id', $id)->update(['orders' => $new_order])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            if (empty($i)) {
                $this->error('没有更新排序', 'index', $post);
            }
            $this->success('成功更新' . $i . '个排序', 'index');
        }
    }

    /**
     * 未添加的路由
     * @return array
     */
    private function noInsertRoutes()
    {
        $allRoutes = $this->allRoutes();
        $allMenusLowCase = $this->allMenusLowCase();

        $noInsertRoutes = [];
        foreach ($allRoutes as $route) {
            if (!in_array(strtolower($route), $allMenusLowCase)) {
                $arr = explode('-', $route);
                $noInsertRoutes[] = [
                    'module' => $arr[0],
                    'controller' => $arr[1],
                    'function' => $arr[2],
                ];
            }
        }

        $default_name = [
            'delete' => '删除',
            'deletes' => '批量删除',
            'publish' => '新增/修改',
            'sort' => '排序',
            'switch_status' => '状态',
            'perview' => '预览',
            'updateTreePath' => '更新treepath',
            'rename' => '重命名',
            'upload' => '上传',
            'istop_time' => '推荐/取消',
            'create' => '创建',
        ];
        foreach ($noInsertRoutes as $k => $v) {
            //set default name
            $noInsertRoutes[$k]['name'] = $default_name[$v['function']]?? $v['function'];
            //set default pid
            $noInsertRoutes[$k]['pid'] = (new menuModel())->where(['module' => 'admin', 'controller' => $v['controller'], 'function' => 'index'])->value('id');
        }

        return $noInsertRoutes;
    }

    /**
     * 批量添加
     */
    public function batchAdd()
    {
        $post = $this->request->param();
        if (isset($post['pid']) && is_array($post['pid'])) {
            $data = [];
            foreach ($post['pid'] as $key => $pid) {
                if (!isset($post['name'][$key])) {
                    $post['name'][$key] = $post['function'][$key];
                }
                $data[] = [
                    'name' => $post['name'][$key],
                    'module' => $post['module'][$key],
                    'controller' => $post['controller'][$key],
                    'function' => $post['function'][$key],
                    'is_display' => $post['is_display'][$key],
                    'type' => $post['type'][$key],
                    'pid' => $pid,
                    'icon' => 'fa-tag',
                ];
            }
            $model = new menuModel();
            if (false == $model->allowField(true)->saveAll($data)) {
                $this->error('添加菜单失败');
            } else {
                \app\admin\model\AdminCate::init_superadmin();
                $this->success('添加菜单成功', 'admin/menu/index');
            }
        }
        $this->error('当前没有路由可添加');
    }


    /**
     * 获取所有路由，Permissions子类的方法
     * @param bool $tolower 是否转小写
     * @return array
     */
    private function allRoutes($tolower = false)
    {
        $allRoutes = [];
        $controllers = DirHelper::getFilenameList(APP_PATH . 'admin' . DS . 'controller' . DS, '*.php');
        foreach ($controllers as $controller) {
            if (is_subclass_of("\app\admin\controller\\$controller", self::BASE_CLASS)) {
                $actions = getActions("\app\admin\controller\\$controller");
                foreach ($actions as $action) {
                    $allRoutes[] = $tolower ? strtolower("admin-$controller-$action") : "admin-$controller-$action";//控制器必须保留驼峰
                }
            }
        }
        return $allRoutes;
    }


    /**
     * 获取所有菜单，转小写
     * @return array
     */
    private function allMenusLowCase()
    {
        $allMenus = (new menuModel())->field("concat(module,'-',controller,'-',function) as route")->where('controller', '<>', '')->select();
        $allMenusLowCase = [];
        foreach ($allMenus as $menu) {
            $allMenusLowCase[] = strtolower($menu['route']);
        }
        return $allMenusLowCase;
    }


    //清理不存在的菜单
    public function batchDel()
    {
        stop_online();

        $allRoutes = $this->allRoutes(true);
        $allMenus = $this->allMenusLowCase();

        $menuModel = new menuModel();
        $total = 0;
        foreach ($allMenus as $menu) {
            if (!in_array($menu, $allRoutes)) {
                //找到不存在路由的菜单
                $arr = explode('-', $menu);
                if ('common' == $arr[1]) {
                    continue;//排除登入
                }
                Log::record('delete menu: ' . $menu);

                $menu = $menuModel->where([
                    'module' => $arr[0],
                    'controller' => $arr[1],
                    'function' => $arr[2],
                ])->find();

                if (Db::name('admin_menu')->where('pid', $menu->id)->select() == null) {
                    if (false == Db::name('admin_menu')->where('id', $menu->id)->delete()) {
                        Log::record('删除失败');
                    } else {
                        Log::record('删除成功');
                        $total++;
                    }
                } else {
                    Log::record('该菜单下还有子菜单，不能删除');
                }
            }
        }

        $this->success('清理完成' . $total . '个记录');
    }
}
