<?php

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use think\Cookie;
use think\Db;
use think\Session;

class Index extends Permissions
{
    public function index()
    {
        //所有菜单
        $menus = Db::name('admin_menu')->where(['is_display' => 1])->order('orders asc')->select();

        //用户所有权限
        $permissions = $this->getPermission();

        //删除不在当前角色权限里的菜单，实现隐藏
        foreach ($menus as $k => $val) {
            if ($val['type'] == 1 and $val['is_display'] == 1 and !in_array($val['id'], $permissions)) {
                unset($menus[$k]);
            }
        }

        //给菜单添加url
        foreach ($menus as $key => $value) {
            if (empty($value['parameter'])) {
                $url = url($value['module'] . '/' . $value['controller'] . '/' . $value['function']);
            } else {
                $url = url($value['module'] . '/' . $value['controller'] . '/' . $value['function']) . "?" . $value['parameter'];
            }
            $menus[$key]['url'] = $url;
        }

        $this->assign('menus', $this->menulist($menus));

        return $this->fetch();
    }


    //树形顺序排序
    protected function menulist($menuAll)
    {
        $menus = array();
        //先找出顶级菜单
        foreach ($menuAll as $k => $val) {
            if ($val['pid'] == 0) {
                $menus[$k] = $val;
            }
        }
        //通过顶级菜单找到下属的子菜单
        foreach ($menus as $k => $val) {
            foreach ($menuAll as $key => $value) {
                if ($value['pid'] == $val['id']) {
                    $menus[$k]['list'][] = $value;
                }
            }
        }
        //三级菜单
        foreach ($menus as $k => $val) {
            if (isset($val['list'])) {
                foreach ($val['list'] as $ks => $vals) {
                    foreach ($menuAll as $key => $value) {
                        if ($value['pid'] == $vals['id']) {
                            $menus[$k]['list'][$ks]['list'][] = $value;
                        }
                    }
                }
            }
        }
        return $menus;
    }


    /**
     * 管理员退出，清除session
     * 有登入鉴权
     */
    public function logout()
    {
        Session::delete(self::ADMIN_ID);
        Session::delete(self::ADMIN_NAME);
        Session::delete(self::ADMIN_CATE_ID);
        Session::delete(self::ADMIN_THUMB);
        Cookie::delete(Permissions::ADMIN_ID);

        if (Session::has(self::ADMIN_ID) or Session::has(self::ADMIN_CATE_ID)) {
            $this->error('退出失败');
        } else {
            $this->success('正在退出...', 'admin/common/login');//最好设置一个安全入口
        }
    }
}
