<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */


namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\admin\model\AdminLog;
use app\admin\model\Urlconfig;
use app\common\model\Webconfig;
use app\common\service\Constant;
use think\Cache;
use think\Db;

class Main extends Permissions
{
    public function index()
    {
        //tp版本号
        $info['tp'] = THINK_VERSION;
        //php版本
        $info['php'] = PHP_VERSION;
        //操作系统
        $info['win'] = PHP_OS;
        //最大上传限制
        $info['upload_max_filesize'] = ini_get('upload_max_filesize');//不能使用ini_set修改
        $info['post_max_size'] = ini_get('post_max_size');//不能使用ini_set修改
        $info['memory_limit'] = ini_get('memory_limit');
        $info['tplay_filesize'] = formatBytes(Webconfig::getValue('file_size') * 1024);
        //脚本执行时间限制
        $info['max_execution_time'] = ini_get('max_execution_time') . 'S';
        //运行环境
        $info['environment'] = $_SERVER['SERVER_SOFTWARE'];
        try {
            //剩余空间大小，服务器没有权限会报错
            $info['disk'] = formatBytes(disk_free_space("/"));
        } catch (\Exception $e) {
        }

        $this->assign('info', $info);

        //==============网站数据=============================

        //会员
        $web['user_num'] = Db::name('user')->where('status', Constant::STATUS_PASS)->count();
        $web['user_num_wait'] = Db::name('user')->where('status', Constant::STATUS_WAIT)->count();
        //文章
        $web['article_num'] = Db::name('article')->count();
        $web['status_article'] = Db::name('article')->where('status', 0)->count();
        //附件
        $web['file_num'] = Db::name('attachment')->count();
        $web['status_file'] = Db::name('attachment')->where('status', 0)->count();
        //消息
        $web['message_num'] = Db::name('messages')->count();
        $web['status_message'] = Db::name('messages')->where('status', 0)->count();

        //==============每日管理员操作次数===========================

        $today = date('Y-m-d');
        //取最近十天
        $date = [];
        $date_string = '';
        for ($i = 9; $i > 0; $i--) {
            $date[] = date("Y-m-d", strtotime("-{$i} day"));
            $date_string .= date("Y-m-d", strtotime("-{$i} day")) . ',';
        }
        $date[] = $today;
        $date_string .= $today;
        $web['date_string'] = $date_string;

        $data_string = '';
        foreach ($date as $k => $val) {
            $min_time = strtotime($val);
            $max_time = $min_time + 60 * 60 * 24;
            $where['create_time'] = [['>=', $min_time], ['<=', $max_time]];
            if ($val == $today) {
                $data_string .= Db::name('admin_log')->where('params', '<>', '')->where($where)->count() . ',';
            } else {
                $data_string .= Db::name('admin_log')->cache()->where('params', '<>', '')->where($where)->count() . ',';
            }
        }
        $web['data_string'] = $data_string;
        $this->assign('web', $web);

        //==============风险提示=============================
        $safe = true;

        if (file_exists(APP_PATH . 'install')) {
            $safe = false;
            //提示删除install目录
            $this->assign('delete_install', true);
        }

        //提示改密码
        $weekpass = Db::name('admin')->where('id', session(self::ADMIN_ID))->where('password', password('123456'))->count();
        if ($weekpass > 0) {
            $safe = false;
            $this->assign('weekpass', true);
            if (!in_array($this->request->ip(), ['0.0.0.0', '127.0.0.1'])) {
                $this->assign('waring', ['msg' => "当前密码过于简单，是否立即修改？", "url" => url('admin/admin/editpassword')]);
            }
        }

        //提示设置安全入口
        if ((new Urlconfig())->isWeekBackend()) {
            $safe = false;
            $this->assign('week_backend', true);
        }

        if (session(self::ADMIN_CATE_ID) == \app\admin\model\Admin::SUPER_ADMIN_ID) {
            //登录请求过多，防止被爆破，提示管理员修改后台地址
            if ((new AdminLog())->where(['admin_menu_id' => '50', 'params' => ['<>', '']])->whereTime('create_time', 'today')->count() > 100) {
                $this->assign('waring', ['msg' => "今日发现多次登录请求，是否立即修改后台地址？", "url" => url('admin/urlsconfig/index')]);
            }
            //提示有错误
            if (file_exists(LOG_PATH . date('Ym') . DS . date('d') . '_error.log')) {
                $this->assign('waring', ['msg' => "发现错误，请立即查看日志", "url" => '#']);
            }
        }

        $this->assign('safe', $safe);
        return $this->fetch();
    }

    /**
     * 清除全部缓存
     */
    public function clear()
    {
        if (false == Cache::clear()) {
            $this->error('清除缓存失败');
        } else {
            $this->success('清除缓存成功');
        }
    }
}
