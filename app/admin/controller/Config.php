<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\common\model\Config as configModel;
use app\common\model\ConfigTab as tabModel;
use app\common\service\Constant;
use think\Db;

class Config extends Permissions
{
    //扩展配置
    public function index()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $where = [];
            if (isset($post['id']) and $post['id'] > 0) {
                $where['id'] = $post['id'];
            }
            if (isset($post['tab_id']) && !empty($post['tab_id'])) {
                $where['tab_id'] = $post['tab_id'];
            }
            if (isset($post['keywords']) and !empty($post['keywords'])) {
                $where['name'] = ['like', '%' . $post['keywords'] . '%'];
            }
            if (isset($post['status']) and ($post['status'] == 1 or $post['status'] === '0')) {
                $where['status'] = $post['status'];
            }
            $model = new configModel();
            $count = $model->where($where)->count();
            $data = $model->where($where)->page($post['page']??0, $post['limit']??0)->order('tab_id desc,sort desc,id desc')->select();
            foreach ($data as $k => $v) {
                $data[$k]['tab_text'] = $v->tab_text;
                $data[$k]['type_text'] = $v->type_text;
            }
            return array('code' => 0, 'count' => $count, 'data' => $data);
        } else {
            $tabs = (new tabModel())->allTabs();
            $this->assign('tabs', $tabs);
            return $this->fetch();
        }
    }

    //显示到系统设置
    public function indexWebconfig()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $where = [
                'status' => Constant::STATUS_OPEN
            ];
            if (isset($post['tab_id']) && !empty($post['tab_id'])) {
                $where['tab_id'] = $post['tab_id'];
            }
            if (isset($post['keywords']) and !empty($post['keywords'])) {
                $where['name'] = ['like', '%' . $post['keywords'] . '%'];
            }
            $model = new configModel();
            $count = $model->where($where)->count();
            $data = $model->where($where)->page($post['page']??0, $post['limit']??0)->order('sort desc,id desc')->select();
            foreach ($data as $k => $v) {
                $data[$k]['value_text'] = $v->value_text;
            }
            return array('code' => 0, 'count' => $count, 'data' => $data);
        } else {
            $tabs = (new tabModel())->showTabs();
            $this->assign('tabs', $tabs);
            return $this->fetch();
        }
    }

    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new configModel();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['name', 'require', '名称不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        } else {
            $grouptabs = (new tabModel())->order('sort desc,id desc')->select();
            $this->assign('tabs', $grouptabs);
            $this->assign('types', $model::TYPES);
        }

        if ($id > 0) {
            $cate = $model->where('id', $id)->find();
            if (empty($cate)) {
                $this->error('id不正确');
            }
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功', 'index');
                }
            } else {
                $this->assign('cate', $cate);
                $this->assign('type', $cate->type);
                return $this->fetch();
            }
        } else {
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {
                    $this->success('添加成功', 'index');
                }
            } else {
                $this->assign('type', 0);
                return $this->fetch();
            }
        }
    }


    public function publishWebconfig()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new configModel();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['name', 'require', '名称不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        } else {
        }

        if ($id > 0) {
            $cate = $model->where('id', $id)->find();
            if (empty($cate)) {
                $this->error('id不正确');
            }
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功');
                }
            } else {
                $this->assign('cate', $cate);
                return $this->fetch();
            }
        } else {
        }
    }


    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            if (Db::name('config_option')->where('pid', $id)->select() == null) {
                if (false == Db::name('config')->where('id', $id)->delete()) {
                    $this->error('删除失败');
                } else {
                    $this->success('删除成功', 'index');
                }
            } else {
                $this->error('请先删除子配置');
            }
        }
    }

    public function sort()
    {
        if ($this->request->isPost() && $this->request->has('ids')) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['ids'] as $k => $id) {
                $sort = Db::name('config')->where('id', $id)->value('sort');
                $newsort = $post['sorts'][$k]??$sort;
                if ($sort != $newsort) {
                    if (false == Db::name('config')->where('id', $id)->update(['sort' => $newsort])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            $this->success('成功更新' . $i . '个数据', 'index');
        } else {
            $this->error('无数据更新', 'index');
        }
    }

    public function status()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (false == Db::name('config')->where('id', $post['id'])->update(['status' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }

}