<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\common\model\ConfigOption as optionModel;
use app\common\model\Config as configModel;
use think\Db;
use think\Log;

class ConfigOption extends Permissions
{
    /**
     * 扩展配置
     * 目前只有基础的几个字段，可以配置，对于复杂的需求，就不能用了
     * 复杂配置，直接用代码生成，做一个独立的配置页面会更好管理
     * @return array|mixed
     */
    public function index()
    {
        $pid = $this->request->param('pid', 0, 'intval');
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $where = [
                'pid' => $pid,
            ];
            if (isset($post['id']) and $post['id'] > 0) {
                $where['id'] = $post['id'];
            }
            if (isset($post['keywords']) and !empty($post['keywords'])) {
                $where['name'] = ['like', '%' . $post['keywords'] . '%'];
            }
            if (isset($post['value']) and !empty($post['value'])) {
                $where['value'] = ['like', '%' . $post['value'] . '%'];
            }
            if (isset($post['description']) and !empty($post['description'])) {
                $where['description'] = ['like', '%' . $post['description'] . '%'];
            }
            if (isset($post['status']) and ($post['status'] == 1 or $post['status'] === '0')) {
                $where['status'] = $post['status'];
            }
            $model = new optionModel();
            $count = $model->where($where)->count();
            $data = $model->where($where)->page($post['page']??0, $post['limit']??15)->order('sort desc,id desc')->select();

            foreach ($data as $k => $v) {
                $v['cate_name'] = $v->config['name'];
                $v['thumb_url'] = getAttachmentUrl($v['image']);
                $v['group_type'] = $v->config['type'];
                $data[$k] = $v;
            }

            return array('code' => 0, 'count' => $count, 'data' => $data);
        } else {
            $this->assign('config', configModel::get($pid));
            return $this->fetch();
        }
    }

    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $pid = $this->request->param('pid', 0, 'intval');
        $this->assign('pid', $pid);
        $model = new optionModel();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['name', 'require', '标题不能为空'],
                ['pid', 'require', '请选择分类'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        }
        if ($id > 0) {
            //是修改操作
            $link = $model->where('id', $id)->find();
            if (empty($link)) {
                $this->error('id不正确');
            }
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功', 'index');
                }
            } else {
                $this->assign('link', $link);
                $config = configModel::get($pid);
                $this->assign('config', $config);
                if ($config->pid_open) {
                    $pnodes = (new configModel())->where(['tab_id' => $link->config->tab_id, 'type' => ['in', [configModel::TYPE_SELECT, configModel::TYPE_RADIO]]])->column('name', 'id');
                    $this->assign('pnodes', $pnodes);
                }
                return $this->fetch();
            }
        } else {
            //是新增操作
            if ($this->request->isPost()) {

                if ($model->where('pid', $pid)->count() == 0) {
                    $config = configModel::get($pid);
                    if (!$config) {
                        $this->error('pid异常');
                    }
                    if ($config->type == configModel::TYPE_RADIO) {
                        $post['single_status'] = 1;
                    }
                }

                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {
                    $this->success('添加成功', 'index');
                }
            } else {
                $this->assign('config', configModel::get($pid));
                return $this->fetch();
            }
        }
    }

    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            if (false == Db::name('config_option')->where('id', $id)->delete()) {
                $this->error('删除失败');
            } else {
                $this->success('删除成功', 'index');
            }
        }
    }

    public function deletes()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $ids = $post['ids'];
            $model = new optionModel();
            if ($model->where('id', 'in', $ids)->delete()) {
                Log::log("批量删除：" . implode(',', $ids));
                $this->success('删除成功');
            }
        }
    }

    //排序
    public function sort()
    {
        if ($this->request->isPost() && $this->request->has('ids')) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['ids'] as $k => $id) {
                $sort = Db::name('config_option')->where('id', $id)->value('sort');
                $newsort = $post['sorts'][$k]??$sort;
                if ($sort != $newsort) {
                    if (false == Db::name('config_option')->where('id', $id)->update(['sort' => $newsort])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            $this->success('成功更新' . $i . '个数据', 'index');
        } else {
            $this->error('无数据更新', 'index');
        }
    }

    public function status()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (false == Db::name('config_option')->where('id', $post['id'])->update(['status' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }

    //单选状态
    public function single_status()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            Db::name('config_option')->where('pid', $post['pid'])->update(['single_status' => 0]);
            if (false == Db::name('config_option')->where('id', $post['id'])->update(['single_status' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }
}