<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2024/6/17
 * Time: 15:23
 * 1464674022@qq.com
 */

namespace app\admin\controller;


use app\admin\controller\base\Permissions;
use file\DirHelper;
use string\Format;
use think\Log;

class Doc extends Permissions
{
    const ROOTPATH = APP_PATH . 'api' . DS . 'controller' . DS . 'doc' . DS;


    //文档列表
    public function index()
    {
        $data = [];
        $this->menulist(self::ROOTPATH, $data);

        $this->assign('data', json_encode($data));
        return $this->fetch();
    }


    //递归
    private function menulist($rootpath, &$arr)
    {
        $names = DirHelper::getFilenameList($rootpath, '*');
        if (empty($names)) { //空列表
            echo '发现空目录 ' . $rootpath . ' <a href="/admin/doc/del?path=' . str_replace(self::ROOTPATH, '', Format::strEncode($rootpath)) . '" target="_blank" style="color: blue;">删除</a><br>';
        }

        foreach ($names as $k => $name) {
            $filePath = appendEndDS($rootpath) . $name;

            if (file_exists($filePath)) {
                $filetype = filetype($filePath);
            } else {
                //处理中文编码，windows文件上传到linux的时候，才需要这样处理
                $name2 = Format::strEncode($name, "GBK");
                $filePath = appendEndDS($rootpath) . $name2;
                if (file_exists($filePath)) {
                    $filetype = filetype($filePath);
                } else {
                    $filetype = '';
                }
            }

            if ('dir' == $filetype) {
                $data = [];
                $this->menulist($filePath, $data);
                $arr[] = [
                    'title' => $name,
                    "id" => $k,
                    "children" => $data,
//                    "spread" => true,//展开
                ];
            } else {
                $arr[] = [
                    'title' => $name,
                    "id" => $k,
                    "href" => "/admin/doc/doc?path=" . str_replace(self::ROOTPATH, '', Format::strEncode($filePath))
                ];
            }
        }
    }


    //文档详情
    public function doc()
    {
        $path = $this->request->param('path', '', 'replaceDS');
        $fullpath = self::ROOTPATH . $path;
        if (!file_exists($fullpath)) {
            //处理中文编码，windows文件上传到linux的时候，才需要这样处理
            $path2 = Format::strEncode($path, "GBK");
            $fullpath = self::ROOTPATH . $path2;
        }

        if (file_exists($fullpath)) {
            $text = file_get_contents($fullpath);
            $text = str_replace('`', '\`', $text);
            $this->assign('title', $path);
            $this->assign('text', $text);
            return $this->fetch();
        } else {
            echo '404';
        }
    }


    public function del()
    {
        $path = $this->request->param('path', '', 'replaceDS');
        $fullpath = self::ROOTPATH . $path;
        if (!file_exists($fullpath)) {
            //处理中文编码，windows文件上传到linux的时候，才需要这样处理
            $path2 = Format::strEncode($path, "GBK");
            $fullpath = self::ROOTPATH . $path2;
        }
        if (file_exists($fullpath)) {
            $names = DirHelper::getFilenameList($fullpath, '*');
            if (empty($names)) { //空列表
                //删除空目录
                rmdir($fullpath);
                Log::record('删除空目录 ' . $fullpath);
                return "删除成功";
            }
        }
        return '404';
    }


    public function test()
    {
        return $this->fetch();
    }
}