<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */


namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\admin\model\AdminLog;
use app\admin\model\Urlconfig;
use app\common\model\ConfigTab as tabModel;
use think\Cache;

class Webconfig extends Permissions
{

    public function index()
    {
        //扩展配置标签
        $tabs = (new tabModel())->showTabs();
        $this->assign('tabs', $tabs);
        //优先展示扩展配置
        if (!$this->request->has('self') && $tabs) {
            $this->redirect(url('admin/config/indexWebconfig') . '?tab_id=' . $tabs[0]->id);
        }

        $this->assign('web_config', \app\common\model\Webconfig::get(1));
        $this->assign('is_open_user_module', \think\Env::get('is_open_user_module', false));
        $this->assign('is_close_site', \think\Env::get('is_close_site', false));
        $this->assign('is_app_debug', \think\Env::get('app_debug', false));
        $this->assign('is_open_log', \think\Env::get('is_open_log', true));
        $this->assign('close_site_key', (new Urlconfig())->getCloseSiteKey());
        $this->assign('admin_log_num', (new AdminLog())->count());
        $this->assign('backend_pass', (new Urlconfig())->getBackendPass());
        $this->assign('editors', \app\common\model\Webconfig::EDITORS);
        return $this->fetch();
    }

    /**
     * 设置开关配置
     * @param string $name
     */
    private function switch_fun($name = 'is_close_site')
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $res = \app\common\model\Webconfig::setEnvValue($name, $post['status']);
            if ($res) {
                $this->success('设置成功');
            } else {
                $this->error('设置失败');
            }
        }
    }

    public function switch_site_close()
    {
        $this->switch_fun('is_close_site');
    }

    public function switch_open_user_module()
    {
        $this->switch_fun('is_open_user_module');
    }

    public function switch_app_debug()
    {
        $this->switch_fun('app_debug');
    }

    public function switch_open_log()
    {
        $this->switch_fun('is_open_log');
    }

    public function publish()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $validate = new \think\Validate([
                ['file_type', 'requireWith:name', '上传类型不能为空'],
                ['file_size', 'requireWith:name', '上传大小不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
                }
            $model = (new \app\common\model\Webconfig())->where('id', 1)->find();
            if (false == $model->allowField(true)->save($post)) {
                $this->error('提交失败');
            } else {
                Cache::clear();
                $this->success('提交成功', 'admin/webconfig/index');
            }
        }
    }
}
