<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\common\model\Catalog as cataLogModel;
use app\common\model\Templet as templetModel;
use app\common\service\CmsService;
use app\common\service\Constant;
use think\Db;
use think\Exception;

class Catalog extends Permissions
{

    public function index()
    {
        if ($this->request->isAjax()) {
            $model = new cataLogModel();
            $data = $model->order('pid,sort desc,id asc')->select();
            foreach ($data as $k => $v) {
                /**@var cataLogModel $v */
                $v['type_text'] = $v->type_text;
                $v['article_count'] = $v->article_count ?: '';
                $v['tpath'] = $v->tpath();
                $data[$k] = $v;
            }
            return array('code' => 0, 'count' => count($data), 'data' => $data, "tip" => "操作成功！");
        } else {
            return $this->fetch();
        }
    }


    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $post = $this->request->post();
        $model = new cataLogModel();

        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['title|栏目名称', 'require'],
                ['catalog_templet|栏目模板', 'requireIf:type,0|requireIf:type,1'],
                ['article_templet|文章模板', 'requireIf:type,1'],
                ['path|栏目路径', 'requireIf:type,0|requireIf:type,1'],
                ['article_rule|文章路径', 'requireIf:type,1'],
                ['articlelist_rule|列表页路径', 'requireIf:type,1'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
            $post['path'] = trim($post['path']);
            $post['path'] = empty($post['path']) ? "" : appendStartDS(trim($post['path']), '/');
            $post['catalog_templet'] = trim($post['catalog_templet']);
            $post['article_templet'] = trim($post['article_templet']);
            $post['article_rule'] = trim($post['article_rule']);
            $post['articlelist_rule'] = trim($post['articlelist_rule']);
            $post['articlelist_rows'] = empty($post['articlelist_rows']) ? 10 : $post['articlelist_rows'];
        } else {
            $pid = $this->request->param('pid', 0, 'intval');
            $this->assign('pid', $pid);
            $this->assign("templets", templetModel::getTemplets());
            $this->assign('cates', $model->treelistForCatalog());
            $this->assign('types', cataLogModel::TYPES);
        }

        if ($id > 0) {
            //修改
            /** @var cataLogModel $item */
            $item = $model->where('id', $id)->find();
            if (empty($item)) {
                $this->error('id不存在');
            }
            if ($this->request->isPost()) {
                //更新tree_path
                try {
                    $item->updateTreePath($post['pid']);
                } catch (Exception $e) {
                    $this->error($e->getMessage());
                }
                //保存更新
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功', 'index');
                }
            } else {
                $this->assign('item', $item);
                return $this->fetch();
            }
        } else {
            //新增
            if ($this->request->isPost()) {
                $post['status'] = Constant::STATUS_OPEN;
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {
                    $cate = $model;
                    //更新tree_path
                    try {
                        $cate->updateTreePath($post['pid']);
                    } catch (Exception $e) {
                        $this->error($e->getMessage());
                    }
                    $this->success('添加成功', 'index', ['insertId' => intval($cate->id)]);
                }
            } else {
                return $this->fetch();
            }
        }
    }


    /**
     * 删除
     */
    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            $exits = cataLogModel::get(['pid' => $id]);
            if ($exits) {
                $this->error('请先删除子节点');
            }
            if (false == Db::name('catalog')->where('id', $id)->delete()) {
                $this->error('删除失败');
            } else {
                $this->success('删除成功', 'index');
            }
        }
    }

    /**
     * 审核
     */
    public function status()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (false == Db::name('catalog')->where('id', $post['id'])->update(['status' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }

    /**
     * 排序
     */
    public function sort()
    {
        if ($this->request->isPost() && $this->request->has('ids')) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['ids'] as $k => $id) {
                $sort = Db::name('catalog')->where('id', $id)->value('sort');
                $newsort = $post['sorts'][$k]??$sort;
                if ($sort != $newsort) {
                    if (false == Db::name('catalog')->where('id', $id)->update(['sort' => $newsort])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            if (empty($i)) {
                $this->error('没有更新排序', 'index');
            }
            $this->success('成功更新' . $i . '个排序', 'index');
        } else {
            $this->error('无数据更新', 'index');
        }
    }

    /**
     * 更新所有节点tree_path
     * admin/catalog/updateTreePath
     */
    public function updateTreePath()
    {
        (new cataLogModel())->updateTreePathAll();
        $this->success();
    }


    /**
     * 预览
     * @return mixed|string
     */
    public function perview()
    {
        tempClear();//预览模式和生成模式切换时会有缓存

        $id = $this->request->param('id', 0, 'intval');
        if ($id) {
            /** @var cataLogModel $catalog */
            $catalog = (new cataLogModel())->where('id', $id)->find();
            return (new CmsService())->perviewCatalog($catalog);
        }
        $this->error('预览失败:id is null');
    }


    /**
     * 生成静态文件
     */
    public function exportHtml()
    {
        if (!$this->request->isAjax() && !$this->request->isCli()) {
            return;
        }

        if ($this->request->isCli()) {
            $ids = (new cataLogModel())->column('id');
        } else {
            $post = $this->request->param();
            if (!isset($post['ids']) || empty($post['ids'])) {
                $this->error('请选择栏目');
            }
            $ids = $post['ids'];
        }

        return (new CmsService())->exportHtml($ids);
    }
}