<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2023/02/05
 * Time: 20:33
 */


namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use think\Db;


class AdminCate extends Permissions
{
    /**
     * 权限组
     * @return mixed
     */
    public function index()
    {
        $model = new \app\admin\model\AdminCate;
        $post = $this->request->param();
        $where = [];
        if (isset($post['keywords']) and !empty($post['keywords'])) {
            $where['name'] = ['like', '%' . $post['keywords'] . '%'];
        }
        if (isset($post['create_time']) and !empty($post['create_time'])) {
            $min_time = strtotime($post['create_time']);
            $max_time = $min_time + 24 * 60 * 60;
            $where['create_time'] = [['>=', $min_time], ['<=', $max_time]];
        }
        $cate = $model->where($where)->order('create_time desc')->paginate(15, false, ['query' => $this->request->param()]);
        $this->assign('cate', $cate);
        return $this->fetch();
    }


    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new \app\admin\model\AdminCate();
        if ($id > 0) {
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['name', 'require', '角色名称不能为空'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }
                $name = $model->where(['name' => $post['name'], 'id' => ['neq', $post['id']]])->select();
                if (!empty($name)) {
                    $this->error('提交失败：该角色名已存在');
                }
                //处理选中的权限菜单id，转为字符串
                if (!empty($post['admin_menu_id'])) {
                    $post['permissions'] = implode(',', $post['admin_menu_id']);
                } else {
                    $post['permissions'] = '0';
                }
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改角色信息成功', 'index');
                }
            } else {
                $info['cate'] = $model->where('id', $id)->find();
                if (!empty($info['cate']['permissions'])) {
                    //将菜单id字符串拆分成数组
                    $info['cate']['permissions'] = explode(',', $info['cate']['permissions']);
                }
                $menus = Db::name('admin_menu')->where('type', 1)->order('orders asc')->select();
                $info['menu'] = $this->menulist($menus);
                $this->assign('info', $info);
                return $this->fetch();
            }
        } else {
            if ($this->request->isPost()) {
                $post = $this->request->post();
                $validate = new \think\Validate([
                    ['name', 'require', '角色名称不能为空'],
                ]);
                if (!$validate->check($post)) {
                    $this->error('提交失败：' . $validate->getError());
                }
                $name = $model->where('name', $post['name'])->find();
                if (!empty($name)) {
                    $this->error('提交失败：该角色名已存在');
                }
                //处理选中的权限菜单id，转为字符串
                if (!empty($post['admin_menu_id'])) {
                    $post['permissions'] = implode(',', $post['admin_menu_id']);
                } else {
                    $post['permissions'] = '0';
                }
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加角色失败');
                } else {
                    $this->success('添加角色成功', 'index');
                }
            } else {
                $menus = Db::name('admin_menu')->where('type', 1)->order('orders asc')->select();
                $info['menu'] = $this->menulist($menus);
                $this->assign('info', $info);
                return $this->fetch();
            }
        }
    }

    public function preview()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new \app\admin\model\AdminCate();
        $info['cate'] = $model->where('id', $id)->find();
        if (!empty($info['cate']['permissions'])) {
            //将菜单id字符串拆分成数组
            $info['cate']['permissions'] = explode(',', $info['cate']['permissions']);
        }
        $menus = Db::name('admin_menu')->where('type', 1)->order('orders asc')->select();
        $info['menu'] = $this->menulist($menus);
        $this->assign('info', $info);
        return $this->fetch();
    }

    protected function menulist($menu, $id = 0, $level = 0)
    {
        static $menus = array();
        $size = count($menus) - 1;
        foreach ($menu as $value) {
            if ($value['pid'] == $id) {
                $value['level'] = $level + 1;
                $value['str'] = $level == 0 ? "" : str_repeat('&emsp;&emsp;', $level) . '└ ';
                if ($level == 0) {
                    $menus[] = $value;
                } else {
                    $menus[$size]['list'][] = $value;
                }
                $this->menulist($menu, $value['id'], $value['level']);
            }
        }
        return $menus;
    }

    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
            if ($id > 0) {
                if ($id == 1) {
                    $this->error('超级管理员角色不能删除');
                }
                if (false == Db::name('admin_cate')->where('id', $id)->delete()) {
                    $this->error('删除失败');
                } else {
                    $this->success('删除成功', 'index');
                }
            } else {
                $this->error('id不正确');
            }
        }
    }

}
