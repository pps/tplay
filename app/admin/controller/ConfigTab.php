<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\admin\controller;

use app\admin\controller\base\Permissions;
use app\common\model\ConfigTab as tabModel;
use think\Db;

class ConfigTab extends Permissions
{
    public function index()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->param();
            $model = new tabModel();
            $data = $model->page($post['page']??0, $post['limit']??15)->order('sort desc,id desc')->select();
            return array('code' => 0, 'count' => count($data), 'data' => $data);
        } else {
            return $this->fetch();
        }
    }

    public function publish()
    {
        $id = $this->request->param('id', 0, 'intval');
        $model = new tabModel();
        $post = $this->request->post();
        if ($this->request->isPost()) {
            $validate = new \think\Validate([
                ['name', 'require', '分类名称不能为空'],
            ]);
            if (!$validate->check($post)) {
                $this->error('提交失败：' . $validate->getError());
            }
        }

        if ($id > 0) {
            $cate = $model->where('id', $id)->find();
            if (empty($cate)) {
                $this->error('id不正确');
            }
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post, ['id' => $id])) {
                    $this->error('修改失败');
                } else {
                    $this->success('修改成功', 'index');
                }
            } else {
                $this->assign('cate', $cate);
                return $this->fetch();
            }
        } else {
            if ($this->request->isPost()) {
                if (false == $model->allowField(true)->save($post)) {
                    $this->error('添加失败');
                } else {
                    $this->success('添加成功', 'index');
                }
            } else {
                return $this->fetch();
            }
        }
    }

    public function delete()
    {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id', 0, 'intval');
            if (Db::name('config')->where('tab_id', $id)->select() == null) {
                if (false == Db::name('config_tab')->where('id', $id)->delete()) {
                    $this->error('删除失败');
                } else {
                    $this->success('删除成功', 'index');
                }
            } else {
                $this->error('该标签已经绑定了链接组，请先删除关联');
            }
        }
    }

    public function sort()
    {
        if ($this->request->isPost() && $this->request->has('ids')) {
            $post = $this->request->post();
            $i = 0;
            foreach ($post['ids'] as $k => $id) {
                $sort = Db::name('config_tab')->where('id', $id)->value('sort');
                $newsort = $post['sorts'][$k]??$sort;
                if ($sort != $newsort) {
                    if (false == Db::name('config_tab')->where('id', $id)->update(['sort' => $newsort])) {
                        $this->error('更新失败');
                    } else {
                        $i++;
                    }
                }
            }
            $this->success('成功更新' . $i . '个数据', 'index');
        } else {
            $this->error('无数据更新', 'index');
        }
    }

    public function status()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (false == Db::name('config_tab')->where('id', $post['id'])->update(['status' => $post['status']])) {
                $this->error('设置失败');
            } else {
                $this->success('设置成功', 'index');
            }
        }
    }
}