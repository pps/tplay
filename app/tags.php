<?php

// 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init' => [
//        'app\common\behavior\TestBehavior',
    ],
    // 应用开始
    'app_begin' => [
//        'app\common\behavior\TestBehavior',
    ],
    // 模块初始化
    'module_init' => [
//        'app\common\behavior\TestBehavior',
    ],
    // 操作开始执行
    'action_begin' => [
//        'app\common\behavior\TestBehavior',
        'app\common\behavior\RecordRequestTime'
    ],
    // 视图内容过滤
    'view_filter' => [
//        'app\common\behavior\TestBehavior',
    ],
    // 应用结束
    'app_end' => [
//        'app\common\behavior\TestBehavior',
    ],
    // 日志写入
    'log_write' => [
//        'app\common\behavior\TestBehavior',
    ],
    'log_write_done' => [
//        'app\common\behavior\TestBehavior',
    ],
    'response_send' => [
//        'app\common\behavior\TestBehavior',
    ],
    'response_end' => [
//        'app\common\behavior\TestBehavior',
        'app\common\behavior\RecordRequestTime'
    ],
];
