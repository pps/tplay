<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\api\controller\base;

use app\common\model\User;
use app\common\model\Webconfig;
use app\common\service\LoginService;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use think\Db;
use think\exception\HttpResponseException;

/**
 * 登入鉴权的接口父类
 * Class Permissions
 * @package app\api\controller\base
 */
class Permissions extends Api
{

    private $userId;
    private $user;

    /**
     * 验证登入令牌
     */
    protected function checkLogin()
    {
        $jwt = $this->request->param(LoginService::ACCESS_TOKEN, "");
        if (!$jwt) {
            $jwt = $this->request->header(LoginService::ACCESS_TOKEN, "");
            if (!$jwt) {
                $jwt = cookie(LoginService::ACCESS_TOKEN);
                if (!$jwt) {
                    $callbackBody = file_get_contents('php://input');
                    if (!$callbackBody) {
                        $this->json_error('请先登入', null, self::ERR_CODE_401);
                    }
                    $callbackJson = json_decode($callbackBody, true);
                    $jwt = isset($callbackJson[LoginService::ACCESS_TOKEN]) ? $callbackJson[LoginService::ACCESS_TOKEN] : '';
                    if (!$jwt) {
                        $this->json_error('请先登入', null, self::ERR_CODE_401);
                    }
                }
            }
        }

        //check jwt
        try {
            JWT::$leeway = 60;
            $jwt_key = getSalt();
            $decoded = JWT::decode($jwt, new Key($jwt_key, 'HS256'));

            $arr = (array)$decoded;
            if (!isset($arr['exp']) || $arr['exp'] < time()) {

                throw new \think\Exception('Expired token');

            } else {

                if (LoginService::ACCESS_TOKEN != $arr['data']->type) {
                    throw new \think\Exception('Wrong number of segments');
                }

                //解析成功
                $this->userId = $arr['data']->userId;
                $loginTime = $arr['data']->loginTime;
                $exptime = $arr['data']->exptime;
                $expire = $arr['data']->expire??3600;

                //限制旧令牌登入
//                if ($this->getUser()->getData('login_time') > $loginTime) {
//                    $this->clear_session();
//                    throw new \think\Exception('您的账号已经在其它地方登入，请重新登入');
//                }

                //需要设置session登入状态，否则只有jwt鉴权的接口能访问
                //网页端自动刷新访问令牌，要保持和登入时jwt一样的过期时长，否则登入时如果设置jwt时长24小时会自动变成2小时了
                return LoginService::getAccessToken($this->userId, $loginTime, $expire);
            }
        } catch (Exception $e) {
//            if ($e instanceof HttpResponseException) {
//                $this->json_error($e->getResponse()->getData()['msg']);
//            }
            switch ($e->getMessage()) {
                case "Expired token":
                    LoginService::clear_session();
                    $this->json_error('Token过期,请重新登录', null, self::ERR_CODE_401);
                    break;
                case "Wrong number of segments":
                    LoginService::clear_session();
                    $this->json_error('Token验证失败,请重新登录', null, self::ERR_CODE_401);
                    break;
                default:
                    $this->json_error($e->getMessage());
            }
        }
    }


    /**
     * 判断用户是否登入
     * @return mixed
     */
    protected function isLogin()
    {
        return session('?' . LoginService::LOGIN_INFO . '.userId');
    }


    /**
     * 获取user对象
     * @return null|User
     */
    protected function getUser()
    {
        if (!$this->user) {
            $user = User::get($this->getUserId());
            if (!$user) {
                $this->json_error('找不到用户信息,请先注册', null, self::ERR_CODE_401);
            }
            $this->user = $user;
        }
        return $this->user;
    }

    /**
     * 获取user_id
     * @return null
     */
    protected function getUserId()
    {
        if (!$this->userId) {
            $this->json_error('请先登入', null, self::ERR_CODE_401);
        }
        return $this->userId;
    }


    /**
     * ip鉴权
     * 登入后台，或ip加入白名单，才能验证通过
     */
    protected function checkIP()
    {
        $withe_ip = Webconfig::getValue('white_ip');

        //转化成数组
        $withe_ip = explode(',', $withe_ip);
        //
        $admin_ips = Db::name('admin')->column('login_ip');
        $withe_ip = array_merge($withe_ip, $admin_ips);

        //获取当前访问的ip
        $ip = $this->request->ip();
        if (!in_array($ip, $withe_ip)) {
            $this->json_error('非法ip', $ip);
        }
    }

}
