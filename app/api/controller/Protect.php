<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\api\controller;


use app\api\controller\base\Permissions;
use app\common\model\User;
use app\common\service\LoginService;
use think\Session;

/**
 * Class Protect
 * 测试登入令牌
 * @package app\api\controller
 */
class Protect extends Permissions
{
    //前置操作
    protected $beforeActionList = [
        'checkIP' => '',//检查ip
        'checkLogin' => ['except' => 'index,testAccessToken,logout'],//不执行登入检查的方法
    ];


    public function index()
    {
        echo "这是一个公开的接口，" . ($this->isLogin() ? "当前用户ID:" . Session::get(LoginService::LOGIN_INFO . '.userId') : "当前未登入");
        echo "<br/><a href='/api/protect/home'>访问私有接口</a>";
        echo "<br/><a href='/api/protect/testAccessToken'>登入并访问私有接口</a>";
    }


    //测试访问令牌，登入并访问私有接口
    public function testAccessToken()
    {
        //省略账号密码验证
        $user = (new User())->where('id', 1)->find();//登入用户1
        if (!$user) {
            $this->error('请先注册一个用户账号');
        }
        //更新登录时间
        $user->save(['login_time' => time()]);

        $token_data = LoginService::getAccessToken($user->id, time());

        $this->redirect('api/protect/loginSuccess', $token_data);
    }


    public function loginSuccess()
    {
        $this->success('登入成功！', 'api/protect/home');
    }


    public function home()
    {
        $text = '访问成功！这是一个私有的接口' . PHP_EOL . PHP_EOL;

        $url = url("/api/protect/testRefreshToken", [LoginService::REFRESH_TOKEN => cookie(LoginService::REFRESH_TOKEN)]);
        $text .= "<a href='/api/protect/logout'>退出登入</a>" . PHP_EOL . PHP_EOL;

        $text .= "访问令牌：" . cookie(LoginService::ACCESS_TOKEN) . PHP_EOL . PHP_EOL;
        $text .= "令牌失效时间：" . date('Y-m-d H:i:s', cookie(LoginService::ACCESS_TOKEN_EXPIRE_TIME)) . ' ';
        $text .= "<a href='$url'>刷新令牌</a><br/>" . PHP_EOL . PHP_EOL;

        $text .= "刷新令牌：" . cookie(LoginService::REFRESH_TOKEN) . PHP_EOL . PHP_EOL;
        $text .= "令牌失效时间：" . date('Y-m-d H:i:s', cookie(LoginService::REFRESH_TOKEN_EXPIRE_TIME)) . PHP_EOL . PHP_EOL;

        $text .= "用户信息：" . json_encode($this->getUser(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;
        $text .= "session :" . json_encode(Session::get(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) . PHP_EOL . PHP_EOL;
        echo nl2br($text);
    }


    /**
     * 测试刷新令牌
     * 网页端会自动刷新cookie中的令牌
     * 其他端如果不支持cookie才需要此接口
     */
    public function testRefreshToken()
    {
        $refresh_token = $this->request->param(LoginService::REFRESH_TOKEN);

        $token_data = (new LoginService())->refreshTokens($refresh_token);

        $this->redirect('api/protect/home');
    }


    //退出登入
    public function logout()
    {
        LoginService::clear_session();
        $this->redirect('api/protect/index');
    }
}
