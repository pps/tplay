<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

namespace app\api\controller;


use app\api\controller\base\Api;

class Index extends Api
{
    public function index()
    {
        $this->json_success('success', ['ip' => $this->request->ip()]);
    }

}
