-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.62-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 tplay.tplay_admin 结构
DROP TABLE IF EXISTS `tplay_admin`;
CREATE TABLE IF NOT EXISTS `tplay_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `name` varchar(50) NOT NULL COMMENT '账号',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `thumb` int(11) NOT NULL DEFAULT '0' COMMENT '头像',
  `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `login_ip` char(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `admin_cate_id` int(2) NOT NULL DEFAULT '1' COMMENT '角色id',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- 正在导出表  tplay.tplay_admin 的数据：~1 rows (大约)
INSERT INTO `tplay_admin` (`id`, `nickname`, `name`, `password`, `thumb`, `login_time`, `login_ip`, `admin_cate_id`, `create_time`, `update_time`) VALUES
	(1, 'Tplay', 'admin', '1722c9d6b1f988dbe7ee898008a77fde', 1, 1721789835, '127.0.0.1', 1, 1649328230, 1717573454);

-- 导出  表 tplay.tplay_admin_cate 结构
DROP TABLE IF EXISTS `tplay_admin_cate`;
CREATE TABLE IF NOT EXISTS `tplay_admin_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `permissions` varchar(1000) NOT NULL DEFAULT '' COMMENT '权限',
  `desc` varchar(50) NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色';

-- 正在导出表  tplay.tplay_admin_cate 的数据：~1 rows (大约)
INSERT INTO `tplay_admin_cate` (`id`, `name`, `permissions`, `desc`, `create_time`, `update_time`) VALUES
	(1, '超级管理员', '52,87,88,120,121,132,139,141,89,138,46,48,84,85,86,119,31,64,66,67,71,118,65,68,69,75,102,103,146,147,111,112,113,114,115,116,127,142,33,34,35,76,100,101,143,36,37,38,39,40,98,99,128,22,7,8,24,25,26,91,27,28,29,96,30,97,1,10,12,13,14,15,16,17,148,152,154,155,156,159,11,41,42,44,45,106,145,43,105,108,2,3,4,5,51,117,153,18,19,20,21,57,58,59,130,131,129,133,134,135,136,53,54,55,56,61,122,140,92,94,137,77,78,79,80,81,82,83,110', '', 1649328138, 1721802043);

-- 导出  表 tplay.tplay_admin_log 结构
DROP TABLE IF EXISTS `tplay_admin_log`;
CREATE TABLE IF NOT EXISTS `tplay_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_menu_id` int(11) NOT NULL COMMENT '操作菜单id',
  `admin_id` int(11) NOT NULL COMMENT '操作者id',
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作ip',
  `url` varchar(255) NOT NULL DEFAULT '',
  `params` varchar(20000) NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台日志';

-- 正在导出表  tplay.tplay_admin_log 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_admin_menu 结构
DROP TABLE IF EXISTS `tplay_admin_menu`;
CREATE TABLE IF NOT EXISTS `tplay_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL COMMENT '模块',
  `controller` varchar(50) NOT NULL COMMENT '控制器',
  `function` varchar(50) NOT NULL COMMENT '方法',
  `parameter` varchar(50) NOT NULL DEFAULT '' COMMENT '参数',
  `description` varchar(50) NOT NULL DEFAULT '' COMMENT '描述',
  `is_display` int(1) NOT NULL DEFAULT '1' COMMENT '1显示在左侧菜单2只作为节点',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1权限节点2普通节点',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级菜单0为顶级菜单',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `is_open` int(1) NOT NULL DEFAULT '0' COMMENT '0默认闭合1默认展开',
  `orders` int(11) NOT NULL DEFAULT '0' COMMENT '排序值，越小越靠前',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module` (`module`) USING BTREE,
  KEY `controller` (`controller`) USING BTREE,
  KEY `function` (`function`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- 正在导出表  tplay.tplay_admin_menu 的数据：~137 rows (大约)
INSERT INTO `tplay_admin_menu` (`id`, `name`, `module`, `controller`, `function`, `parameter`, `description`, `is_display`, `type`, `pid`, `icon`, `is_open`, `orders`, `create_time`, `update_time`) VALUES
	(1, '系统', '', '', '', '', '系统设置。', 1, 1, 0, 'fa-cog', 0, 30, 0, 1581073013),
	(2, '高级', '', '', '', '', '菜单管理。', 1, 1, 1, 'fa-paw', 0, 10, 0, 1649318909),
	(3, '系统菜单', 'admin', 'menu', 'index', '', '系统菜单管理', 1, 1, 2, 'fa-share-alt', 0, 0, 0, 1581073030),
	(4, '新增/修改', 'admin', 'menu', 'publish', '', '新增/修改系统菜单.', 2, 1, 3, '', 0, 0, 1516948769, 1516948769),
	(5, '删除', 'admin', 'menu', 'delete', '', '删除系统菜单。', 2, 1, 3, '', 0, 0, 1516948857, 1516948857),
	(7, '个人信息', 'admin', 'admin', 'personal', '', '个人信息修改。', 1, 1, 22, 'fa-user', 0, 0, 1516949435, 1608538891),
	(8, '修改密码', 'admin', 'admin', 'editpassword', '', '管理员修改个人密码。', 2, 1, 7, 'fa-unlock-alt', 0, 0, 1516949702, 1608538880),
	(10, '系统设置', 'admin', 'webconfig', 'index', '', '网站相关设置。', 1, 1, 1, 'fa-internet-explorer', 0, 0, 1516949994, 1649318636),
	(11, '保存修改', 'admin', 'webconfig', 'publish', '', '修改网站设置。', 2, 1, 10, 'fa-tag', 0, 1, 1516950047, 1670839634),
	(12, '邮件设置', 'admin', 'emailconfig', 'index', '', '邮件配置。', 2, 1, 10, 'fa-envelope', 0, 0, 1516950129, 1649318610),
	(13, '修改', 'admin', 'emailconfig', 'publish', '', '修改邮件设置。', 2, 1, 12, '', 0, 0, 1516950215, 1516950215),
	(14, '发送测试邮件', 'admin', 'emailconfig', 'mailto', '', '发送测试邮件。', 2, 1, 12, '', 0, 0, 1516950295, 1516950295),
	(18, 'URL美化', 'admin', 'urlsconfig', 'index', '', 'URL美化', 1, 1, 2, 'fa-link', 0, 0, 1516950738, 1645173161),
	(19, '新增/修改', 'admin', 'urlsconfig', 'publish', '', '新增/修改url设置。', 2, 1, 18, '', 0, 0, 1516950850, 1516950850),
	(20, '启用/禁用', 'admin', 'urlsconfig', 'status', '', '启用/禁用url美化。', 2, 1, 18, '', 0, 0, 1516950909, 1516950909),
	(21, '删除', 'admin', 'urlsconfig', 'delete', '', ' 删除url美化规则。', 2, 1, 18, '', 0, 0, 1516950941, 1516950941),
	(22, '权限', '', '', '', '', '会员管理。', 1, 1, 0, 'fa-users', 0, 20, 1516950991, 1649318328),
	(24, '管理员', 'admin', 'admin', 'index', '', '系统管理员列表。', 1, 1, 22, 'fa-user-circle-o', 0, 0, 1516951163, 1649318427),
	(25, '新增/修改', 'admin', 'admin', 'publish', '', '新增/修改系统管理员。', 2, 1, 24, '', 0, 0, 1516951224, 1516951224),
	(26, '删除', 'admin', 'admin', 'delete', '', '删除管理员。', 2, 1, 24, '', 0, 0, 1516951253, 1516951253),
	(27, '权限组', 'admin', 'AdminCate', 'index', '', '角色组', 1, 1, 22, 'fa-dot-circle-o', 0, 0, 1516951353, 1649318367),
	(28, '新增/修改', 'admin', 'AdminCate', 'publish', '', '新增/修改权限组。', 2, 1, 27, '', 0, 0, 1516951483, 1516951483),
	(29, '删除', 'admin', 'AdminCate', 'delete', '', '删除权限组。', 2, 1, 27, '', 0, 0, 1516951515, 1516951515),
	(30, '操作日志', 'admin', 'AdminLog', 'index', '', '系统管理员操作日志。', 1, 1, 22, 'fa-pencil', 0, 0, 1516951754, 1681713740),
	(31, 'CMS', '', '', '', '', 'CMS 内容管理。', 1, 1, 0, 'fa-th-large', 1, 10, 1516952262, 1649318280),
	(33, '分类', 'admin', 'Articlecate', 'index', '', '文章分类管理。', 1, 1, 31, 'fa-tag', 0, 3, 1516952856, 1681701848),
	(34, '新增/修改', 'admin', 'Articlecate', 'publish', '', '新增/修改文章分类。', 2, 1, 33, 'fa-tag', 0, 0, 1516952896, 1681701873),
	(35, '删除', 'admin', 'Articlecate', 'delete', '', '删除文章分类。', 2, 1, 33, 'fa-tag', 0, 0, 1516952942, 1681701879),
	(36, '文章', 'admin', 'Article', 'index', '', '文章管理。', 1, 1, 31, 'fa-bookmark', 0, 4, 1516953011, 1681701891),
	(37, '新增/修改', 'admin', 'Article', 'publish', '', '新增/修改文章。', 2, 1, 36, 'fa-tag', 0, 0, 1516953056, 1681701899),
	(38, '审核/拒绝', 'admin', 'article', 'status', '', '审核/拒绝文章。', 2, 1, 36, '', 0, 0, 1516953113, 1516953113),
	(39, '推荐/取消', 'admin', 'Article', 'istop_time', '', '推荐/取消。', 2, 1, 36, 'fa-tag', 0, 0, 1516953162, 1681701921),
	(40, '删除', 'admin', 'article', 'delete', '', '删除文章。', 2, 1, 36, 'fa-asterisk', 0, 0, 1516953183, 1645180258),
	(41, '附件', 'admin', 'attachment', 'index', '', '附件管理。', 1, 1, 1, 'fa-cube', 0, 1, 1516953306, 1649318660),
	(42, '附件审核', 'admin', 'attachment', 'status', '', '附件审核。', 2, 1, 41, 'fa-asterisk', 0, 0, 1516953359, 1655952448),
	(43, '附件上传', 'admin', 'attachment', 'upload', '', '附件上传。', 2, 1, 41, '', 0, 1, 1516953392, 1516953392),
	(44, '附件下载', 'admin', 'attachment', 'download', '', '附件下载。', 2, 1, 41, '', 0, 0, 1516953430, 1516953430),
	(45, '附件删除', 'admin', 'attachment', 'delete', '', '附件删除。', 2, 1, 41, '', 0, 0, 1516953477, 1516953477),
	(46, '消息', 'admin', 'Tomessages', 'index', '', '消息管理。', 2, 1, 87, 'fa-comments', 0, 2, 1516953526, 1681701805),
	(48, '删除', 'admin', 'Tomessages', 'delete', '', '消息删除。', 2, 1, 46, 'fa-asterisk', 0, 0, 1516953648, 1681701813),
	(50, '管理员登录', 'admin', 'common', 'login', '', '', 2, 2, 104, 'fa-asterisk', 0, 1, 1516954517, 1718679218),
	(51, '排序', 'admin', 'menu', 'orders', '', '系统菜单排序。', 2, 1, 3, '', 0, 0, 1517562047, 1517562047),
	(52, '网站', '', '', '', '', '', 1, 1, 0, 'fa-send', 0, 0, 1581067786, 1599641038),
	(53, '扩展子配置', 'admin', 'ConfigOption', 'index', '', '', 2, 1, 57, '', 0, 10, 1581072344, 1617264867),
	(54, '新增/修改', 'admin', 'ConfigOption', 'publish', '', '', 2, 1, 53, '', 0, 0, 1581072401, 1581072401),
	(55, '删除', 'admin', 'ConfigOption', 'delete', '', '', 2, 1, 53, '', 0, 0, 1581072427, 1581072427),
	(56, '显示/隐藏', 'admin', 'ConfigOption', 'status', '', '', 2, 1, 53, '', 0, 0, 1581072456, 1581072456),
	(57, '扩展配置', 'admin', 'Config', 'index', '', '', 1, 1, 2, 'fa-grav', 0, 0, 1581072519, 1660029911),
	(58, '新增/修改', 'admin', 'Config', 'publish', '', '', 2, 1, 57, '', 0, 0, 1581072579, 1581072579),
	(59, '删除', 'admin', 'Config', 'delete', '', '', 2, 1, 57, '', 0, 0, 1581072605, 1581072605),
	(61, '批量删除', 'admin', 'ConfigOption', 'deletes', '', '', 2, 1, 53, '', 0, 0, 1581072664, 1581072664),
	(64, '模板', 'admin', 'Templet', 'index', '', '', 1, 1, 31, 'fa-copy', 0, 1, 1599644649, 1645173972),
	(65, '栏目', 'admin', 'Catalog', 'index', '', '', 1, 1, 31, 'fa-windows', 0, 2, 1599701321, 1645173987),
	(66, '新增/修改', 'admin', 'Templet', 'publish', '', '', 2, 1, 64, '', 0, 0, 1599705520, 1599705520),
	(67, '删除', 'admin', 'Templet', 'delete', '', '', 2, 1, 64, '', 0, 0, 1599705558, 1599705558),
	(68, '新增/修改', 'admin', 'Catalog', 'publish', '', '', 2, 1, 65, '', 0, 0, 1599710325, 1599721798),
	(69, '删除', 'admin', 'Catalog', 'delete', '', '', 2, 1, 65, '', 0, 0, 1599710355, 1599721809),
	(71, '上传模板', 'admin', 'Templet', 'upload', '', '', 2, 1, 64, '', 0, 0, 1599735854, 1599735854),
	(75, '生成静态页', 'admin', 'Catalog', 'exportHtml', '', '', 2, 1, 65, '', 0, 0, 1600134972, 1600134972),
	(76, '审核', 'admin', 'Articlecate', 'status', '', '', 2, 1, 33, '', 0, 0, 1600146630, 1600146630),
	(77, '数据备份', 'admin', 'Databackup', 'index', '', '', 1, 1, 2, 'fa-database', 0, 2, 1600936940, 1660029575),
	(78, '备份文件列表', 'admin', 'Databackup', 'importlist', '', '', 2, 1, 77, '', 0, 0, 1600938506, 1600938506),
	(79, '还原', 'admin', 'Databackup', 'import', '', '', 2, 1, 77, '', 0, 0, 1600938557, 1600938659),
	(80, '删除备份文件', 'admin', 'Databackup', 'del', '', '', 2, 1, 77, '', 0, 0, 1600938603, 1600938603),
	(81, '备份', 'admin', 'Databackup', 'export', '', '', 2, 1, 77, '', 0, 0, 1600938638, 1600938638),
	(82, '修复表', 'admin', 'Databackup', 'repair', '', '', 2, 1, 77, '', 0, 0, 1600938694, 1600938694),
	(83, '优化表', 'admin', 'Databackup', 'optimize', '', '', 2, 1, 77, '', 0, 0, 1600938718, 1600938718),
	(84, '审核', 'admin', 'Tomessages', 'status', '', '', 2, 1, 46, '', 0, 0, 1604907214, 1604907214),
	(85, '新增/编辑', 'admin', 'Tomessages', 'publish', '', '', 2, 1, 46, '', 0, 0, 1604908198, 1604908198),
	(86, '发送公告', 'admin', 'Tomessages', 'sysmessage', '', '', 2, 1, 46, '', 0, 0, 1604908198, 1604908198),
	(87, '用户', 'admin', 'User', 'index', '', '', 1, 1, 52, 'fa-user', 0, 0, 1604912892, 1681701781),
	(88, '新增/修改', 'admin', 'User', 'publish', '', '', 2, 1, 87, 'fa-tag', 0, 0, 1604912952, 1681701787),
	(89, '积分', 'admin', 'PointLog', 'index', '', '', 2, 1, 87, 'fa-bitcoin', 0, 1, 1604917674, 1681701707),
	(91, '重置密码', 'admin', 'admin', 'resetpass', '', '', 2, 1, 24, '', 0, 0, 1612342055, 1612342055),
	(92, '代码生成', 'admin', 'CodeGeneration', 'index', '', '', 1, 1, 2, 'fa-asterisk', 0, 2, 1617266817, 1617266817),
	(94, '生成', 'admin', 'CodeGeneration', 'generation', '', '', 2, 1, 92, '', 0, 0, 1617336327, 1617336327),
	(96, '查看权限', 'admin', 'AdminCate', 'preview', '', '', 2, 1, 27, '', 0, 0, 1623230264, 1623230264),
	(98, '批量删除', 'admin', 'Article', 'deletes', '', '', 2, 1, 36, '', 0, 0, 1623230671, 1623230671),
	(99, '预览', 'admin', 'Article', 'perview', '', '', 2, 1, 36, '', 0, 0, 1623230671, 1623230671),
	(100, '排序', 'admin', 'Articlecate', 'sort', '', '', 2, 1, 33, '', 0, 0, 1623230671, 1623230671),
	(101, '预览', 'admin', 'Articlecate', 'perview', '', '', 2, 1, 33, '', 0, 0, 1623230671, 1623230671),
	(102, '排序', 'admin', 'Catalog', 'sort', '', '', 2, 1, 65, '', 0, 0, 1623230671, 1623230671),
	(103, '预览', 'admin', 'Catalog', 'perview', '', '', 2, 1, 65, '', 0, 0, 1623230671, 1623230671),
	(104, '其他', 'admin', '', '', '', '有登入验证，没有权限验证的路由，例如登入加了权限验证会导致登入不了', 2, 2, 0, 'fa-asterisk', 0, 40, 1623230716, 1718679208),
	(105, '图片选择/上传', 'admin', 'Attachment', 'selectImage', '', '', 2, 1, 41, '', 0, 1, 1623231185, 1623231185),
	(106, '附件网络图片持久化', 'admin', 'Attachment', 'imgPersistence', '', '', 2, 1, 41, '', 0, 0, 1623231185, 1623231185),
	(108, '添加网络图片', 'admin', 'Attachment', 'create', '', '', 2, 1, 41, '', 0, 1, 1623231185, 1623231185),
	(109, '退出登录', 'admin', 'Index', 'logout', '', '', 2, 2, 104, 'fa-asterisk', 0, 1, 1623231185, 1645174466),
	(110, '命令行备份', 'admin', 'Databackup', 'backup', '', '', 2, 1, 77, '', 0, 0, 1623231185, 1623231185),
	(111, 'HTML管理', 'admin', 'FileManage', 'index', '', '', 2, 1, 65, '', 0, 1, 1623231185, 1623231185),
	(112, '新增/编辑', 'admin', 'FileManage', 'publish', '', '', 2, 1, 111, '', 0, 0, 1623231379, 1623231379),
	(113, '删除', 'admin', 'FileManage', 'delete', '', '', 2, 1, 111, '', 0, 0, 1623231379, 1623231379),
	(114, '重命名', 'admin', 'FileManage', 'rename', '', '', 2, 1, 111, '', 0, 0, 1623231379, 1623231379),
	(115, '上传', 'admin', 'FileManage', 'upload', '', '', 2, 1, 111, '', 0, 0, 1623231379, 1623231379),
	(116, '新增目录', 'admin', 'FileManage', 'createDir', '', '', 2, 1, 111, '', 0, 0, 1623231379, 1623231379),
	(117, '批量添加', 'admin', 'Menu', 'batchAdd', '', '', 2, 1, 3, '', 0, 0, 1623231501, 1623231501),
	(118, '重命名', 'admin', 'Templet', 'rename', '', '', 2, 1, 64, '', 0, 0, 1623231501, 1623231501),
	(119, '批量删除', 'admin', 'Tomessages', 'deletes', '', '', 2, 1, 46, '', 0, 0, 1623231501, 1623231501),
	(120, '删除', 'admin', 'User', 'delete', '', '', 2, 1, 87, '', 0, 0, 1623231501, 1623231501),
	(121, '修改积分', 'admin', 'User', 'setPoint', '', '', 2, 1, 87, '', 0, 0, 1623231501, 1623231501),
	(122, '排序', 'admin', 'ConfigOption', 'sort', '', '', 2, 1, 53, '', 0, 0, 1623231782, 1623231782),
	(123, '首页', 'admin', 'Main', 'index', '', '', 2, 2, 104, '', 0, 0, 1623231782, 1623231782),
	(124, '清理缓存', 'admin', 'Main', 'clear', '', '', 2, 2, 104, 'fa-asterisk', 0, 0, 1623231782, 1645173920),
	(126, '后台布局', 'admin', 'Index', 'index', '', '', 2, 2, 104, '', 0, 0, 1623232537, 1623232537),
	(127, '发布到远程', 'admin', 'FileManage', 'ftpCover', '', '', 2, 1, 111, '', 0, 0, 1624613010, 1624613010),
	(128, '重新生成', 'admin', 'Article', 'createFile', '', '', 2, 1, 36, 'fa-asterisk', 0, 0, 1629975856, 1629975856),
	(129, '扩展配置标签', 'admin', 'ConfigTab', 'index', '', '', 2, 1, 57, 'fa-asterisk', 0, 1, 1629975856, 1629975856),
	(130, '排序', 'admin', 'Config', 'sort', '', '', 2, 1, 57, 'fa-asterisk', 0, 0, 1629975856, 1629975856),
	(131, '审核', 'admin', 'Config', 'status', '', '', 2, 1, 57, 'fa-asterisk', 0, 0, 1629975856, 1629975856),
	(132, '重置密码', 'admin', 'User', 'resetpass', '', '', 2, 1, 87, 'fa-asterisk', 0, 0, 1629975856, 1629975856),
	(133, '新增/修改', 'admin', 'ConfigTab', 'publish', '', '', 2, 1, 129, 'fa-asterisk', 0, 0, 1629975894, 1629975894),
	(134, '删除', 'admin', 'ConfigTab', 'delete', '', '', 2, 1, 129, 'fa-asterisk', 0, 0, 1629975894, 1629975894),
	(135, '排序', 'admin', 'ConfigTab', 'sort', '', '', 2, 1, 129, 'fa-asterisk', 0, 0, 1629975894, 1629975894),
	(136, '审核', 'admin', 'ConfigTab', 'status', '', '', 2, 1, 129, 'fa-asterisk', 0, 0, 1629975894, 1629975894),
	(137, '获取表字段信息', 'admin', 'CodeGeneration', 'getFieldsInfo', '', '', 2, 1, 92, 'fa-asterisk', 0, 0, 1645172736, 1645172736),
	(138, '批量删除', 'admin', 'PointLog', 'deletes', '', '', 2, 1, 89, 'fa-asterisk', 0, 0, 1645172736, 1645172736),
	(139, '设置会员', 'admin', 'User', 'setVips', '', '', 2, 1, 87, 'fa-asterisk', 0, 0, 1645172736, 1645172736),
	(140, '单选开关', 'admin', 'ConfigOption', 'single_status', '', '', 2, 1, 53, 'fa-asterisk', 0, 0, 1645180213, 1645180213),
	(141, '审核', 'admin', 'User', 'status', '', '', 2, 1, 87, 'fa-asterisk', 0, 0, 1650266964, 1650266964),
	(142, '解压', 'admin', 'FileManage', 'unzip', '', '', 2, 1, 111, 'fa-asterisk', 0, 0, 1650593397, 1650593397),
	(143, '更新treepath', 'admin', 'Articlecate', 'updateTreePath', '', '', 2, 1, 33, 'fa-asterisk', 0, 0, 1655952413, 1670839553),
	(145, '批量删除', 'admin', 'Attachment', 'deletes', '', '', 2, 1, 41, 'fa-asterisk', 0, 0, 1655952465, 1655952465),
	(146, '审核', 'admin', 'Catalog', 'status', '', '', 2, 1, 65, 'fa-tag', 0, 0, 1670839160, 1670839160),
	(147, '更新treepath', 'admin', 'Catalog', 'updateTreePath', '', '', 2, 1, 65, 'fa-tag', 0, 0, 1670839160, 1670839160),
	(148, '显示扩展配置', 'admin', 'Config', 'indexWebconfig', '', '', 2, 1, 10, 'fa-tag', 0, 0, 1670839811, 1682145824),
	(152, '编辑配置', 'admin', 'Config', 'publishWebconfig', '', '', 2, 1, 148, 'fa-tag', 0, 0, 1682145873, 1682145873),
	(153, '清理菜单', 'admin', 'menu', 'batchdel', '', '', 2, 1, 3, 'fa-tag', 0, 0, 1718674554, 1718674554),
	(154, '网站维护', 'admin', 'webconfig', 'switch_site_close', '', '', 2, 1, 10, 'fa-tag', 0, 0, 1718674554, 1718674554),
	(155, '开发模式', 'admin', 'webconfig', 'switch_app_debug', '', '', 2, 1, 10, 'fa-tag', 0, 0, 1718674554, 1718674554),
	(156, '操作日志', 'admin', 'webconfig', 'switch_open_log', '', '', 2, 1, 10, 'fa-tag', 0, 0, 1718674554, 1718674554),
	(157, '接口文档', 'admin', 'doc', 'index', '', '', 2, 2, 104, 'fa-tag', 0, 2, 1718674644, 1721802127),
	(158, '文档详情', 'admin', 'doc', 'doc', '', '', 2, 2, 157, 'fa-tag', 0, 0, 1718674661, 1721802136),
	(159, '用户模块开关', 'admin', 'Webconfig', 'switch_open_user_module', '', '', 2, 1, 10, 'fa-tag', 0, 0, 1721802013, 1721802013),
	(160, 'test', 'admin', 'Doc', 'test', '', '', 2, 2, 157, 'fa-tag', 0, 0, 1721815706, 1721815706);

-- 导出  表 tplay.tplay_article 结构
DROP TABLE IF EXISTS `tplay_article`;
CREATE TABLE IF NOT EXISTS `tplay_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL COMMENT '标题',
  `seo_title` varchar(50) NOT NULL DEFAULT '',
  `seo_keyword` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(255) NOT NULL DEFAULT '' COMMENT '标签',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT 'html文件名',
  `page_views` int(11) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `article_cate_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `catalog_id` int(11) NOT NULL DEFAULT '0' COMMENT '栏目id',
  `thumb` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `content2` mediumtext NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT '发布人id',
  `edit_admin_id` int(11) NOT NULL COMMENT '修改人id',
  `uploadip` char(15) NOT NULL COMMENT '操作ip',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0待审核1已审核',
  `article_editor` varchar(50) NOT NULL DEFAULT '' COMMENT '指定编辑器',
  `source` varchar(255) NOT NULL DEFAULT '' COMMENT '文章来源地址',
  `istop_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章';

-- 正在导出表  tplay.tplay_article 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_article_cate 结构
DROP TABLE IF EXISTS `tplay_article_cate`;
CREATE TABLE IF NOT EXISTS `tplay_article_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT ' 上级节点id',
  `title` varchar(50) NOT NULL COMMENT '分类名称',
  `en_name` varchar(50) NOT NULL DEFAULT '' COMMENT '英文别名',
  `seo_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keyword` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO关键词',
  `seo_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：1显示0隐藏',
  `tree_path` varchar(50) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章分类';

-- 正在导出表  tplay.tplay_article_cate 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_attachment 结构
DROP TABLE IF EXISTS `tplay_attachment`;
CREATE TABLE IF NOT EXISTS `tplay_attachment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL DEFAULT '' COMMENT '所属模块',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `fileext` varchar(50) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `uploadip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL COMMENT '审核时间',
  `use` varchar(255) NOT NULL DEFAULT '' COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件表';

-- 正在导出表  tplay.tplay_attachment 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_catalog 结构
DROP TABLE IF EXISTS `tplay_catalog`;
CREATE TABLE IF NOT EXISTS `tplay_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL COMMENT '栏目名',
  `path` varchar(50) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `sort` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0单页1文章列表',
  `catalog_templet` varchar(50) NOT NULL DEFAULT '' COMMENT '栏目模板',
  `article_templet` varchar(50) NOT NULL DEFAULT '' COMMENT '文章模板',
  `article_rule` varchar(50) NOT NULL DEFAULT '' COMMENT '文章命名规则',
  `articlelist_rule` varchar(50) NOT NULL DEFAULT '' COMMENT '列表命名规则',
  `articlelist_rows` int(11) NOT NULL DEFAULT '10' COMMENT '分页',
  `articlelist_sort` varchar(50) NOT NULL DEFAULT 'id desc' COMMENT '分页排序',
  `articlelist_where` varchar(500) NOT NULL DEFAULT '' COMMENT '分页条件',
  `seo_title` varchar(50) NOT NULL DEFAULT '',
  `seo_keyword` varchar(500) NOT NULL DEFAULT '',
  `seo_description` varchar(500) NOT NULL DEFAULT '',
  `tree_path` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='栏目';

-- 正在导出表  tplay.tplay_catalog 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_cate_catalog 结构
DROP TABLE IF EXISTS `tplay_cate_catalog`;
CREATE TABLE IF NOT EXISTS `tplay_cate_catalog` (
  `article_cate_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  PRIMARY KEY (`article_cate_id`,`catalog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  tplay.tplay_cate_catalog 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_config 结构
DROP TABLE IF EXISTS `tplay_config`;
CREATE TABLE IF NOT EXISTS `tplay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab_id` int(11) NOT NULL DEFAULT '0' COMMENT '标签id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `value` varchar(500) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '审核,1:启用,0停用',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `name_label` varchar(50) NOT NULL DEFAULT '',
  `value_label` varchar(50) NOT NULL DEFAULT '',
  `desc_label` varchar(50) NOT NULL DEFAULT '',
  `image_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1启用图片配置',
  `image_label` varchar(50) NOT NULL DEFAULT '',
  `image_aux` varchar(50) NOT NULL DEFAULT '',
  `color_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1启用颜色配置',
  `color_label` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展配置';

-- 正在导出表  tplay.tplay_config 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_config_option 结构
DROP TABLE IF EXISTS `tplay_config_option`;
CREATE TABLE IF NOT EXISTS `tplay_config_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父配置id',
  `value` varchar(500) NOT NULL DEFAULT '' COMMENT '配置值',
  `description` varchar(500) NOT NULL DEFAULT '' COMMENT '描述',
  `image` int(11) NOT NULL DEFAULT '0' COMMENT '图标、图片',
  `color` varchar(50) NOT NULL DEFAULT '' COMMENT '颜色',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '多选:1:启用,0停用',
  `single_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '单选:1:启用,0停用',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展子配置';

-- 正在导出表  tplay.tplay_config_option 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_config_tab 结构
DROP TABLE IF EXISTS `tplay_config_tab`;
CREATE TABLE IF NOT EXISTS `tplay_config_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1显示到系统配置0不显示',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `name_label` varchar(50) NOT NULL DEFAULT '',
  `value_label` varchar(50) NOT NULL DEFAULT '',
  `desc_label` varchar(50) NOT NULL DEFAULT '',
  `image_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1启用图片配置',
  `image_label` varchar(50) NOT NULL DEFAULT '',
  `image_aux` varchar(50) NOT NULL DEFAULT '',
  `color_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1启用颜色配置',
  `color_label` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展配置标签';

-- 正在导出表  tplay.tplay_config_tab 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_emailconfig 结构
DROP TABLE IF EXISTS `tplay_emailconfig`;
CREATE TABLE IF NOT EXISTS `tplay_emailconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(5) NOT NULL COMMENT '邮箱配置标识',
  `from_email` varchar(50) NOT NULL COMMENT '邮件来源也就是邮件地址',
  `from_name` varchar(50) NOT NULL,
  `smtp` varchar(50) NOT NULL COMMENT '邮箱smtp服务器',
  `username` varchar(100) NOT NULL COMMENT '邮箱账号',
  `password` varchar(100) NOT NULL COMMENT '邮箱密码',
  `title` varchar(200) NOT NULL COMMENT '邮件标题',
  `content` text NOT NULL COMMENT '邮件模板',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='邮件配置表';

-- 正在导出表  tplay.tplay_emailconfig 的数据：~1 rows (大约)
INSERT INTO `tplay_emailconfig` (`id`, `email`, `from_email`, `from_name`, `smtp`, `username`, `password`, `title`, `content`) VALUES
	(1, 'email', '', 'Tplay', 'smtp.qq.com', '', '', '邮箱验证', '<div style="background-color: #ececec; padding: 35px;">\r\n<table style="width: 600px; margin: 0px auto; text-align: left; position: relative; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; font-size: 14px; font-family: 微软雅黑, 黑体; line-height: 1.5; box-shadow: #999999 0px 0px 5px; border-collapse: collapse; background-position: initial initial; background-repeat: initial initial; background: #fff;" cellpadding="0" align="center">\r\n<tbody>\r\n<tr>\r\n<th style="height: 25px; line-height: 25px; padding: 15px 35px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #42a3d3; background-color: #49bcff; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;" valign="middle"><span style="color: #ffffff; font-family: 微软雅黑; font-size: x-large;">请完成邮箱验证! </span></th>\r\n</tr>\r\n<tr>\r\n<td>\r\n<div style="padding: 25px 35px 40px; background-color: #fff;">\r\n<h2 style="margin: 5px 0px;"><span style="line-height: 20px; color: #333333;"> <span style="line-height: 22px; font-size: large;"> 亲爱的 {nickname}<br />您正在进行注册操作，请在浏览器打开以下链接，以完成激活操作</span></span></h2>\r\n<pre>{activation_url}</pre>\r\n<p>首先感谢您加入本站！下面是您的账号信息<br />您的账号：{passport}<br />您的邮箱：<strong>{email}</strong><br />当您在使用本网站时，遵守当地法律法规。<br />如果您有什么疑问可以联系网站管理员</p>\r\n<p align="right">此为系统邮件，请勿回复</p>\r\n<p align="right">{time}</p>\r\n<div style="width: 700px; margin: 0 auto;">\r\n<div style="padding: 10px 10px 0; border-top: 1px solid #ccc; color: #747474; margin-bottom: 20px; line-height: 1.3em; font-size: 12px;">\r\n<p>此为系统邮件，请勿回复<br />请保管好您的邮箱，避免账号被他人盗用</p>\r\n<p>©***</p>\r\n</div>\r\n</div>\r\n</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>');

-- 导出  表 tplay.tplay_messages 结构
DROP TABLE IF EXISTS `tplay_messages`;
CREATE TABLE IF NOT EXISTS `tplay_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0公告1管理员2留言...',
  `from_user_id` int(11) NOT NULL DEFAULT '0',
  `to_msg_id` int(11) NOT NULL DEFAULT '0',
  `to_user_id` int(11) NOT NULL DEFAULT '0',
  `message` varchar(500) NOT NULL DEFAULT '' COMMENT '消息内容',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0待审核1通过-1拒绝',
  `read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未读1已读',
  `ip` char(15) NOT NULL,
  `tree_path` varchar(50) NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息';

-- 正在导出表  tplay.tplay_messages 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_point_log 结构
DROP TABLE IF EXISTS `tplay_point_log`;
CREATE TABLE IF NOT EXISTS `tplay_point_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `before` int(11) NOT NULL COMMENT '变化之前的积分',
  `symbol` char(1) NOT NULL COMMENT '+-符号',
  `change` int(11) NOT NULL COMMENT '变化的积分',
  `final` int(11) NOT NULL COMMENT '变换后的积分',
  `type` tinyint(4) NOT NULL COMMENT '类型',
  `remark` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '说明',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='积分';

-- 正在导出表  tplay.tplay_point_log 的数据：~0 rows (大约)


-- 导出  表 tplay.tplay_urlconfig 结构
DROP TABLE IF EXISTS `tplay_urlconfig`;
CREATE TABLE IF NOT EXISTS `tplay_urlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aliases` varchar(200) NOT NULL COMMENT '别名',
  `url` varchar(200) NOT NULL COMMENT '原url结构',
  `desc` varchar(50) NOT NULL DEFAULT '' COMMENT '备注',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0禁用1使用',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='URL美化';

-- 正在导出表  tplay.tplay_urlconfig 的数据：~1 rows (大约)
INSERT INTO `tplay_urlconfig` (`id`, `aliases`, `url`, `desc`, `status`, `create_time`, `update_time`) VALUES
	(1, 'admin_login', 'admin/common/login', '后台登录地址', 1, 1517621629, 1690195322);

-- 导出  表 tplay.tplay_user 结构
DROP TABLE IF EXISTS `tplay_user`;
CREATE TABLE IF NOT EXISTS `tplay_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '推荐人id',
  `passport` varchar(50) NOT NULL COMMENT '账号',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
  `user_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账号类型',
  `user_cate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户角色',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '角色等级',
  `head_pic` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `point` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `money` int(11) NOT NULL DEFAULT '0' COMMENT '余额',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '注册ip',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `stop_time` int(11) NOT NULL DEFAULT '0' COMMENT '到期时间',
  `create_time` int(11) NOT NULL COMMENT '注册时间',
  `login_time` int(11) NOT NULL COMMENT '最后登录时间',
  `salt` char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  tplay.tplay_user 的数据：~0 rows (大约)

-- 导出  表 tplay.tplay_webconfig 结构
DROP TABLE IF EXISTS `tplay_webconfig`;
CREATE TABLE IF NOT EXISTS `tplay_webconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '别名',
  `file_type` varchar(200) NOT NULL DEFAULT '' COMMENT '允许上传的类型',
  `file_size` bigint(20) NOT NULL DEFAULT '500' COMMENT '允许上传的最大值',
  `black_ip` varchar(2000) NOT NULL DEFAULT '' COMMENT 'ip黑名单',
  `article_editor` varchar(50) NOT NULL DEFAULT '' COMMENT '文章编辑器',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置';

-- 正在导出表  tplay.tplay_webconfig 的数据：~1 rows (大约)
INSERT INTO `tplay_webconfig` (`id`, `name`, `file_type`, `file_size`, `black_ip`, `article_editor`) VALUES
	(1, 'Tplay', 'jpg,png,gif,mp4,zip,jpeg,html', 10240, '', 'wangEditor');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
ALTER TABLE `tplay_article`
	ADD COLUMN `sort` INT(11) NOT NULL DEFAULT '0' AFTER `istop_time`;

ALTER TABLE `tplay_config_tab`
	ADD COLUMN `pid_open` TINYINT(4) NOT NULL DEFAULT '0' AFTER `color_label`;
ALTER TABLE `tplay_config`
	ADD COLUMN `pid_open` TINYINT(4) NOT NULL DEFAULT '0' AFTER `color_label`;
ALTER TABLE `tplay_config_tab`
	ADD COLUMN `value_open` TINYINT(4) NOT NULL DEFAULT '1' AFTER `name_label`,
	ADD COLUMN `desc_open` TINYINT(4) NOT NULL DEFAULT '1' AFTER `value_label`;
ALTER TABLE `tplay_config`
	ADD COLUMN `value_open` TINYINT(4) NOT NULL DEFAULT '1' AFTER `name_label`,
	ADD COLUMN `desc_open` TINYINT(4) NOT NULL DEFAULT '1' AFTER `value_label`;

ALTER TABLE `tplay_webconfig`
	ADD COLUMN `white_ip` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT 'ip白名单' AFTER `black_ip`;