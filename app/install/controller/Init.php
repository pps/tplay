<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace app\install\controller;

use file\DirHelper;
use think\Controller;

class Init extends Controller
{
    protected function _initialize()
    {
        if (!isInstalled()) {
            $this->error('未找到安裝文件，请重新安装!', '/');
        }
    }

    public function delete()
    {
        $install_path = APP_PATH . 'install' . DS;
        DirHelper::delDir($install_path);
        if (file_exists($install_path)) {
            $this->error("删除失败,请检查目录权限");
        }
        $this->success("删除成功");
    }
}