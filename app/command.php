<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2019/12/5
 * Time: 17:44
 */

// +----------------------------------------------------------------------
// 自定义命令
// +----------------------------------------------------------------------

return [
    "app\common\command\ArticleImgPersistence",
    "app\common\command\CheckBOM",
    "app\common\command\CreateTableByCsv",
    "app\common\command\ExportTagList",
    "app\common\command\ImportDedecms",
    "app\common\command\InstallDemo",
    "app\common\command\ResetPassWord",
    "app\common\command\Test",
];
