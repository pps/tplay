<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/11/3
 * Time: 16:49
 */

namespace paginate;


class PaginateHelper
{
    private $total; //数据表中总记录数
    private $listRows; //每页显示行数
    private $page; //当前页码
    private $pageNum; //总页数
    private $config = array("prev" => "上一页", "next" => "下一页", "first" => "首 页", "last" => "尾 页");
    private $uri;
    private $listNum = 8;

    /**
     * PaginateHelper constructor.
     * @param $total int 总记录数
     * @param int $listRows 每页显示行数
     * @param null $currentPage 当前页码
     * @param string $query_string 追加get参数
     */
    public function __construct($total, $listRows = 10, $currentPage = null, $query_string = "")
    {
        $this->total = $total;//总记录数
        $this->listRows = $listRows;//每页记录数
        $this->pageNum = ceil($this->total / $this->listRows);//计算总页数
        $this->uri = $this->getUri($query_string);
        $this->setPage($currentPage);
    }

    /**
     * 获取sql的limit语句
     * @return string
     */
    public function getLimitStr()
    {
        return "Limit " . ($this->page - 1) * $this->listRows . ", {$this->listRows}";
    }

    private function setPage($currentPage = null)
    {
        $page = $currentPage ?: ($_GET["page"]??1);
        $page = intval($page);
        $this->page = $page > 0 ? ($page > $this->pageNum ? $this->pageNum : $page) : 1;//当前页码
    }

    /**
     * 获取当前uri
     * @param $query_string
     * @return string
     */
    public function getUri($query_string)
    {
        $url = $_SERVER["REQUEST_URI"] . (strpos($_SERVER["REQUEST_URI"], '?') ? '' : "?") . $query_string;
        $parse = parse_url($url);
        if (isset($parse["query"])) {
            parse_str($parse['query'], $params);
            unset($params["page"]);//去除page参数
            $url = $parse['path'] . '?' . http_build_query($params);
        }
        return $url;
    }

    /**
     * 当前页的起始位置
     * @return int
     */
    public function start()
    {
        if ($this->total == 0)
            return 0;
        else
            return ($this->page - 1) * $this->listRows + 1;//记录从1开始
    }

    /**
     * 当前页的结束位置
     * @return mixed
     */
    public function end()
    {
        return min($this->page * $this->listRows, $this->total);
    }

    //首页
    private function first()
    {
        $html = "";
        if ($this->page == 1)
            $html .= '';
        else
            $html .= "&nbsp;&nbsp;<a href='{$this->uri}&page=1'>{$this->config["first"]}</a>&nbsp;&nbsp;";

        return $html;
    }

    //上一页
    private function prev()
    {
        $html = "";
        if ($this->page == 1)
            $html .= '';
        else
            $html .= "&nbsp;&nbsp;<a href='{$this->uri}&page=" . ($this->page - 1) . "'>{$this->config["prev"]}</a>&nbsp;&nbsp;";

        return $html;
    }


    private function pageList()
    {
        $linkPage = "";

        $inum = floor($this->listNum / 2);

        for ($i = $inum; $i >= 1; $i--) {
            $page = $this->page - $i;

            if ($page < 1)
                continue;
            $linkPage .= "&nbsp;<a href='{$this->uri}&page={$page}'>{$page}</a>&nbsp;";
        }

        $linkPage .= "&nbsp;{$this->page}&nbsp;";

        for ($i = 1; $i <= $inum; $i++) {
            $page = $this->page + $i;
            if ($page <= $this->pageNum)
                $linkPage .= "&nbsp;<a href='{$this->uri}&page={$page}'>{$page}</a>&nbsp;";
            else
                break;
        }

        return $linkPage;
    }

    //下一页
    private function next()
    {
        $html = "";
        if ($this->page >= $this->pageNum)
            $html .= '';
        else
            $html .= "&nbsp;&nbsp;<a href='{$this->uri}&page=" . ($this->page + 1) . "'>{$this->config["next"]}</a>&nbsp;&nbsp;";

        return $html;
    }

    //尾页
    private function last()
    {
        $html = "";
        if ($this->page == $this->pageNum)
            $html .= '';
        else
            $html .= "&nbsp;&nbsp;<a href='{$this->uri}&page=" . ($this->pageNum) . "'>{$this->config["last"]}</a>&nbsp;&nbsp;";

        return $html;
    }

    //跳到第几页
    private function goPage()
    {
        return '&nbsp;&nbsp;<input type="text" onkeydown="javascript:if(event.keyCode==13){var page=(this.value>' . $this->pageNum . ')?' . $this->pageNum . ':this.value;location=\'' . $this->uri . '&page=\'+page+\'\'}" value="' . $this->page . '" style="width:25px"><input type="button" value="GO" onclick="javascript:var page=(this.previousSibling.value>' . $this->pageNum . ')?' . $this->pageNum . ':this.previousSibling.value;location=\'' . $this->uri . '&page=\'+page+\'\'">&nbsp;&nbsp;';
    }

    //显示分页
    public function fpage($display = array(0, 1, 2, 3, 4, 5, 6, 7, 8))
    {
        $html[0] = "&nbsp;&nbsp;共有<b>{$this->total}</b>个记录&nbsp;&nbsp;";
        $html[1] = "&nbsp;&nbsp;每页显示<b>{$this->listRows}</b>条，本页<b>{$this->start()}~{$this->end()}</b>条&nbsp;&nbsp;";
        $html[2] = "&nbsp;&nbsp;<b>{$this->page}/{$this->pageNum}</b>页&nbsp;&nbsp;";
        $html[3] = $this->first();
        $html[4] = $this->prev();
        $html[5] = $this->pageList();
        $html[6] = $this->next();
        $html[7] = $this->last();
        $html[8] = $this->goPage();
        $fpage = '';
        foreach ($display as $index) {
            $fpage .= $html[$index];
        }
        return $fpage;
    }

}