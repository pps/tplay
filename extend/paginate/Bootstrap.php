<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace paginate;

class Bootstrap extends \think\paginator\driver\Bootstrap
{

    /**
     * 渲染分页html
     * @return mixed
     */
    public function render()
    {
        if ($this->hasPages()) {
            if ($this->simple) {
                return sprintf(
                    '<ul class="pager">%s %s</ul>',
                    $this->getPreviousButton("上一页"),
                    $this->getNextButton("下一页")
                );
            } else {
                return sprintf(
                    '<ul class="pagination">%s %s %s</ul>',
                    $this->getPreviousButton("上一页"),
                    $this->getLinks(),
                    $this->getNextButton("下一页")
                );
            }
        }
    }
}
