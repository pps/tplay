<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 19:23
 */

namespace time;


use think\Exception;
use think\Log;

class DateHelper
{

    /**
     * 获取当前时间
     * @param string $format
     * @return false|string
     */
    public static function getNow($format = 'Y-m-d H:i:s')
    {
        return date($format, time());//时间戳转日期
    }

    /**
     * 检测是否为日期
     * @param $dateString
     * @return bool
     */
    public static function isDate($dateString)
    {
        /*date函数会给月和日补零，所以最终用unix时间戳来校验*/
        return strtotime(date('Y-m', strtotime($dateString))) === strtotime($dateString);
    }

    /**
     * 检测是否为Y-m-d格式日期
     * @param $date
     * @return bool
     */
    public static function isDateYmd($date)
    {
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts)) {
            return checkdate($parts[2], $parts[3], $parts[1]);
        } else {
            return false;
        }
    }

    /**
     * 日期格式转换
     * @param $dateTime string 原始日期
     * @param string $format 指定日期格式
     * @return false|string
     */
    public static function formatDate($dateTime, $format = 'Y-m-d H:i:s')
    {
        $nowtime = strtotime($dateTime);//得到时间戳
        return date($format, $nowtime);//时间戳转日期
    }


    /**
     * 时间文字简化
     * @param $startdate [日期或时间戳]
     * @return string
     */
    public static function formatTimeTip($startdate)
    {
        if ($startdate) {
            if (is_numeric($startdate)) {
                $startdate = date('Y-m-d H:i:s', $startdate);//时间戳转日期（要是日期的话可以不用转）
            }
            $enddate = date('Y-m-d H:i:s');//当前日期
            $date = floor((strtotime($enddate) - strtotime($startdate)) / 86400);
            $hour = floor((strtotime($enddate) - strtotime($startdate)) % 86400 / 3600);
            $minute = floor((strtotime($enddate) - strtotime($startdate)) % 86400 % 3600 / 60);
            $second = floor((strtotime($enddate) - strtotime($startdate)) % 86400 % 60);
            if ($date > 90) {
                return '很久前';
            } elseif ($date >= 30 && $date <= 90) {
                return floor($date / 30) . '个月前';
            } elseif ($date > 0 && $date < 30) {
                return $date . '天前';
            } elseif ($hour < 24 && $hour > 0) {
                return $hour . '小时前';
            } elseif ($minute < 60 && $minute > 0) {
                return $minute . '分钟前';
            } elseif ($second < 60 && $second > 0) {
                return $second . '秒前';
            } else {
                return '刚刚';
            }
        }
        return "null";
    }


    /**
     * 获取指定范围内的时间列表
     *
     * @param $startTime string 起始时间
     * @param $endTime string 结束时间
     * @param $interval string 时间的间隔，默认隔天
     * @param $format string 返回的时间格式
     * @param $inclued bool 是否包含首尾时间,默认不含首尾时间
     * @return array
     * @throws Exception
     */
    public static function dateTimeList($startTime, $endTime, $interval = 'tomorrow', $format = 'Y-m-d', $inclued = false)
    {
        $starttime = strtotime($startTime);
        $endtime = strtotime($endTime);
        /**
         * 日期的间隔写法如下:
         * +1 week 3 days 7 hours 5 seconds 获取一周后+3天+7小时+5秒的时间戳
         * -1 year 3 month 一年前-3个月
         * +1 month 获取下个月最后一天:如果是1月31号计算得到的却是3月3号，应该是2月最后一天才对，改为last day of +1 month即可
         *
         * Monday this week 这星期一
         * Monday 是从今天开始的下一个星期一,如果今天是周一获取到的是这周一的日期,如果今天是周二到周日获取到的是下周一的日期
         * last Monday 上星期一 (问题同上) 改为 date('m-d', strtotime('Monday this week'),time()-3600*24*7) 即可
         * next Sunday 下星期日 (问题同上)
         *
         * yesterday 昨天
         * first day of 获取本月1号的时间戳(永远为获取本月会死循环)
         * last day of  获取本月月底的时间戳
         * first day of next month 下个月月初的时间戳
         * last day of next month 或 last day of +1 month 下个月月底的时间戳
         * last day of last month  上个月月底的时间戳
         * 2022-02 first day of 获得固定时间的月初，得到2022-02-01
         * 2022-02 last day of  得到2022-02-28
         */
        $i = 0;
        $arr = [];
        if ($inclued) {
            $arr[] = date($format, $starttime);
        }
        while (($starttime = strtotime($interval, $starttime)) < $endtime) {
            $arr[] = date($format, $starttime);
            if ($i++ > 500) {
                Log::error($arr);
                throw new Exception('循环超过限制');//防止死循环
            }
        }
        if ($inclued) {
            $arr[] = date($format, $endtime);
        }
        return $arr;
    }

    /**
     * 拆分时段
     * @param $start string 时间格式H:i:s
     * @param $end string 时间格式H:i:s
     * @param $count int 拆分个数
     * @param $format string 返回的日期格式
     * @return array
     */
    public static function splitTimePeriod($start, $end, $count, $format = 'H:i:s')
    {
        $count = $count <= 0 ? 1 : $count;
        $second_total = strtotime($end) - strtotime($start);
        $arr = [];
        if ($count > 1) {
            $interval = floor($second_total / $count);
            $time_period = DateHelper::dateTimeList($start, $end, "+ $interval seconds", $format);
            foreach ($time_period as $k => $period) {
                if ($k == 0) {
                    $arr[] = $start . " - " . $period;
                } else {
                    $arr[] = $time_period[$k - 1] . " - " . $period;
                }
            }
            $arr[] = end($time_period) . " - " . $end;
        } else {
            $arr[] = $start . " - " . $end;
        }
        return $arr;
    }

}