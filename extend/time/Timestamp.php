<?php

namespace time;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 19:16
 */
class Timestamp
{
    /**
     * 获取当前时间戳
     * @return int
     */
    public static function getNow()
    {
        return time();
    }

    /**
     * 获取当前毫秒级别的时间戳
     * @return float
     */
    public static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        $time = (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
        return $time;
    }

    /**
     * 将毫秒时间戳转换成秒时间戳
     * @param $millis
     * @return int
     */
    public static function millis2Seconds($millis)
    {
        return intval(substr($millis, 0, -3));
    }

    /**
     * 获取今日开始时间戳
     * @return int 时间戳
     */
    public static function beginToday()
    {
        $today = getdate();
        return mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']);
    }

    /**
     * 获取今日结束时间戳
     * @return int
     */
    public static function endToday()
    {
        $today = getdate();
        return mktime(23, 59, 59, $today['mon'], $today['mday'], $today['year']);
    }

    /**
     * 获取昨日开始时间戳
     * @return int
     */
    public static function beginYesterday()
    {
        return self::beginToday() - 24 * 3600;
    }

    /**
     * 获取昨日结束时间戳
     * @return int
     */
    public static function endYesterday()
    {
        return self::endToday() - 24 * 3600;
    }


    /**
     * 获取本月起始时间戳
     * @return int
     */
    public static function monthStartTime()
    {
        return mktime(0, 0, 0, date('m'), 1, date('Y'));
    }


    /**
     * 获取本年起始时间戳
     * @return int
     */
    public static function yearStartTime()
    {
        return mktime(0, 0, 0, 1, 1, date('Y'));
    }


    /**
     * 获取指定范围内每天的起始结束时间戳
     * @param $startTime
     * @param $endTime
     * @return array
     */
    public static function dayTimeRangeList($startTime, $endTime)
    {
        $begin = self::dayStart($endTime);
        $arr = [];
        $arr[] = [$begin, $endTime];
        while (($begin = strtotime('-1 day', $begin)) >= $startTime) {
            $arr[] = [$begin, self::dayEnd($begin)];
        }
        if (end($arr)[0] > $startTime) {
            $arr[] = [$startTime, self::dayEnd($begin)];
        }
        return $arr;
    }

    /**
     * 时间戳 转 当日起始时间戳
     * @param $timestamp
     * @return int
     */
    public static function dayStart($timestamp)
    {
        return strtotime(date('Y-m-d 00:00:00', $timestamp));
    }

    /**
     * 时间戳 转 当日结束时间戳
     * @param $timestamp
     * @return int
     */
    public static function dayEnd($timestamp)
    {
        return strtotime(date('Y-m-d 23:59:59', $timestamp));
    }
}