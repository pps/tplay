<?php

namespace network;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/18
 * Time: 13:00
 */
class CurlHelper
{
    private $curl;
    private $showResponseHeader = false;
    private $responseHeader;
    private $showRequestHeader = false;
    private $requestHeader;
    private $response;
    private $responseBody;
    private $httpCode;
    private $error;

    public function __construct($url, $params = [])
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);//要求结果为字符串且输出到屏幕上
        $this->setMaxRedirs();
        $this->setHttpVersion();
        $this->setUserAgent();
        $this->setTimeout();
        $this->setEncoding();
        //跳过https证书验证
        if (substr($url, 0, 5) == 'https') {
            $this->sslVerify();
        }
        $this->setParams($params);
    }


    /**
     * 最多允许重定向多少次
     * @param int $count
     */
    public function setMaxRedirs($count = 10)
    {
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);//跟踪爬取重定向页面
        curl_setopt($this->curl, CURLOPT_MAXREDIRS, $count);//表示最多允许重定向多少次
    }

    /**
     * 设置 HTTP_VERSION
     * @param int $version
     */
    public function setHttpVersion($version = CURL_HTTP_VERSION_1_1)
    {
        curl_setopt($this->curl, CURLOPT_HTTP_VERSION, $version);
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent($userAgent = '')
    {
        if (!$userAgent) {
            $userAgent = empty($_SERVER['HTTP_USER_AGENT']) ? 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)' : $_SERVER['HTTP_USER_AGENT'];//模拟用户使用的浏览器
        }
        curl_setopt($this->curl, CURLOPT_USERAGENT, $userAgent);
    }

    /**
     * 设置超时时间
     * @param int $connect_timeout 连接超时时间(秒)
     * @param int $timeout 数据传输的最大允许时间(秒), 默认为0，意思是永远不会断开链接
     */
    public function setTimeout($connect_timeout = 10, $timeout = 180)
    {
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $connect_timeout);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout);
    }

    /**
     * 设置请求参数
     * @param $params array
     */
    public function setParams($params)
    {
        if ($params) {
            // 准备POST数据，判断包含文件上传
            if (is_array($params)) {
                foreach ($params as $key => $value) {
                    if (isset($value['path']) && isset($value['type']) && isset($value['name'])) {
                        $params[$key] = new \CURLFile($value['path'], $value['type'], $value['name']);
                    }
                }
            }
            // post请求
            curl_setopt($this->curl, CURLOPT_POST, true);
            // $post_data为array类型时multipart/form­data请求，string类型时application/x-www-form-urlencoded
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
        } else {
            // get请求
            curl_setopt($this->curl, CURLOPT_HTTPGET, true);
        }
    }


    /**
     * 指定一个自定义的HTTP请求方法
     * @param string $httpMethod
     */
    public function setHttpMethod($httpMethod = "GET")
    {
        //这可以用来执行除了GET和POST之外的HTTP方法，如PUT、DELETE等
        //这种方式提供了更多的灵活性，允许执行非标准的HTTP请求方法
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $httpMethod);
    }


    /**
     * 设置代理
     * @param $proxy_host string
     * @param $proxy_port int
     */
    public function setProxy($proxy_host, $proxy_port)
    {
        curl_setopt($this->curl, CURLOPT_PROXY, $proxy_host);
        curl_setopt($this->curl, CURLOPT_PROXYPORT, $proxy_port);
    }

    /**
     * 设置是否验证https证书
     * @param bool $ignore 默认不验证
     */
    public function sslVerify($ignore = false)
    {
        if ($ignore) {
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, TRUE);//严格校验
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 2);//严格校验
        } else {
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);//跳过证书检查
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);//从证书中检查SSL加密算法是否存在
        }
    }

    /**
     * 设置https证书，严格校验时才需要
     * @param $sslCertPath string
     * @param $sslKeyPath string
     */
    public function useCert($sslCertPath, $sslKeyPath)
    {
        //设置证书,证书路径,注意应该填写绝对路径
        //使用证书：cert 与 key 分别属于两个.pem文件
        //证书文件请放入服务器的非web目录下,防止被他人下载
        curl_setopt($this->curl, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($this->curl, CURLOPT_SSLCERT, $sslCertPath);
        curl_setopt($this->curl, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($this->curl, CURLOPT_SSLKEY, $sslKeyPath);
    }

    /**
     * 设置referer
     * @param $referer string
     */
    public function setReferer($referer)
    {
        if ($referer) {
            curl_setopt($this->curl, CURLOPT_REFERER, $referer);
        }
    }

    /**
     * 设置接收包大小
     * @param int $size
     */
    public function setBufferSize($size = 1024)
    {
        //设置的是 cURL 从网络中读取一块数据的大小，而不是 CURLOPT_WRITEFUNCTION 回调函数中传递给 $buffer 的数据块大小
        //设置成 128，意味着 cURL 会尝试从网络中读取 128 字节的数据块，然后再传递给 CURLOPT_WRITEFUNCTION 回调函数。
        //但是，实际上在读取数据块时，cURL 会根据网络状况和服务器响应速度等因素进行调整，读取的数据块大小不一定是 128 字节，可能会大于或小于 128 字节。
        //因此，如果你在 CURLOPT_WRITEFUNCTION 回调函数中对 $buffer 的长度进行了计算，输出的结果不一定是 128 字节。
        //如果需要在回调函数中精确控制数据块的大小，可以使用 CURLOPT_WRITEHEADER 选项，它类似于 CURLOPT_WRITEFUNCTION，但是只传递响应头部分数据。接下来，使用 CURLOPT_WRITEFUNCTION 选项处理响应主体部分数据，这样可以确保每次读取的数据块大小不会超过指定大小。
        curl_setopt($this->curl, CURLOPT_BUFFERSIZE, $size);
    }

    /**
     * 设置回调函数,捕获数据流
     * @param $callback
     */
    public function setSseCallback($callback)
    {
        if (!$callback) {
            $callback = function ($ch, $res) {
                // 这里可以处理接收到的数据
                // 输出接收到的数据流 或 处理过的数据
                echo $res;
                /**
                 * https://blog.csdn.net/qq_37391017/article/details/84256553
                 * 个别web服务器程序，特别是Win32下的web服务器程序，在发送结果到浏览器之前，仍然会缓存脚本的输出，直到程序结束为止
                 * 如果你不想让程序执行完毕才向浏器输出，那么你也可以用到ob_flush()和flush()来刷新缓存
                 * 这两个函数一般要一起使用，顺序是先ob_flush()，然后flush()，它们的作用是刷新缓冲区
                 */
                ob_flush();//刷新PHP自身的缓冲区
                flush();//只有在PHP做为apache的Module(handler或者filter)安装的时候, 才有实际作用. 它是刷新WebServer(可以认为特指apache)的缓冲区.
                // 这里返回数据的长度，以便 cURL 知道写入了多少数据
                return strlen($res);
            };
        }

        //CURLOPT_WRITEFUNCTION 允许你指定一个回调函数来处理通过 cURL 接收到的数据。这个选项通常与 CURLOPT_FILE 一起使用，用于将数据写入到一个文件中，而不是直接输出到浏览器。
        curl_setopt($this->curl, CURLOPT_WRITEFUNCTION, $callback);
    }


    /**
     * 设置回调函数,捕获数据流
     * @param $localFile string 保存文件的本地路径
     * @param $callback
     */
    public function setFileCallback($localFile, $callback)
    {
        if (!$callback) {
            $callback = function ($ch, $res) {
                // 这里可以处理接收到的数据.  例如，你可以在这里将数据写入到另一个文件中
                // 请注意，由于 CURLOPT_FILE 已经设置，所以数据将自动写入到文件中，Callback 函数在这里实际上并没有执行任何操作。如果你需要在数据写入文件之前或之后进行一些自定义处理，你可以在这个回调函数中实现。
                // 这里返回数据的长度，以便 cURL 知道写入了多少数据
                return strlen($res);
            };
        }

        curl_setopt($this->curl, CURLOPT_FILE, fopen($localFile, 'w')); // 打开文件准备写入
        //CURLOPT_WRITEFUNCTION 允许你指定一个回调函数来处理通过 cURL 接收到的数据。这个选项通常与 CURLOPT_FILE 一起使用，用于将数据写入到一个文件中，而不是直接输出到浏览器。
        curl_setopt($this->curl, CURLOPT_WRITEFUNCTION, $callback);
    }

    /**
     * 设置进度回调函数
     * @param $callback
     */
    public function setProgressCallback($callback = null)
    {
        if (!$callback) {
            /**
             * @param $resource cURL资源
             * @param $downloadSize int 预期的下载总大小
             * @param $downloaded int 已下载的大小
             * @param $uploadSize int 预期的上传总大小（如果使用POST方法）
             * @param $uploaded int 已上传的大小
             */
            $callback = function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) {
                if ($downloadSize > 0) {
                    echo " 下载进度: " . round(($downloaded / $downloadSize) * 100) . "%\r";
                }
            };
        }

        curl_setopt($this->curl, CURLOPT_NOPROGRESS, false); // 允许进度回调
        curl_setopt($this->curl, CURLOPT_PROGRESSFUNCTION, $callback); // 设置进度回调函数
    }

    /**
     * 设置转码
     * @param string $encode 默认是可解压缩gzip、deflate等压缩文件
     */
    public function setEncoding($encode = '')
    {
        curl_setopt($this->curl, CURLOPT_ENCODING, $encode);
    }

    /**
     * 设置header数据
     * @param $header array 支持数组['x:y','x2:zh',...]和字典类型['x'=>'y','x2'=>'zh',...]
     */
    public function setHeader($header)
    {
        if (!$header) {
            return;
        }
        // 将键值数组{key:val}转为索引数组['key:val']
        // 例如：['User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64)'] 转为 ['User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64)']
        if (!isset($header[0])) {
            foreach ($header as $hk => $hv) {
                unset ($header [$hk]);
                $header [] = $hk . ':' . $hv;
            }
        }
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
    }

    /**
     * 设置cookie
     * @param $cookie string
     */
    public function setCookie($cookie)
    {
        if ($cookie) {
            curl_setopt($this->curl, CURLOPT_COOKIE, $cookie);
        }
    }

    /**
     * 设置显示响应头
     */
    public function showResponseHeader()
    {
        curl_setopt($this->curl, CURLOPT_HEADER, true);//会显示响应内容+响应头
        $this->showResponseHeader = true;
    }

    /**
     * 设置显示请求头
     */
    public function showRequestHeader()
    {
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        $this->showRequestHeader = true;
    }

    /**
     * 执行请求
     * @param bool $autoClose 默认自动关闭连接
     */
    public function exec($autoClose = true)
    {
        // 发送请求
        $this->response = curl_exec($this->curl);
        $this->httpCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        if ($error = curl_error($this->curl)) {
            $this->error = $error;
        } else {
            if ($this->showRequestHeader) {// 请求头
                $this->requestHeader = curl_getinfo($this->curl, CURLINFO_HEADER_OUT);
            }
            //头信息大小，curl 在这里有 BUG，需要升级到7.4 或 7.5
            $headerSize = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
            $this->responseBody = substr($this->response, $headerSize);
            $this->responseHeader = substr($this->response, 0, $headerSize);
        }
        if ($autoClose) {
            $this->close();
        }
    }


    /**
     * 关闭连接
     */
    public function close()
    {
        curl_close($this->curl);
    }

    //获取响应状态码
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    //获取响应内容
    public function getResponse()
    {
        if ($this->showResponseHeader) {
            return $this->responseBody;
        }
        return $this->response;
    }

    //获取响应的错误消息
    public function getError()
    {
        return $this->error;
    }

    //获取响应体
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    //获取请求头
    public function getRequestHeader()
    {
        return $this->showRequestHeader ? $this->requestHeader : '';
    }

    //获取响应头
    public function getResponseHeader()
    {
        return $this->showResponseHeader ? $this->responseHeader : '';
    }
}