<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/3/26
 * Time: 17:24
 */

namespace network\ip;
include EXTEND_PATH . 'network/ip/IpLocation.php';

class IpHelper
{
    /**
     * 获取某个IP地址所在的位置
     * @param $ip
     * @return array = ['area'=>area , 'country'=>country]
     */
    public static function get($ip)
    {
        $Ips = new IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
        $area = $Ips->getlocation($ip); // 获取某个IP地址所在的位置
        return $area;
    }

    /**
     * 生成随机ip
     * @return string
     */
    public static function getRandIp()
    {
        return rand(13, 255) . '.' . rand(13, 255) . '.' . rand(13, 255) . '.' . rand(13, 255);
    }
}