<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 19:31
 */

namespace network;


class UrlHelper
{

    /**
     * 解析url
     * @param $path
     * @return mixed
     */
    public static function parse($path)
    {
//        $url = 'http://localhost/test.php?m=admin&c=index&a=lists&catid=1&page=1#top';
//        解析后的数组内容
//        array (
//            'dirname' => 'http://localhost',
//            'basename' => 'test.php?m=admin&c=index&a=lists&catid=1&page=1#top',
//            'extension' => 'php?m=admin&c=index&a=lists&catid=1&page=1#top',
//            'filename' => 'test',
//        )
        return pathinfo($path);
    }


    /**
     * 解析url
     * @param $path
     * @return mixed
     */
    public static function parse2($path)
    {
//        $url = 'http://localhost/test.php?m=admin&c=index&a=lists&catid=1&page=1#top';
//        解析后的数组内容
//        array (
//            'scheme' => 'http',
//            'host' => 'localhost',
//            'path' => '/test.php',
//            'query' => 'm=admin&c=index&a=lists&catid=1&page=1',
//            'fragment' => 'top',
//        )
        return parse_url($path);
    }


    //正则匹配
    public static function parse3()
    {
//        $url = 'http://localhost/test.php?m=admin&c=index&a=lists&catid=1&page=1#top';
//        preg_match_all("/(\w+=\w+)(#\w+)?/i",$url,$match);
//        var_export($match);
    }


    /**
     * 将字符串参数变为数组
     *
     * m=admin&c=index&a=lists&catid=1&page=1
     * 变为
     * array ('m' => 'admin','c' => 'index','a' => 'lists','catid' => '1','page' => '1')
     *
     * @param $query
     * @return array
     */
    public static function queryStr2Array($query)
    {
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1]??'';
        }
        return $params;
    }

    /**
     * 将数组变为字符串
     *
     * 和 convertUrlQuery()方法相反
     *
     * @param $array_query
     * @return string
     */
    public static function array2QueryStr($array_query)
    {
        return http_build_query($array_query);

//        $tmp = array();
//        foreach ($array_query as $k => $param) {
//            $tmp[] = $k . '=' . $param;
//        }
//        $params = implode('&', $tmp);
//        return $params;
    }
}