<?php
/**
 * Created by PhpStorm.
 * User: 中闽 < 1464674022@qq.com >
 * Date: 2020/2/4
 * Time: 12:47
 */

namespace phpqrcode;

require_once 'phpqrcode.php';

/**
 * php生成二维码
 * PHP环境必须开启支持GD2扩展库支持（一般情况下都是开启状态）
 * https://blog.csdn.net/sinat_35861727/article/details/73862991
 */
class QrCode
{
    /**
     * 生成二维码
     * @param $text [二维码包含的内容，可以是链接、文字、json字符串等等]
     * @param bool $outfile 生成二维码图片的文件名及路径, 默认为false，不生成文件，只将二维码图片返回输出；
     * @param int $level 默认为L，这个参数可传递的值分别是L(QR_ECLEVEL_L，7%)、M(QR_ECLEVEL_M，15%)、Q(QR_ECLEVEL_Q，25%)、H(QR_ECLEVEL_H，30%)，这个参数控制二维码容错率，不同的参数表示二维码可被覆盖的区域百分比，也就是被覆盖的区域还能识别
     * @param int $size 控制生成图片的大小，默认为4
     * @param int $margin 控制生成二维码的空白区域大小
     * @param bool $saveandprint 保存二维码图片并显示出来，$outfile必须传递图片路径
     */
    public static function png($text, $outfile = false, $level = QR_ECLEVEL_L, $size = 3, $margin = 4, $saveandprint = false)
    {
        \QRcode::png($text, $outfile, $level, $size, $margin, $saveandprint);
    }
}