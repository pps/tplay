<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2024/1/19
 * Time: 10:48
 * 1464674022@qq.com
 */

namespace string;


class Random
{

    /**
     * 随机获取一个值
     * @param $datas
     * @return mixed
     */
    public static function getOne($datas)
    {
        $max = count($datas) - 1;
        return $datas[rand(0, $max)];
    }

    /**
     * 随机获取N个值
     * @param $datas array
     * @param $num int
     * @return mixed
     */
    public static function getMany($datas, $num)
    {
        $num_req = count($datas) < $num ? count($datas) : $num;
        //num 不能 超过 data个数
        $randKeys = array_rand($datas, $num_req);//返回一个随机键名，或，返回多个键名的数组。
        if ($num_req == 1) {
            return [$datas[$randKeys]];
        } else {
            $randValues = [];
            foreach ($randKeys as $key) {
                $randValues[] = $datas[$key];
            }
            return $randValues;
        }
    }
}