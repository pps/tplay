<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/10/9
 * Time: 10:09
 */

namespace string;

/**
 * 加密函数
 * Class Encryption
 * @package string
 */
class Encryption
{
    /**
     * 生成随机密码
     * @param int $length [密码长度]
     * @param null $chars [设置密码字符集合]
     * @return bool|string
     */
    public static function randomStr($length = 32, $chars = null)
    {
        if (empty($chars)) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        }
        $chars = str_shuffle($chars);
        $num = $length < strlen($chars) - 1 ? $length : strlen($chars) - 1;
        return substr($chars, 0, $num);
    }

    /**
     * 生成一个唯一id
     * @return string
     */
    public static function uniqidStr()
    {
        //mt_rand() 函数返回结果的速度是 rand() 函数的 4 倍
        return uniqid((string)mt_rand(), true);
    }

    /**
     * 字符串(可逆)加密/解密
     * @param $value [需要加密的字符串]
     * @param int $type [加密解密（0：加密，1：解密）]
     * @return int|mixed    [加密或解密后的字符串]
     */
    public static function encryption($value, $type = 0)
    {
        $key = self::strToBin(md5(getSalt()));
        if (!$type) {
            return str_replace('=', '', base64_encode($value ^ $key));
        } else {
            return base64_decode($value) ^ $key;
        }
    }

    /**
     * 将字符串转换成二进制
     * @param $str
     * @return string
     */
    public static function strToBin($str)
    {
        //1.列出每个字符
        $arr = preg_split('/(?<!^)(?!$)/u', $str);
        //2.unpack字符
        foreach ($arr as &$v) {
            $temp = unpack('H*', $v);
            $v = base_convert($temp[1], 16, 2);
            unset($temp);
        }

        return join(' ', $arr);
    }

    /**
     * 二进制转换成字符串
     * @param $str
     * @return string
     */
    public static function binToStr($str)
    {
        $arr = explode(' ', $str);
        foreach ($arr as &$v) {
            $v = pack("H" . strlen(base_convert($v, 2, 16)), base_convert($v, 2, 16));
        }
        return join('', $arr);
    }

    /**
     * 对id编码加密的函数，可用于生成不重复的邀请码
     *
     * https://www.php.cn/php-weizijiaocheng-388405.html
     * id是唯一的,转成36进制,就能得到简短的不重复的邀请码
     * 防止别人反推id，把0剔除，当做补位符号，变成35进制，再打乱$source_string
     *
     * @param $num [用户id]
     * @param $len [邀请码补位长度]
     * @param $source_str [范围为‘0-9A-Z’字符串再打乱就得到了$source_str]
     * @return string [邀请码]
     */
    public static function inviteEncode($num, $len = 6, $source_str = 'ENV6J2R3HWXSTQA47MU5FCDGOPIB189KLYZ')
    {
        $code = '';
        while ($num > 0) {
            $mod = $num % 35;
            $num = ($num - $mod) / 35;
            $code = $source_str[$mod] . $code;
        }
        if (empty($code[$len - 1]))
            $code = str_pad($code, $len, '0', STR_PAD_LEFT);
        return $code;
    }

    /**
     * inviteEncode对应的解码函数
     * @param $code [邀请码]
     * @param $source_str [需要和编码时的$source_str一致才能解码]
     * @return bool|int [用户id]
     */
    public static function inviteDecode($code, $source_str = 'ENV6J2R3HWXSTQA47MU5FCDGOPIB189KLYZ')
    {
        if (strrpos($code, '0') !== false)
            $code = substr($code, strrpos($code, '0') + 1);
        $len = strlen($code);
        $code = strrev($code);
        $num = 0;
        for ($i = 0; $i < $len; $i++) {
            $num += strpos($source_str, $code[$i]) * pow(35, $i);
        }
        return $num;
    }

}