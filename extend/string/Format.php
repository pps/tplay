<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/10/10
 * Time: 15:49
 */

namespace string;

use think\Exception;

/**
 * 格式转换
 * Class Format
 * @package string
 */
class Format
{

    /**
     * 字符串转编码
     * @param $str string 输入内容
     * @param $outputEncoding string 输出内容编码
     * @return string
     * @throws Exception
     */
    public static function strEncode($str, $outputEncoding = 'UTF-8')
    {
        $detectedEncoding = mb_detect_encoding($str, array("ASCII", 'UTF-8', "GB2312", "GBK", 'BIG5', 'ISO-8859-1', 'Windows-1252'));
        if (!$detectedEncoding) {
            throw new Exception("无法检测文件编码，请检查文件内容是否正确");
        }
        $str_encode = mb_convert_encoding($str, $outputEncoding, $detectedEncoding);
        return $str_encode;
    }

    /**
     * 文字转拼音
     * @param $text
     * @return string
     */
    public static function toPinyin($text)
    {
        $pinyin = new \Overtrue\Pinyin\Pinyin();
        return $pinyin->permalink($text, '');//分隔符""
    }

    /**
     * 文字转首字母
     */
    public static function toFirstLetter($text)
    {
        $pinyin = new \Overtrue\Pinyin\Pinyin();
        return $pinyin->abbr($text, PINYIN_KEEP_NUMBER);//拼音首字母+数字
    }

    /**
     * 字节大小 格式化为合适的单位
     * @param  number $size 字节数
     * @param  string $delimiter 数字和单位分隔符
     * @return string   格式化后的带单位的大小
     */
    public static function formatBytes($size, $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
        return round($size, 2) . $delimiter . $units[$i];
    }

    /**
     * 隐藏昵称部分
     * @param $str
     * @param int $start 从第3位置开始替换
     * @param int $len 替换6个字符
     * @return mixed
     */
    public static function hideStr($str, $start = 3, $len = 6)
    {
        $replacement = str_repeat('*', $len);
        return substr_replace($str, $replacement, $start, $len);
    }

    /**
     * 转为百分比格式
     * @param $number int [计算好的rate]
     * @return int|string
     */
    public static function rateToPercent($number)
    {
        return $number == 0 ? 0 : sprintf("%.4f", $number) * 100 . '%';
    }

    /**
     * PHP将Markdown文件解析为HTML
     * https://gitee.com/JonahXie/parsedown/tree/master
     * @param $source string 原始文本
     * @return string
     */
    public static function formatMarkdown($source)
    {
        include("Parsedown.php");
        $Parsedown = new \Parsedown();
        $Parsedown->setSafeMode(true);
        return $Parsedown->text($source);
    }
}