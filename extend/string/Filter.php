<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2023/10/9
 * Time: 10:05
 */

namespace string;

use think\Exception;

/**
 * 过滤字符串
 * Class Filter
 * @package string
 */
class Filter
{
    /**
     * 过滤特殊字符，如微信昵称的表情符
     * @param $str
     * @return mixed
     */
    public static function filterEmoji($str)
    {
        $str = preg_replace_callback('/./u',
            function (array $match) {
                return strlen($match[0]) >= 4 ? '?' : $match[0];
            },
            $str);
        return $str;
    }

    /**
     * 去除换行符号
     * @param $str
     * @return mixed
     */
    public static function filterLineFeedCharacter($str)
    {
        return str_replace(array("\r\n", "\r", "\n"), "", $str);
    }

    /**
     * 过滤所有标点符号
     * @param $str
     * @return mixed|string
     */
    public static function filterWord($str)
    {
        if (trim($str) == '') return '';
        return preg_replace("/[^\x{4e00}-\x{9fa5}a-zA-Z0-9]/u", '', $str);
    }

    /**
     * 防止SQL注入
     * @param $value
     * @throws Exception
     */
    public static function checkSqlInj($value)
    {
        $arr = explode('|', 'UPDATEXML|UPDATE|WHERE|EXEC|INSERT|SELECT|DELETE|COUNT|CHR|MID|MASTER|TRUNCATE|DECLARE|BIND|DROP|CREATE| EXP |EXP%| OR |XOR| LIKE |NOTLIKE|NOT BETWEEN|NOTBETWEEN|BETWEEN|NOTIN|NOT IN|CONTACT|EXTRACTVALUE|LOAD_FILE|INFORMATION_SCHEMA|outfile|%20|into|union');
        if (is_string($value)) {
            foreach ($arr as $a) {
                //判断参数值中是否含有SQL关键字，如果有则跳出
                if (stripos($value, $a) !== false) {
                    throw new Exception('含有敏感字符' . $a);
                }
            }
        } elseif (is_array($value)) {
            //如果参数值是数组则递归遍历判断
            foreach ($value as $v) {
                self::checkSqlInj($v);
            }
        }
    }

}