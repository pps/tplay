<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 19:31
 */

namespace file;


class PathHelper
{

    /**
     * 解析文件路径
     * @param $path
     * @return mixed
     */
    public static function parse($path)
    {
//        $url = 'C:/Windows/System32/test.txt';
//        解析后的数组内容
//        array (
//            'dirname' => 'C:/Windows/System32',//目录路径
//            'basename' => 'test.txt',//文件名+后缀名
//            'extension' => 'txt',//文件后缀名
//            'filename' => 'test',//文件名
//        )
        return pathinfo($path);
    }


    /**
     * 提取路径中的目录路径
     * @param $path
     * @return string
     */
    public static function getDir($path)
    {
        // 测试结果，短的路径，获取的结果不能用于拼接路径使用
//      /dcyy/          =  \
//      /dcyy           =  \
//      ./dcyy          =  .
//      bbbb            =  .
//      /aaaa/bbbb      =  /aaaa
//      /aaaa/bbbb/cc   =  /aaaa/bbbb
//      ./aaaa/bbbb/cc  =  ./aaaa/bbbb
//      ../aaaa/bbbb/cc =  ../aaaa/bbbb
//C:/Windows/System32/etc    = C:/Windows/System32
//http://my.com/post/20.html = http://my.com/post

        return dirname($path);//等于 pathinfo($path)['dirname']
    }

    /**
     * 提取父目录路径
     * @param $path
     * @param string $separator
     * @return array|mixed|string
     */
    public static function getParentDir($path, $separator = DS)
    {
        $path = deleteEndDS($path, $separator);
        $res = explode($separator, $path);
        if (is_array($res)) {
            $end = end($res);
            //去除最后一个节点，使用preg_replace替换最后一个匹配到的
            $endres = preg_replace("~$end(?!.*$end)~", '', $path);
            return $separator == $endres ? $endres : deleteEndDS($endres, $separator);
        }
        return $res;
    }
}