<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 17:23
 */

namespace file;


use string\Format;
use think\Exception;

class CsvHelper
{

    /**
     * 解析 CSV 文件
     * @param string $filepath 文件完整路径
     * @return array 返回二维数组，每一行是一个子数组，子数组的每个元素对应一个单元格,第一行是标题
     * @throws Exception
     */
    public static function csvToArray($filepath)
    {
        $handle = fopen($filepath, 'r');
        if ($handle === false) {
            throw new Exception("无法打开文件: " . $filepath);
        }
        $out = [];
        $n = 0;
        while ($data = fgetcsv($handle, 0)) {
            foreach ($data as $i => $value) {
                $out[$n][$i] = Format::strEncode($value);
            }
            $n++;
        }
        fclose($handle);
        return $out;
    }

    /**
     * 导出csv (在浏览器中导出)
     * @param $titles array 标题数组
     * @param $data array 内容,二维数组
     * @param string $filename 文件名,默认是今天日期
     */
    public static function arrayToCsv($titles, $data, $filename = "")
    {
        if (empty($filename)) {
            $filename = date('Ymd');
        }
        $str = "";
        if ($titles && is_array($titles)) {
            foreach ($titles as $title) {
                $str .= (empty($str) ? "" : ",") . $title;
            }
            $str = Format::strEncode($str . PHP_EOL);
        }

        if ($data && is_array($data)) {
            foreach ($data as $line) {
                $line_str = "";
                foreach ($line as $row) {
                    $row_ = Format::strEncode($row);
                    $line_str .= (empty($line_str) ? "" : ",") . $row_;
                }
                $str .= $line_str . PHP_EOL;
            }
        }
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=$filename.csv");
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        exit($str);
    }


    /**
     * 导出csv
     * @param $titles array 标题数组
     * @param $data array 内容,二维数组
     * @param string $filepath 文件名,默认是今天日期
     */
    public static function arrayToCsvFile($titles, $data, $filepath)
    {
        $str = "";
        if ($titles && is_array($titles)) {
            foreach ($titles as $title) {
                $str .= (empty($str) ? "" : ",") . $title;
            }
            $str = Format::strEncode($str . PHP_EOL);
        }

        if ($data && is_array($data)) {
            foreach ($data as $line) {
                $line_str = "";
                foreach ($line as $row) {
                    $row_ = Format::strEncode($row);
                    $line_str .= (empty($line_str) ? "" : ",") . $row_;
                }
                $str .= $line_str . PHP_EOL;
            }
        }
        FileHelper::save($filepath, $str);
    }


    /**
     * 导出excel (在浏览器中导出)
     * @param $titles array 标题数组
     * @param $data array 内容,二维数组
     * @param string $filename 文件名,默认是今天日期
     */
    public static function arrayToExcel($titles, $data, $filename = "")
    {
        if (empty($filename)) {
            $filename = date('Ymd');
        }
        header("Content-Type: application/vnd.ms-execl;charset=utf-8");
        header("Content-Disposition: attachment; filename=$filename.xls");
        $str = '
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
xmlns:html="http://www.w3.org/TR/REC-html40">
<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">


  <Created>2014-05-06T09:09:49Z</Created>
  <LastSaved>2014-05-06T08:55:17Z</LastSaved>
  <Version>14.00</Version>
</DocumentProperties>
<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
</OfficeDocumentSettings>
<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12330</WindowHeight>
  <WindowWidth>28035</WindowWidth>
  <WindowTopX>360</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
</ExcelWorkbook>
<Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s62">
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#FF0000"/>
  </Style>
  <Style ss:ID="s63">
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#FF0000"/>
  </Style>
</Styles>
<Worksheet ss:Name="myExcel (3)">
  <Table>
   <Column ss:Index="3" ss:AutoFitWidth="0" ss:Width="207"/>';
        echo $str;

        echo '<Row ss:AutoFitHeight="0">';
        /* 第一行标题 */
        foreach ($titles as $v) {
            echo '<Cell><Data ss:Type="String">';
            echo $v;
            echo '</Data></Cell>';
        }
        echo '</Row>';

        /* 表格内容 */
        foreach ($data as $line) {
            echo '<Row ss:AutoFitHeight="0">';
            foreach ($line as $row) {
                echo '<Cell><Data ss:Type="String">';
                echo $row;
                echo '</Data></Cell>';
            }
            echo '</Row>';
        }

        $str2 = '</Table></Worksheet></Workbook>';
        echo $str2;
    }

}