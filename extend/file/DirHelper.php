<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 18:39
 */

namespace file;


use string\Format;

class DirHelper
{

    /**
     * 创建多级目录
     * @param string $path 目录路径
     * @return bool 返回操作是否成功
     */
    public static function makeDir($path)
    {
        if (!file_exists($path)) {
            // 尝试创建目录
            if (mkdir($path, 0755, true)) {
                // 目录创建成功
                return true;
            } else {
                // 目录创建失败
                return false;
            }
        }
        // 如果目录已存在，不需要再次创建
        return true;
    }

    /**
     * 不经过回收站，递归删除文件夹（本机测试时请慎用）
     * @param $dir [目录路径]
     */
    public static function delDir($dir)
    {
        if (file_exists($dir)) {
            $mydir = dir($dir);
            while (false !== ($file = $mydir->read())) {
                if ($file != "." && $file != "..") {
                    $path = $dir . DS . $file;
                    is_dir($path) ? self::delDir($path) : @unlink($path);
                }
            }
            $mydir->close();
            @rmdir($dir);
        }
    }

    /**
     * 复制目录
     * @param $source [源目录路径]
     * @param $dest [目标目录路径]
     * @param $cover [拷贝并覆盖同名文件]
     */
    public static function copyDir($source, $dest, $cover = true)
    {
        if (!file_exists($dest)) self::makeDir($dest);
        $handle = opendir($source);
        while (($item = readdir($handle)) !== false) {
            if ($item == '.' || $item == '..') continue;
            $_source = $source . DS . $item;
            $_dest = $dest . DS . $item;
            if (is_dir($_source)) {
                self::copyDir($_source, $_dest, $cover);
            } else if (is_file($_source)) {
                if ($cover || (!$cover && !file_exists($_dest))) {
                    copy($_source, $_dest);
                }
            }
        }
        closedir($handle);
    }

    /**
     * 读取出一个文件夹及其子文件夹下所有文件
     * @param $path
     * @return array|mixed|string
     */
    public static function scanDirs($path)
    {
        global $result;
        $files = scandir($path);
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                if (is_dir($path . DS . $file)) {
                    $result[] = $path . DS . $file;
                    self::scanDirs($path . DS . $file);
                } else {
                    $result[] = $path . DS . $file;
                }
            }
        }
        return $result;
    }

    /**
     * 获取$dir目录下的所有 文件名or目录名
     * @param $dir string 根目录
     * @param string $pattern 指定匹配的文件 [*返回所有目录和文件]、[*.*返回所有文件]
     * @param string $suffix 扩展名,返回不带扩展名的文件名
     * @return array
     */
    public static function getFilenameList($dir, $pattern = '*.*', $suffix = null)
    {
        if (!$suffix && ifContain($pattern, '.')) {
            $suffix = deleteStartDS($pattern, '*');//默认提取$pattern扩展名.*
        }
        $pathList = glob(appendEndDS($dir) . $pattern);//匹配文件/目录的数组。如果失败则返回 FALSE
        $res = [];
        foreach ($pathList as $key => $path) {
            //处理中文编码
            $path = Format::strEncode($path);
            //返回不带扩展名的文件名
            $res[] = self::mybasename($path, $suffix);
        }
        return $res;
    }


    //返回不带扩展名的文件名
    public static function mybasename($path, $suffix = '')
    {
        /**
         * basename()有时候出问题
         * basename('动物/牛/coverr-cows.mp4');得到的是 “牛/coverr-cows.mp4”
         * 可能是转义导致的？改为 basename('动物/牛////coverr-cows.mp4');才可以
         * 中文也会有bug，部分中文名，读取时好时坏
         */
//        $basename = basename($path, $suffix);

        //正則代替basename方法
        preg_match('/[^\/]*$/', $path, $matches);
        $basename = empty($suffix) ? $matches[0] : str_replace($suffix, '', $matches[0]); // 输出文件名

        if (ifContain($basename, '/')) {
            $arr = explode('/', $basename);
            return end($arr);
        }
        if (ifContain($basename, '\\')) {
            $arr = explode('\\', $basename);
            return end($arr);
        }
        return $basename;
    }
}