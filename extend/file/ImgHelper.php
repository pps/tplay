<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022/8/10
 * Time: 17:43
 */

namespace file;


use think\Image;

class ImgHelper
{
    /**
     * 判断该文件或路径是否是一个图片文件
     * https://www.jb51.net/article/102260.htm
     * @param $filepath
     * @return bool
     */
    public static function isImage($filepath)
    {
        //如果本地有文件，通过文件信息判断
        if (function_exists('exif_imagetype') && file_exists($filepath)) {
            $mimetype = @exif_imagetype($filepath);
            if ($mimetype == IMAGETYPE_GIF || $mimetype == IMAGETYPE_JPEG || $mimetype == IMAGETYPE_PNG || $mimetype == IMAGETYPE_BMP) {
                return true;
            }
            return false;
        }
        //否则通过文件后缀判断
        $ext = strtolower(PathHelper::parse($filepath)['extension']);
        if (in_array($ext, ['gif', 'jpg', 'jpeg', 'png', 'bmp'])) {
            return true;
        }
        return false;
    }

    /**
     * 缩略图
     * @param $filePath [原图片物理路径]
     * @param string $savePath [保存路径]
     * @param int $width [缩略图最大宽度]
     * @param int $height [缩略图最大高度]
     * @param int $type [缩略图裁剪类型]
     * @return $obj /think/Image
     */
    public static function thumb($filePath, $width, $height, $savePath = '', $type = Image::THUMB_SCALING)
    {
        if (!$savePath) {
            $savePath = $filePath;//覆盖原图
        }
        $image = Image::open($filePath);
        return $image->thumb($width, $height, $type)->save($savePath);//生成缩略图
    }
}